package com.mobilewarehouse.firdavs.mobilewarehouse;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.print.PrintManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Firdavs on 25.02.2015.
 */
public class Priem_sredstvActivity extends Activity {
    public AutoCompleteTextView txt_client;
    public EditText txt_summa;
    public EditText txt_primechanie;
    public TextView txt_klient;
    public Button btn_add;
    public static Boolean txtchanged = true;
    public static String sclient_name, sclient_account, sclient_balance;
    public static Priem_sredstvActivity _myLayout;
    public ImageButton btn_client_select;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.slide_to_right, R.anim.slide_to_left);
        setContentView(R.layout.activity_menu_priem_sredstv);
        Priem_sredstvActivity._myLayout = this;
        this.setTitle(LoginActivity._myLayout.app_title);

        txtchanged = true;
        txt_client = (AutoCompleteTextView) findViewById(R.id.edt_priem_klient);
        txt_summa = (EditText) findViewById(R.id.edt_priem_summa);
        txt_primechanie = (EditText) findViewById(R.id.edt_priem_primechanie);
        txt_klient = (TextView) findViewById(R.id.txt_priem_klient);
        btn_add = (Button) findViewById(R.id.btn_priem_prinyat);
        btn_client_select = (ImageButton) findViewById(R.id.btn_client_select);

        //zapolnyaem polya
        if (sclient_balance != null) {
            txt_klient.setText(getString(R.string.sales_client) + "[" + sclient_balance + "]");
        }
        txt_client.setText(sclient_name);


        //vybor clienta iz spiska
        btn_client_select.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                show_client_list();
            }
        });

        txt_client.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (txt_client.getText().length() > 2
                        && txt_client.getText().length() < 11
                        && txtchanged == true) {
                    fill_list(txt_client.getText().toString(), "1");
                }
            }
        });

        txt_client.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                //position v massive
                sclient_name = Sales_Activity._myLayout.client_name[position];
                sclient_account = Sales_Activity._myLayout.client_account[position];
                sclient_balance = Sales_Activity._myLayout.client_balance[position];
                txtchanged = false;
                txt_klient.setText(getString(R.string.sales_client) + "[" + sclient_balance + "]");
                txt_summa.requestFocus();
            }
        });

        txt_client.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (txt_client.getRight() - txt_client.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        txt_client.setText("");
                        txt_klient.setText(getString(R.string.sales_client));
                        txtchanged = true;
                        return true;
                    }
                }
                return false;
            }
        });

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alrtauth = new AlertDialog.Builder(Priem_sredstvActivity.this);
                alrtauth.setTitle(getString(R.string.app_warning));
                if (check_fields() == 0) {   //все поля заполнены
                    //delaem proc. otpravki dannyx na server
                    send_kassa_to_server_dlg();
                    //-->>
                    //чистим поля
                    //clear_fields();
                    //dat vozmozhnost polzovatsya http_fill_list
                    //txtchanged = true;
                    //dannie uspsehno otpravleny
                    //Toast.makeText(Priem_sredstvActivity.this, getString(R.string.sales_tovar_send_to_server_ok), Toast.LENGTH_LONG).show();
                    return;
                } else {
                    //заполните поля
                    alrtauth.setMessage(getString(R.string.app_fill_field));
                }
                alrtauth.setCancelable(false);
                alrtauth.setNegativeButton(getString(R.string.app_ok), null);
                alrtauth.show();
            }
        });
    }

    void send_kassa_to_server_dlg() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_warning)); // заголовок для диалога
        builder.setMessage(getString(R.string.sales_tovar_send_to_server));
        builder.setNegativeButton(getString(R.string.app_no), new DialogInterface.OnClickListener() {
            //@Override
            public void onClick(DialogInterface dialog, int item) {
                dialog.cancel();
            }
        });
        builder.setPositiveButton(getString(R.string.app_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                send_kassa_to_server();
            }
        });

        builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    void send_kassa_to_server() {
        if(LoginActivity._myLayout.app_mode) {
            JSONObject jsonObjSend = new JSONObject();
            try {
                jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
                jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
                jsonObjSend.put("Client", sclient_account);
                jsonObjSend.put("Sum", txt_summa.getText().toString());
                jsonObjSend.put("Comment", txt_primechanie.getText().toString());

                Log.d("json", jsonObjSend.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/prixodDeneg/", jsonObjSend);
            http_send_kassoviy_order kassa_me = new http_send_kassoviy_order(Priem_sredstvActivity.this);
            kassa_me.execute(params);
        }else{
            if(save_prixod(txt_summa.getText().toString(), txt_primechanie.getText().toString()) != 0)
                Toast.makeText(Priem_sredstvActivity.this, getString(R.string.sales_prixod_save_err), Toast.LENGTH_LONG).show();
            clear_fields();
            txtchanged = true;
        }
    }

    int save_prixod(String sum, String comment) {
        int otvet = 0;
        SimpleDateFormat dformat = new SimpleDateFormat("yyMMddHHmmss");
        Date date = new Date();
        String _tranzaction = dformat.format(date);

        ContentValues content = new ContentValues();
        //записываем в табл. prixody
        content.put("_tranzcode", _tranzaction);
        content.put("_client_account", sclient_account);
        content.put("_sum", sum);
        content.put("_comment", comment);
        try {
            LoginActivity.database.insert("_prixody", null, content);
        } catch (Exception e) {
            otvet = 1;
        }
        content.clear();
        return otvet;
    }

    //chistim polya
    void clear_fields() {
        sclient_name = "";
        sclient_account = "";
        sclient_balance = "";
        txt_client.setText("");
        txt_summa.setText("");
        txt_primechanie.setText("");
        txt_klient.setText(getString(R.string.sales_client));
        txt_client.requestFocus();
    }

    //pokazyvaem spisok klientov
    void show_client_list() {
        Intent intent = new Intent(getApplicationContext(), Marshrut_records_accountsActivity.class);
        startActivity(intent);
        finish();
    }

    int check_fields() {
        int otvet = 0;
        if (
                txt_client.length() == 0
                        || txt_summa.length() == 0
                ) {
            otvet = 1;
        }
        return otvet;
    }

    //timer dlya pokaza spiska autocomplete
    public void show_drop_down_list(final int _list) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (_list == 1) {
                    txt_client.showDropDown();
                }
            }
        }, 300);
    }

    void fill_list(String _txt, String _type) {
        JSONObject jsonObjSend = new JSONObject();
        try {
            // Add key/value pairs
            Boolean rememb = true;
            jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
            jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
            jsonObjSend.put("Like", _txt);
            jsonObjSend.put("Type", _type);

            // Output the JSON object we're sending to Logcat:
            Log.d("json", jsonObjSend.toString(2));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/getlist/", jsonObjSend);
        http_fill_list fill_list = new http_fill_list(Priem_sredstvActivity.this);
        fill_list.execute(params);
    }

    ///////// WORKING WITH BACK BUTTON //////////////////////////////////////
    @Override
    public void onBackPressed() {
        show_client_list();
        //finish();
    }

    //создаем меню
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        menu.add(Menu.NONE, LoginActivity.IDM_MAIN, menu.NONE, getString(R.string.app_menu2))
                .setIcon(android.R.drawable.ic_menu_revert);
        return (super.onCreateOptionsMenu(menu));
    }

    ;

    //обрабатываем выбор пункта меню
    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case LoginActivity.IDM_MAIN:
                show_client_list();
                //finish();
                break;
            default:
                return false;
        }
        return true;
    }

    ;
}
