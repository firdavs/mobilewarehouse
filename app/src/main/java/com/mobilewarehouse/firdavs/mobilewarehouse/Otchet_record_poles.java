package com.mobilewarehouse.firdavs.mobilewarehouse;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

/**
 * Created by Firdavs on 10.03.2015.
 */
public class Otchet_record_poles extends Activity {
    public AutoCompleteTextView txt_klient;
    EditText txt_data_ot = null;
    EditText txt_data_po = null;
    TableRow trow = null;
    TextView lbl_ot_name = null;

    public static Boolean txtchanged = true;
    public static String s_dt_ot = "", sclient_account = "", s_dt_po = "", ot_html;
    public static Otchet_record_poles _myLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otchet_record_poles);
        Otchet_record_poles._myLayout = this;
        this.setTitle(LoginActivity._myLayout.app_title);

        Operations_menuActivity._myLayout._operation = "_otchet_clients";
        gen_otchet_fields();

        Button btn_data_ot = (Button) findViewById(R.id.btn_otchet_data_ot);
        Button btn_data_po = (Button) findViewById(R.id.btn_otchet_data_po);
        Button btn_poluchit = (Button) findViewById(R.id.btn_otchet_get_otchet);

        txt_data_ot = (EditText) findViewById(R.id.txt_otchet_data_ot);
        txt_data_po = (EditText) findViewById(R.id.txt_otchet_data_po);
        txt_klient = (AutoCompleteTextView) findViewById(R.id.txt_otchet_klient);
        lbl_ot_name = (TextView) findViewById(R.id.lbl_otchet_name);
        lbl_ot_name.setText(Otchet_recordsActivity._myLayout.ot_name[Otchet_recordsActivity._myLayout.sel_otchet]);

        btn_data_ot.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dpd = new DatePickerDialog(Otchet_record_poles.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                String _nol = "0";
                                txt_data_ot.setText((year) + "-" + _nol.substring(String.valueOf(monthOfYear + 1).length() - 1, 1) + "" + (monthOfYear + 1) + "-" + _nol.substring(String.valueOf(dayOfMonth).length() - 1, 1) + "" + dayOfMonth);
                                s_dt_ot = txt_data_ot.getText().toString();
                            }
                        }, mYear, mMonth, mDay);
                dpd.show();
            }
        });

        btn_data_po.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dpd = new DatePickerDialog(Otchet_record_poles.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                String _nol = "0";
                                txt_data_po.setText((year) + "-" + _nol.substring(String.valueOf(monthOfYear + 1).length() - 1, 1) + "" + (monthOfYear + 1) + "-" + _nol.substring(String.valueOf(dayOfMonth).length() - 1, 1) + "" + dayOfMonth);
                                s_dt_po = txt_data_po.getText().toString();
                            }
                        }, mYear, mMonth, mDay);
                dpd.show();
            }
        });

        txt_klient.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (txt_klient.getText().length() > 2
                        && txt_klient.getText().length() < 11
                        && txtchanged == true) {
                    fill_list(txt_klient.getText().toString(), Otchet_recordsActivity._myLayout.ot_p1_source[Otchet_recordsActivity._myLayout.sel_otchet]);
                }
            }
        });

        txt_klient.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                sclient_account = Sales_Activity._myLayout.client_account[position];
                txtchanged = false;
            }
        });

        txt_klient.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (txt_klient.getRight() - txt_klient.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        txt_klient.setText("");
                        txtchanged = true;
                        return true;
                    }
                }
                return false;
            }
        });

        btn_poluchit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (check_fields() == 0) { //vse polya zapolneni
                    get_otchet_data();
                } else {   //zapolenite vse polya
                    AlertDialog.Builder alrtauth = new AlertDialog.Builder(Otchet_record_poles.this);
                    alrtauth.setTitle(getString(R.string.app_warning));
                    alrtauth.setMessage(getString(R.string.app_fill_field));
                    alrtauth.setCancelable(false);
                    alrtauth.setNegativeButton(getString(R.string.app_cancel), null);
                    alrtauth.show();
                }
            }
        });
    }

    int check_fields() {
        int _otv = 0;
        if (Otchet_recordsActivity._myLayout.ot_p1[Otchet_recordsActivity._myLayout.sel_otchet] == true) {
            if (txt_klient.getText().toString().trim().length() == 0)
                _otv = 1;
        }
        if (Otchet_recordsActivity._myLayout.ot_p2[Otchet_recordsActivity._myLayout.sel_otchet] == true) {
            if (txt_data_ot.getText().toString().trim().length() == 0)
                _otv = 1;
        }
        if (Otchet_recordsActivity._myLayout.ot_p3[Otchet_recordsActivity._myLayout.sel_otchet] == true) {
            if (txt_data_po.getText().toString().trim().length() == 0)
                _otv = 1;
        }
        return _otv;
    }

    void gen_otchet_fields() {
        trow = (TableRow) findViewById(R.id.ot_tableRow1);
        trow.setVisibility(View.GONE);
        trow = (TableRow) findViewById(R.id.ot_tableRow2);
        trow.setVisibility(View.GONE);
        trow = (TableRow) findViewById(R.id.ot_tableRow3);
        trow.setVisibility(View.GONE);

        if (Otchet_recordsActivity._myLayout.ot_p1[Otchet_recordsActivity._myLayout.sel_otchet] == true) { //data ot
            trow = (TableRow) findViewById(R.id.ot_tableRow1);
            trow.setVisibility(View.VISIBLE);
        }
        if (Otchet_recordsActivity._myLayout.ot_p2[Otchet_recordsActivity._myLayout.sel_otchet] == true) { //data po
            trow = (TableRow) findViewById(R.id.ot_tableRow2);
            trow.setVisibility(View.VISIBLE);
        }
        if (Otchet_recordsActivity._myLayout.ot_p3[Otchet_recordsActivity._myLayout.sel_otchet] == true) { //spisok
            trow = (TableRow) findViewById(R.id.ot_tableRow3);
            trow.setVisibility(View.VISIBLE);
        }
    }

    //timer dlya pokaza spiska autocomplete
    public void show_drop_down_list(final int _list) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (_list == 1) {
                    txt_klient.showDropDown();
                }
            }
        }, 300);
    }

    void fill_list(String _txt, String _type) {
        JSONObject jsonObjSend = new JSONObject();
        try {
            jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
            jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
            jsonObjSend.put("Like", _txt);
            if (_type.equalsIgnoreCase("accounts_62")) {
                jsonObjSend.put("Type", "1");
            } else {
                jsonObjSend.put("Type", "3");
            }

            // Output the JSON object we're sending to Logcat:
            Log.d("json", jsonObjSend.toString(2));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/getlist/", jsonObjSend);
        http_fill_list fill_list = new http_fill_list(Otchet_record_poles.this);
        fill_list.execute(params);
    }

    void get_otchet_data() {
        JSONObject jsonObjSend = new JSONObject();
        try {
            jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
            jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
            jsonObjSend.put("Id", Otchet_recordsActivity._myLayout.ot_id[Otchet_recordsActivity._myLayout.sel_otchet]);
            //peremeshaem znachenie peremennyx, esli pervoe pole ne pokazyvaetsya
            if (sclient_account.length() == 0) {
                //sclient_account = s_dt_ot;
                sclient_account = LoginActivity._myLayout.user_login;
                //if id is 1048, send login_account insted of login
                if(Otchet_recordsActivity._myLayout.ot_id[Otchet_recordsActivity._myLayout.sel_otchet].equalsIgnoreCase("1048")
                        || Otchet_recordsActivity._myLayout.ot_id[Otchet_recordsActivity._myLayout.sel_otchet].equalsIgnoreCase("1051")) {
                    sclient_account = LoginActivity._myLayout.login_account;
                }
                if (s_dt_po.length() > 0 && s_dt_ot.length() <= 0)
                    s_dt_ot = s_dt_po;
            }
            jsonObjSend.put("Param1", sclient_account);
            //160203 tempopary
            //jsonObjSend.put("Param1", LoginActivity._myLayout.user_login);
            jsonObjSend.put("Param2", s_dt_ot);
            jsonObjSend.put("Param3", s_dt_po);

            Log.d("json", jsonObjSend.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/getReport/", jsonObjSend);
        http_get_otchet_data otchet_data_me = new http_get_otchet_data(Otchet_record_poles.this);
        otchet_data_me.execute(params);
    }

    ///////// WORKING WITH BACK BUTTON //////////////////////////////////////
    @Override
    public void onBackPressed() {
        finish();
    }

    //создаем меню
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        menu.add(Menu.NONE, LoginActivity.IDM_MAIN, menu.NONE, getString(R.string.app_menu2))
                .setIcon(android.R.drawable.ic_menu_revert);
        return (super.onCreateOptionsMenu(menu));
    }

    ;

    //обрабатываем выбор пункта меню
    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case LoginActivity.IDM_MAIN:
                finish();
                break;
            default:
                return false;
        }
        return true;
    }

    ;
}
