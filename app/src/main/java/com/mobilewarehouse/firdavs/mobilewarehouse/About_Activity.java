package com.mobilewarehouse.firdavs.mobilewarehouse;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Created by Firdavs on 30.03.2015.
 */
public class About_Activity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        TextView txt_man11 = (TextView) findViewById(R.id.lbl_about_man11);
        TextView txt_man12 = (TextView) findViewById(R.id.lbl_about_man12);
        TextView txt_man21 = (TextView) findViewById(R.id.lbl_about_man21);
        TextView txt_man22 = (TextView) findViewById(R.id.lbl_about_man22);
        ImageButton btn_about_dai = (ImageButton) findViewById(R.id.btn_about_dai);
        TextView txt_man31 = (TextView) findViewById(R.id.lbl_about_man31);
        TextView txt_man32 = (TextView) findViewById(R.id.lbl_about_man32);
        TextView txt_man41 = (TextView) findViewById(R.id.lbl_about_man41);
        TextView txt_man42 = (TextView) findViewById(R.id.lbl_about_man42);

        txt_man11.setText(getString(R.string.about_app1)); //nazvanie app
        txt_man12.setText(getString(R.string.about_app2));
        txt_man21.setText(getString(R.string.app_versiya) + ": ");
        txt_man22.setText(LoginActivity._myLayout.app_version); //version

        //String _t = "<a href=\"http://" + LoginActivity._myLayout.server_ip + "/app/mobile_sklad.apk\">"+getString(R.string.about_developer5) + "</a>";
        //String app_url = String.format(getResources().getString(R.string.about_developer5), LoginActivity._myLayout.server_ip + "/app/mobile_sklad.apk");
        //txt_man23.setText(getString(R.string.about_developer5).replace("?", LoginActivity._myLayout.server_ip + "/app/mobile_sklad.apk")); //ssilka na obnovlenie app-a
        //Spanned sp = Html.fromHtml("<a " + app_url + ">" + getString(R.string.about_developer6) + "</a>");
        //txt_man23.setText(sp); //ssilka na obnovlenie app-a
        //txt_man23.setMovementMethod(LinkMovementMethod.getInstance());

        txt_man31.setText(getString(R.string.man2man_manager) + ": ");
        txt_man32.setText(LoginActivity.login_fio);   //manager fio

        txt_man41.setText(getString(R.string.sales_client_ostatok) + " "); //ostatok
        txt_man42.setText(LoginActivity._myLayout.login_ostatok); //ostatok

        btn_about_dai.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                mnu_dai();
                //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+LoginActivity._myLayout.server_ip + "/assets/app/mobile_sklad.apk")));
            }
        });
    }

    void mnu_dai() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_warning)); // заголовок для диалога
        builder.setMessage(getString(R.string.about_app_dai_confirm));
        builder.setNegativeButton(getString(R.string.app_no), new DialogInterface.OnClickListener() {
            //@Override
            public void onClick(DialogInterface dialog, int item) {
                dialog.cancel();
            }
        });
        builder.setPositiveButton(getString(R.string.app_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                http_download_and_install_apk downloadAndInstall = new http_download_and_install_apk(About_Activity.this);
                //progress.setCancelable(false);
                //progress.setMessage("Downloading...");
                //downloadAndInstall.setContext(getApplicationContext(), progress);
                downloadAndInstall.execute("http://" + LoginActivity._myLayout.server_ip + "/assets/app/mobile_sklad.apk");
            }
        });

        builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    //создаем меню
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        menu.add(Menu.NONE, LoginActivity.IDM_BACK, menu.NONE, getString(R.string.app_menu2))
                .setIcon(android.R.drawable.ic_menu_revert);
        return (super.onCreateOptionsMenu(menu));
    }

    ;

    //обрабатываем выбор пункта меню
    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case LoginActivity.IDM_BACK:
                finish();
                break;
            default:
                return false;
        }
        return true;
    }

    ;
}
