package com.mobilewarehouse.firdavs.mobilewarehouse;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Set;

/**
 * Created by Firdavs on 25.02.2015.
 */
public class Settings_Activity extends Activity {
    public static Settings_Activity _myLayout;
    public static RadioButton chk_sync_clients, chk_sync_operations;
    public static TextView txt_queue;
//    public static CheckBox ;
    public static Switch app_mode;
    public static String _tranzaction_operation;
    public static int _type_operation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.slide_to_right, R.anim.slide_to_left);
        setContentView(R.layout.activity_menu_settings);
        Settings_Activity._myLayout = this;

        Button btn_sync = (Button) findViewById(R.id.btn_sync);
        txt_queue = (TextView) findViewById(R.id.txt_queue);
        chk_sync_clients = (RadioButton) findViewById(R.id.chk_sync_clients);
        chk_sync_operations = (RadioButton) findViewById(R.id.chk_sync_operations);
        app_mode = (Switch) findViewById(R.id.app_mode);
        app_mode.setChecked(LoginActivity._myLayout.app_mode);
        read_queue();

        app_mode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                set_mode(isChecked);
            }
        });

        btn_sync.setOnClickListener(new View.OnClickListener(){
            public void onClick(View arg0) {
                do_sync();
            }
        });

    }

    void do_sync() {
        if(chk_sync_clients.isChecked() || chk_sync_operations.isChecked()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getString(R.string.app_warning)); // заголовок для диалога
            builder.setMessage(getString(R.string.app_sync_to_do));
            builder.setNegativeButton(getString(R.string.app_no), new DialogInterface.OnClickListener() {
                //@Override
                public void onClick(DialogInterface dialog, int item) {
                    dialog.cancel();
                }
            });
            builder.setPositiveButton(getString(R.string.app_yes), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int item) {
                    LoginActivity._myLayout.syncing = true;
                    if (chk_sync_clients.isChecked()) {
                        if(!LoginActivity._myLayout.login_type.equalsIgnoreCase("2")) { //ne proizvodstvo
                            get_marshrut_list();
                        }else{
                            get_fasovshiki_list();
                        }
                    } else if (chk_sync_operations.isChecked()) {
                        send_operations();
                    }
                }
            });

            builder.setCancelable(false);
            builder.create();
            builder.show();
        }
    }

    void get_marshrut_list() {
        JSONObject jsonObjSend = new JSONObject();
        try {
            jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
            jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
            jsonObjSend.put("Type", "4");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/getList/", jsonObjSend);
        http_get_marshrut_list marshrut_me = new http_get_marshrut_list(Settings_Activity.this);
        marshrut_me.execute(params);
    }

    void get_fasovshiki_list()
    {
        JSONObject jsonObjSend = new JSONObject();
        try {
            jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
            jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/getListOfFas/", jsonObjSend);
        http_fill_list_tsex fill_list_tsex = new http_fill_list_tsex(Settings_Activity.this);
        fill_list_tsex.execute(params);
    }

    void set_mode(boolean mode)
    {
        //режим работы
        LoginActivity._myLayout.keys_editor = LoginActivity._myLayout.keys.edit();
        LoginActivity._myLayout.keys_editor.putBoolean("app_mode", mode);
        try {
            LoginActivity._myLayout.keys_editor.commit();
            LoginActivity._myLayout.app_mode = mode;
        } catch (Exception err) {
            LoginActivity._myLayout.generate_message(getString(R.string.app_error2), err.getMessage().toString());
        }
    }

    void read_queue()
    {
        String lbl;
        String[] s = {"*"};
        Cursor cur = null;

        lbl = getString(R.string.settings_queue) + "\r\n";
        cur = LoginActivity._myLayout.query("_operations_common_info", s, "_client_account!=\"\"", "");
        if (cur.getCount() > 0) {
            lbl += getString(R.string.settings_queue_sales) + " " + Integer.valueOf(cur.getCount()) + "\r\n";
        }else{
            lbl += getString(R.string.settings_queue_sales) + " 0\r\n";
        }

        cur = LoginActivity._myLayout.query("_prixody", s, "", "");
        if (cur.getCount() > 0) {
            lbl += getString(R.string.settings_queue_kpo) + " " + Integer.valueOf(cur.getCount()) + "\r\n";
        }else{
            lbl += getString(R.string.settings_queue_kpo) + " 0\r\n";
        }

        cur = LoginActivity._myLayout.query("_rasxody", s, "", "");
        if (cur.getCount() > 0) {
            lbl += getString(R.string.settings_queue_kro) + " " + Integer.valueOf(cur.getCount()) + "\r\n";
        }else{
            lbl += getString(R.string.settings_queue_kro) + " 0\r\n";
        }

        cur = LoginActivity._myLayout.query("_cancels", s, "", "");
        if (cur.getCount() > 0) {
            lbl += getString(R.string.settings_queue_cancels) + " " + Integer.valueOf(cur.getCount()) + "\r\n";
        }else{
            lbl += getString(R.string.settings_queue_cancels) + " 0\r\n";
        }

        cur = LoginActivity._myLayout.query("_operations_common_info_proiz", s, "_client_account!=\"\"", "");
        if (cur.getCount() > 0) {
            lbl += getString(R.string.settings_queue_proiz) + " " + Integer.valueOf(cur.getCount()) + "\r\n";
        }else{
            lbl += getString(R.string.settings_queue_proiz) + " 0\r\n";
        }
        cur.close();
        txt_queue.setText(lbl);
    }

    void send_operations()
    {
        //отправляем заказ на сервер
        String[] s = {"*"};
        Cursor cur = null;
        JSONObject jsonObjSend = new JSONObject();
        JSONObject jsonObjSend2 = null;
        JSONArray jsonArr = null;
        int kol = 0;

        read_queue();
        cur = LoginActivity._myLayout.query("_operations_common_info", s, "", "");
        if (cur.getCount() > 0) {
            try {
                jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
                jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
                jsonObjSend.put("Client", cur.getString(3));
                jsonObjSend.put("Operation", "1");
                jsonObjSend.put("Role", LoginActivity._myLayout.login_type);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //
            _tranzaction_operation = cur.getString(0);
            cur.close();

            //получаем данные о товарах
            cur = LoginActivity._myLayout.query("_operations_info", s, "_tranzcode = '" + _tranzaction_operation + "'", "_tovar_name");

            if (cur.getCount() > 0) {
                jsonArr = new JSONArray();
                do {
                    try {
                        jsonObjSend2 = new JSONObject();
                        jsonObjSend2.put("Naimenovanie", cur.getString(2));
                        jsonObjSend2.put("Id", cur.getString(1));
                        jsonObjSend2.put("Count", cur.getString(3));
                        jsonObjSend2.put("Price", cur.getString(5));
                        try {
                            jsonArr.put(kol, jsonObjSend2);
                        } catch (Exception e) {}
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    kol++;
                } while (cur.moveToNext());
            }
            cur.close();

            try {
                jsonObjSend.put("Members", jsonArr);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            _type_operation = 1;
            Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/otpuskTovara/", jsonObjSend);
            http_send_sale send_sale = new http_send_sale(Settings_Activity.this);
            send_sale.execute(params);
        }
        else {
            cur = LoginActivity._myLayout.query("_prixody", s, "", "");
            if (cur.getCount() > 0) {  //ищем операции, приходы
                try {
                    jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
                    jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
                    jsonObjSend.put("Client", cur.getString(1));
                    jsonObjSend.put("Sum", cur.getString(2));
                    jsonObjSend.put("Comment", cur.getString(3));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                _tranzaction_operation = cur.getString(0);
                cur.close();

                _type_operation = 2;
                Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/prixodDeneg/", jsonObjSend);
                http_send_kassoviy_order kassa_me = new http_send_kassoviy_order(Settings_Activity._myLayout);
                kassa_me.execute(params);
            } else {
                cur = LoginActivity._myLayout.query("_rasxody", s, "", "");
                if (cur.getCount() > 0) {  //ищем операции, расходы
                    try {
                        jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
                        jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
                        jsonObjSend.put("Sum", cur.getString(1));
                        jsonObjSend.put("Comment", cur.getString(2));
                        jsonObjSend.put("Statya", cur.getString(3));
                        jsonObjSend.put("Kolvo", cur.getString(4));
                        jsonObjSend.put("Price", cur.getString(5));
                        jsonObjSend.put("Sklad_Id", cur.getString(6));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    _tranzaction_operation = cur.getString(0);
                    cur.close();

                    _type_operation = 3;
                    Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/rasxodDeneg/", jsonObjSend);
                    http_send_rasxodi rasxodi_me = new http_send_rasxodi(Settings_Activity.this);
                    rasxodi_me.execute(params);
                }else {
                    cur = LoginActivity._myLayout.query("_cancels", s, "", "");
                    if (cur.getCount() > 0) {  //ищем операции, расходы
                        try {
                            jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
                            jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
                            jsonObjSend.put("Client", cur.getString(1));
                            jsonObjSend.put("CancelID", cur.getString(2));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        _tranzaction_operation = cur.getString(0);
                        cur.close();

                        _type_operation = 4;
                        Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/sendCancel/", jsonObjSend);
                        http_send_cancel_reason cancel_me = new http_send_cancel_reason(Settings_Activity.this);
                        cancel_me.execute(params);
                    }else {
                        cur = LoginActivity._myLayout.query("_operations_common_info_proiz", s, "", "");
                        if (cur.getCount() > 0) {
                            try {
                                jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
                                jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
                                jsonObjSend.put("Sklad_id", LoginActivity._myLayout.sklad_id);
                                jsonObjSend.put("Fas", cur.getString(3));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            //
                            _tranzaction_operation = cur.getString(0);
                            cur.close();

                            //получаем данные о товарах
                            cur = LoginActivity._myLayout.query("_operations_info_proiz", s, "_tranzcode = '" + _tranzaction_operation + "'", "_tovar_name");

                            if (cur.getCount() > 0) {
                                jsonArr = new JSONArray();
                                do {
                                    try {
                                        jsonObjSend2 = new JSONObject();
                                        jsonObjSend2.put("Id", cur.getString(1));
                                        jsonObjSend2.put("Count", cur.getString(3));
                                        try {
                                            jsonArr.put(kol, jsonObjSend2);
                                        } catch (Exception e) {
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    kol++;
                                } while (cur.moveToNext());
                            }
                            cur.close();

                            try {
                                jsonObjSend.put("Members", jsonArr);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            _type_operation = 5;
                            Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/prixodNaSklad/", jsonObjSend);
                            http_send_proiz send_proiz = new http_send_proiz(Settings_Activity.this);
                            send_proiz.execute(params);
                        }else {
                            chk_sync_operations.setChecked(false);
                        }
                    }
                }
            }
        }
    }

    void clear_tables(int type) {
        if(type == 1) {
            try {
                LoginActivity.database.delete("_operations_common_info", "_tranzcode = '" + _tranzaction_operation + "'", null);
            } catch (Exception e) {
                return;
            }

            try {
                LoginActivity.database.delete("_operations_info", "_tranzcode = '" + _tranzaction_operation + "'", null);
            } catch (Exception e) {
                return;
            }
        }else if(type == 2){
            try {
                LoginActivity.database.delete("_prixody", "_tranzcode = '" + _tranzaction_operation + "'", null);
            } catch (Exception e) {
                return;
            }
        }else if(type == 3){
            try {
                LoginActivity.database.delete("_rasxody", "_tranzcode = '" + _tranzaction_operation + "'", null);
            } catch (Exception e) {
                return;
            }
        }else if(type == 4){
            try {
                LoginActivity.database.delete("_cancels", "_tranzcode = '" + _tranzaction_operation + "'", null);
            } catch (Exception e) {
                return;
            }
        }else if(type == 5){
            try {
                LoginActivity.database.delete("_operations_common_info_proiz", "_tranzcode = '" + _tranzaction_operation + "'", null);
            } catch (Exception e) {
                return;
            }

            try {
                LoginActivity.database.delete("_operations_info_proiz", "_tranzcode = '" + _tranzaction_operation + "'", null);
            } catch (Exception e) {
                return;
            }
        }
    }

    ///////// WORKING WITH BACK BUTTON //////////////////////////////////////
    @Override
    public void onBackPressed() {
        LoginActivity._myLayout.syncing = false;
        finish();
        //overridePendingTransition(R.anim.slide_to_left, R.anim.slide_to_right);
    }
}
