package com.mobilewarehouse.firdavs.mobilewarehouse;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Firdavs on 09.03.2015.
 */
public class Jurnal_record_infoActivity extends Activity {
    public static Jurnal_record_infoActivity _myLayout;
    public static String[] jni_tovar, jni_kolvo, jni_tsena;
    public static String jni_client_name, jni_date, jni_opn_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jurnal_record_info);
        Jurnal_record_infoActivity._myLayout = this;
        this.setTitle(LoginActivity._myLayout.app_title);

        TextView lbl_jn_date = (TextView) findViewById(R.id.jn_lbl_date);
        TextView lbl_jn_operation = (TextView) findViewById(R.id.jn_lbl_operation);
        TextView lbl_jn_client_info = (TextView) findViewById(R.id.jn_lbl_client_info);
        lbl_jn_date.setText(getString(R.string.jni_date) + " " + jni_date);
        lbl_jn_operation.setText(getString(R.string.jni_operation) + " " + jni_opn_name);
        lbl_jn_client_info.setText(getString(R.string.jni_fio) + " " + jni_client_name);

        set_tovari_list_headers();
        fill_tovar_list();
    }

    public void set_tovari_list_headers() {
        ListView list_tovari_heads = (ListView) findViewById(R.id.jn_list_tovari_heads_jurnal);
        ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();
        list_tovari_heads.addHeaderView(getLayoutInflater().inflate(R.layout.list_tovari_headers, null, false));

        SimpleAdapter tovarlist = new SimpleAdapter(this, mylist, R.layout.list_tovari_rows,
                new String[]{"tovar", "kolvo", "tsena", "summa"}, new int[]{R.id.tovari_name, R.id.tovari_kolvo, R.id.tovari_tsena, R.id.tovari_summa});
        list_tovari_heads.setAdapter(tovarlist);
        list_tovari_heads.setClickable(false);

        //header titles
        TextView txt = (TextView) findViewById(R.id.htovar_name);
        txt.setText(R.string.sales_tovar_naimenovanie);
        //kolvo
        txt = (TextView) findViewById(R.id.htovari_kolvo);
        txt.setText(R.string.sales_tovar_kolvo);
        //tsena
        txt = (TextView) findViewById(R.id.htovari_tsena);
        txt.setText(R.string.sales_tovar_tsena);
        //summa
        txt = (TextView) findViewById(R.id.htovari_summa);
        txt.setText(R.string.sales_tovar_summa);
    }

    public void fill_tovar_list() {
        ListView list_tovari = (ListView) findViewById(R.id.jn_list_tovari_jurnal);
        ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();

        double obshs = 0;
        for (int i = 0; i < jni_tovar.length; i++) {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("tovar", jni_tovar[i]);
            map.put("kolvo", jni_kolvo[i]);
            map.put("tsena", jni_tsena[i]);
            map.put("summa", String.valueOf(new DecimalFormat("##.##").format(Double.parseDouble(jni_tsena[i].replace(',', '.')) * Double.parseDouble(jni_kolvo[i]))));
            mylist.add(map);
            obshs += (Double.parseDouble(jni_tsena[i].replace(',', '.')) * Double.parseDouble(jni_kolvo[i]));
        }
        TextView lbl_jn_summa = (TextView) findViewById(R.id.jn_lbl_summa);
        lbl_jn_summa.setText(getString(R.string.sales_tovar_summa_itogo) + " " + String.valueOf(new DecimalFormat("##.##").format(obshs)));

        SimpleAdapter tovarlist = new SimpleAdapter(this, mylist, R.layout.list_tovari_rows,
                new String[]{"tovar", "kolvo", "tsena", "summa"}, new int[]{R.id.tovari_name, R.id.tovari_kolvo, R.id.tovari_tsena, R.id.tovari_summa});
        list_tovari.setAdapter(tovarlist);
        list_tovari.setClickable(true);

        list_tovari.requestFocusFromTouch();
        //list_tovari.setSelection(listtouchposition);
        //cur = null;
    }

    ///////// WORKING WITH BACK BUTTON //////////////////////////////////////
    @Override
    public void onBackPressed() {
        finish();
    }

    //создаем меню
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        menu.add(Menu.NONE, LoginActivity.IDM_BACK, menu.NONE, getString(R.string.app_menu2))
                .setIcon(android.R.drawable.ic_menu_revert);
        return (super.onCreateOptionsMenu(menu));
    }

    ;

    //обрабатываем выбор пункта меню
    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case LoginActivity.IDM_BACK:
                finish();
                break;
            default:
                return false;
        }
        return true;
    }

    ;
}
