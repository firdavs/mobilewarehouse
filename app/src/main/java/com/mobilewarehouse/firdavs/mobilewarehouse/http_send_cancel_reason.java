package com.mobilewarehouse.firdavs.mobilewarehouse;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Firdavs on 24.03.2016.
 */
public class http_send_cancel_reason extends AsyncTask<Url_Params, Void, Void> {
    StringBuilder builder = new StringBuilder();
    Boolean _result = false;
    ProgressDialog progressdlg;
    private Context context;
    String _list;
    JSONObject jsonObjRecv;

    public http_send_cancel_reason(Context cxt) {
        context = cxt;
        progressdlg = new ProgressDialog(context);
    }

    //@Override
    protected Void doInBackground(Url_Params... params) {
        String url = "http://" + params[0].url;
        JSONObject jsonObjSend = params[0].jsonObjSend;

        try {
            DefaultHttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPostRequest = new HttpPost(url);


            List<NameValuePair> data = new ArrayList<NameValuePair>();
            data.add(new BasicNameValuePair("data", jsonObjSend.toString()));
            httpPostRequest.setEntity(new UrlEncodedFormEntity(data, "UTF-8"));

            HttpResponse response = httpclient.execute(httpPostRequest);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream instream = entity.getContent();
                String resultString = Main_menuActivity._myLayout.convertStreamToString(instream);
                instream.close();
                try {
                    jsonObjRecv = new JSONObject(resultString);
                    if (jsonObjRecv != null) {
                        try {
                            if (jsonObjRecv.get("Has_result").toString().equalsIgnoreCase("true")) {
                                _result = true;
                            }
                        } catch (Exception e) {
                            _result = false;
                        }
                    }
                } catch (Exception e) {
                    _result = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        showDialog(context);
    }

    @Override
    protected void onPostExecute(Void result) {
        if (_result == true) {
            Toast.makeText(this.context, context.getString(R.string.sales_tovar_send_to_server_ok), Toast.LENGTH_LONG).show();
            Parse_http_result _prs = new Parse_http_result();
            _prs.parse_result(context, "Cancel_sale", jsonObjRecv);
//            Toast.makeText(this.context, context.getString(R.string.sales_tovar_send_to_server_ok), Toast.LENGTH_LONG).show();
        } else {
            String _error = "";
            try {
                _error = jsonObjRecv.get("message").toString();
            } catch (Exception e) {
            }
            Toast.makeText(this.context, _error, Toast.LENGTH_LONG).show();
        }
        closeDialog();
    }

    private void closeDialog() {
        if (progressdlg != null) {
            progressdlg.dismiss();
        }
    }

    private void showDialog(Context context) {
        if (progressdlg != null) {
            try {
                progressdlg.dismiss();
                progressdlg.cancel();
                progressdlg = null;
            } catch (Exception e) {
            }
        }
        if (progressdlg == null) {
            progressdlg = new ProgressDialog(context);
            progressdlg.setMessage(context.getString(R.string.app_progress_send_sale));
            try {
                progressdlg.show();
            } catch (Exception e) {
            }
        }
    }
}

