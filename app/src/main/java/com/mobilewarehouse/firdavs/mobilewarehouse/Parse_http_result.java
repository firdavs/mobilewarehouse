package com.mobilewarehouse.firdavs.mobilewarehouse;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

/**
 * Created by Firdavs on 27.02.2015.
 */
public class Parse_http_result {
    String _tmp;
    JSONArray _json_arr, _json_prices;
    JSONObject _json_obj;
    ArrayAdapter<String> adapter = null;

    public void parse_result(Context ctx, String result, JSONObject jsonObjRecv) {
        try {
            _tmp = jsonObjRecv.get("Has_result").toString();
        } catch (Exception e) {
        }

        if (_tmp.equalsIgnoreCase("true")) {
            if (result.equalsIgnoreCase("List_clients")) { //clienti
                try {
                    _tmp = jsonObjRecv.get("Members").toString();
                    _json_arr = new JSONArray(_tmp);
                } catch (Exception e) {
                }

                //perepbyalyaem peremennye
                Sales_Activity._myLayout.client_name = new String[_json_arr.length()];
                Sales_Activity._myLayout.client_account = new String[_json_arr.length()];
                Sales_Activity._myLayout.client_balance = new String[_json_arr.length()];
                //v cikle 4itaem dannie kazhdogo tovara
                for (int i = 0; i < _json_arr.length(); i++) {
                    try {
                        _json_obj = _json_arr.getJSONObject(i);
                        Sales_Activity._myLayout.client_name[i] = _json_obj.get("FIO").toString();
                        Sales_Activity._myLayout.client_account[i] = _json_obj.get("Account").toString();
                        Sales_Activity._myLayout.client_balance[i] = _json_obj.get("Balance").toString();
                    } catch (Exception e) {
                    }
                }

                if (Operations_menuActivity._myLayout._operation.equalsIgnoreCase("_sales")
                        || Operations_menuActivity._myLayout._operation.equalsIgnoreCase("_man2man")
                        || Operations_menuActivity._myLayout._operation.equalsIgnoreCase("_vozvrat")) {
                    adapter = new ArrayAdapter<String>(Sales_Activity._myLayout, android.R.layout.simple_dropdown_item_1line, Sales_Activity._myLayout.client_name);
                    Sales_Activity._myLayout.txt_client.setAdapter(null);
                    Sales_Activity._myLayout.txt_client.setAdapter(adapter);
                    Sales_Activity._myLayout.show_drop_down_list(1);
                    ////150506 if (_json_arr.length() > 0) Sales_Activity._myLayout.txtchanged = false;
                } else if (Operations_menuActivity._myLayout._operation.equalsIgnoreCase("_kassa")) {
                    adapter = new ArrayAdapter<String>(Priem_sredstvActivity._myLayout, android.R.layout.simple_dropdown_item_1line, Sales_Activity._myLayout.client_name);
                    Priem_sredstvActivity._myLayout.txt_client.setAdapter(null);
                    Priem_sredstvActivity._myLayout.txt_client.setAdapter(adapter);
                    Priem_sredstvActivity._myLayout.show_drop_down_list(1);
                    if (_json_arr.length() > 0) Priem_sredstvActivity._myLayout.txtchanged = false;
                } else if (Operations_menuActivity._myLayout._operation.equalsIgnoreCase("_otchet_clients")) {
                    adapter = new ArrayAdapter<String>(Otchet_record_poles._myLayout, android.R.layout.simple_dropdown_item_1line, Sales_Activity._myLayout.client_name);
                    Otchet_record_poles._myLayout.txt_klient.setAdapter(null);
                    Otchet_record_poles._myLayout.txt_klient.setAdapter(adapter);
                    Otchet_record_poles._myLayout.show_drop_down_list(1);
                    if (_json_arr.length() > 0) Otchet_record_poles._myLayout.txtchanged = false;
                }

            } else if (result.equalsIgnoreCase("List_tovari")) { //tovari
                try {
                    _tmp = jsonObjRecv.get("Members").toString();
                    _json_arr = new JSONArray(_tmp);
                } catch (Exception e) {
                }

                //pereobyavlyaem peremennye
                Sales_Activity._myLayout.tovar_name = new String[_json_arr.length()];
                Sales_Activity._myLayout.tovar_id = new String[_json_arr.length()];
                Sales_Activity._myLayout.tovar_price = new String[_json_arr.length()][];
                Sales_Activity._myLayout.tovar_ostatok = new String[_json_arr.length()];
                Sales_Activity._myLayout.default_tovar_price = new String[_json_arr.length()];

                //v cikle 4itaem dannie kazhdogo tovara
                for (int i = 0; i < _json_arr.length(); i++) {
                    try {
                        _json_obj = _json_arr.getJSONObject(i);
                        Sales_Activity._myLayout.tovar_name[i] = _json_obj.get("Naimenovanie").toString();
                        Sales_Activity._myLayout.tovar_id[i] = _json_obj.get("Id").toString();
                        _json_prices = new JSONArray(_json_obj.get("Price").toString());
                        Sales_Activity._myLayout.tovar_price[i] = new String[_json_prices.length()];
                        for (int j = 0; j < _json_prices.length(); j++) {
                            Sales_Activity._myLayout.tovar_price[i][j] = _json_prices.get(j).toString();
                        }
                        Sales_Activity._myLayout.tovar_ostatok[i] = _json_obj.get("Ostatok").toString();
                        Sales_Activity._myLayout.default_tovar_price[i] = _json_obj.get("Default").toString();
                    } catch (Exception e) {
                    }
                }

                adapter = new ArrayAdapter<String>(Sales_Activity._myLayout, android.R.layout.simple_dropdown_item_1line, Sales_Activity._myLayout.tovar_name);
                if (Sales_Activity._myLayout.dont_show_list == false) {
                    Sales_Activity._myLayout.txt_tovar.setAdapter(null);
                    Sales_Activity._myLayout.txt_tovar.setAdapter(adapter);
                    Sales_Activity._myLayout.show_drop_down_list(2);

                    ////150506 if (_json_arr.length() > 0) Sales_Activity._myLayout.txtchanged = false;
                } else {
                    Sales_Activity.dont_show_list = false;
                }

                //pri nabore nazvaniya tovara, zanovo popolnyaem peremennye
                Sales_Activity._myLayout.list_tovar_name = new String[_json_arr.length()];
                Sales_Activity._myLayout.list_tovar_id = new String[_json_arr.length()];
                Sales_Activity._myLayout.list_tovar_price = new String[_json_arr.length()][_json_arr.length()];
                Sales_Activity._myLayout.list_tovar_ostatok = new String[_json_arr.length()];

                Sales_Activity._myLayout.list_tovar_name = Sales_Activity._myLayout.tovar_name;
                Sales_Activity._myLayout.list_tovar_id = Sales_Activity._myLayout.tovar_id;
                Sales_Activity._myLayout.list_tovar_price = Sales_Activity._myLayout.tovar_price;
                Sales_Activity._myLayout.list_tovar_ostatok = Sales_Activity._myLayout.tovar_ostatok;
                //}

                if (Operations_menuActivity._myLayout._operation.equalsIgnoreCase("_proiz")) {
                    Proizvodtsvo_Activity._myLayout.tovar_id = new String[Sales_Activity._myLayout.tovar_id.length];
                    Proizvodtsvo_Activity._myLayout.tovar_name = new String[Sales_Activity._myLayout.tovar_name.length];

                    Proizvodtsvo_Activity._myLayout.tovar_id = Sales_Activity._myLayout.tovar_id;
                    Proizvodtsvo_Activity._myLayout.tovar_name = Sales_Activity._myLayout.tovar_name;
                }
            } else if (result.equalsIgnoreCase("List_statyi")) { //spisok statyei rasxoda
                if(!ctx.getClass().getSimpleName().equalsIgnoreCase("Settings_Activity")) {
                    try {
                        _tmp = jsonObjRecv.get("Members").toString();
                        _json_arr = new JSONArray(_tmp);
                    } catch (Exception e) {
                    }

                    //perepbyalyaem peremennye
                    Rasxodi_Activity._myLayout.list_statya = new String[_json_arr.length()];
                    Rasxodi_Activity._myLayout.list_statya_id = new String[_json_arr.length()];
                    Rasxodi_Activity._myLayout.statya_price = new String[_json_arr.length()][];

                    for (int i = 0; i < _json_arr.length(); i++) {
                        try {
                            _json_obj = _json_arr.getJSONObject(i);
                            Rasxodi_Activity._myLayout.list_statya[i] = _json_obj.get("name").toString();
                            Rasxodi_Activity._myLayout.list_statya_id[i] = _json_obj.get("id").toString();

                            _json_prices = new JSONArray(_json_obj.get("Price").toString());
                            Rasxodi_Activity._myLayout.statya_price[i] = new String[_json_prices.length()];
                            for (int j = 0; j < _json_prices.length(); j++) {
                                Rasxodi_Activity._myLayout.statya_price[i][j] = _json_prices.get(j).toString();
                            }
                        } catch (Exception e) {
                        }
                    }
                }else{
                    LoginActivity._myLayout.keys_editor = LoginActivity._myLayout.keys.edit();
                    LoginActivity._myLayout.keys_editor.putString("List_statyi", jsonObjRecv.toString());
                    try {
                        LoginActivity._myLayout.keys_editor.commit();
                    } catch (Exception err) {
                        LoginActivity._myLayout.generate_message(ctx.getString(R.string.app_error2), err.getMessage().toString());
                    }

                    JSONObject jsonObjSend = new JSONObject();
                    try {
                        jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
                        jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Url_Params params = null;
                    params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/getListOfFas/", jsonObjSend);
                    http_fill_list_tsex fill_list_tsex = new http_fill_list_tsex(ctx);
                    fill_list_tsex.execute(params);

//                    Settings_Activity._myLayout.chk_sync_clients.setChecked(false);
//                    Toast.makeText(ctx, ctx.getString(R.string.app_sync_marshrut_ok), Toast.LENGTH_LONG).show();
                }
            } else if (result.equalsIgnoreCase("List_fas")) { //tsexi, fasovshiki
                if(!ctx.getClass().getSimpleName().equalsIgnoreCase("Settings_Activity")) {
                    try {
                        _tmp = jsonObjRecv.get("Members").toString();
                        _json_arr = new JSONArray(_tmp);
                    } catch (Exception e) {
                    }

                    //perepbyalyaem peremennye
                    Proizvodtsvo_Activity._myLayout.tsex_id = new String[_json_arr.length()];
                    Proizvodtsvo_Activity._myLayout.tsex_name = new String[_json_arr.length()];
                    //
                    for (int i = 0; i < _json_arr.length(); i++) {
                        try {
                            _json_obj = _json_arr.getJSONObject(i);
                            Proizvodtsvo_Activity._myLayout.tsex_id[i] = _json_obj.get("schet").toString();
                            Proizvodtsvo_Activity._myLayout.tsex_name[i] = _json_obj.get("fio").toString();
                        } catch (Exception e) {
                        }
                    }
                }else{
                    LoginActivity._myLayout.keys_editor = LoginActivity._myLayout.keys.edit();
                    LoginActivity._myLayout.keys_editor.putString("List_fas", jsonObjRecv.toString());
                    try {
                        LoginActivity._myLayout.keys_editor.commit();
                    } catch (Exception err) {
                        LoginActivity._myLayout.generate_message(ctx.getString(R.string.app_error2), err.getMessage().toString());
                    }

                    JSONObject jsonObjSend = new JSONObject();
                    try {
                        jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
                        jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
                        jsonObjSend.put("Sklad_id", LoginActivity._myLayout.sklad_id);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Url_Params params = null;
                    params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/getListOfTovarsOfSklad/", jsonObjSend);
                    http_fill_list_tsex fill_list_tsex = new http_fill_list_tsex(ctx);
                    fill_list_tsex.execute(params);

//                    Settings_Activity._myLayout.chk_sync_clients.setChecked(false);
//                    Toast.makeText(ctx, ctx.getString(R.string.app_sync_marshrut_ok), Toast.LENGTH_LONG).show();
                }
            } else if (result.equalsIgnoreCase("List_sklads")) { //tsexi
                try {
                    _tmp = jsonObjRecv.get("Members").toString();
                    _json_arr = new JSONArray(_tmp);
                } catch (Exception e) {
                }

                //perepbyalyaem peremennye
                Proizvodtsvo_Activity._myLayout.tsex_id = new String[_json_arr.length()];
                Proizvodtsvo_Activity._myLayout.tsex_name = new String[_json_arr.length()];
                //
                for (int i = 0; i < _json_arr.length(); i++) {
                    try {
                        _json_obj = _json_arr.getJSONObject(i);
                        Proizvodtsvo_Activity._myLayout.tsex_id[i] = _json_obj.get("sklad_id").toString();
                        Proizvodtsvo_Activity._myLayout.tsex_name[i] = _json_obj.get("name").toString();
                    } catch (Exception e) {
                    }
                }
            } else if (result.equalsIgnoreCase("List_tovars")) { //tovari sklada
                if(!ctx.getClass().getSimpleName().equalsIgnoreCase("Settings_Activity")) {
                    try {
                        _tmp = jsonObjRecv.get("Members").toString();
                        _json_arr = new JSONArray(_tmp);
                    } catch (Exception e) {
                    }

                    Proizvodtsvo_Activity._myLayout.tovar_id = new String[_json_arr.length()];
                    Proizvodtsvo_Activity._myLayout.tovar_name = new String[_json_arr.length()];
                    //
                    for (int i = 0; i < _json_arr.length(); i++) {
                        try {
                            _json_obj = _json_arr.getJSONObject(i);
                            Proizvodtsvo_Activity._myLayout.tovar_id[i] = _json_obj.get("tovar_id").toString();
                            Proizvodtsvo_Activity._myLayout.tovar_name[i] = _json_obj.get("name").toString();
                        } catch (Exception e) {
                        }
                    }
                }else{
                    LoginActivity._myLayout.keys_editor = LoginActivity._myLayout.keys.edit();
                    LoginActivity._myLayout.keys_editor.putString("List_tovars_of_sklad", jsonObjRecv.toString());
                    try {
                        LoginActivity._myLayout.keys_editor.commit();
                    } catch (Exception err) {
                        LoginActivity._myLayout.generate_message(ctx.getString(R.string.app_error2), err.getMessage().toString());
                    }

                    Settings_Activity._myLayout.chk_sync_clients.setChecked(false);
                    Toast.makeText(ctx, ctx.getString(R.string.app_sync_marshrut_ok), Toast.LENGTH_LONG).show();
                }
            } else if (result.equalsIgnoreCase("Sale")) {
                if(!ctx.getClass().getSimpleName().equalsIgnoreCase("Settings_Activity")) {
                    //chistim dannye iz tablitsi s zapomnennym _tranzaction
                    Sales_Activity._myLayout.clear_tables();
                    //меняем остаток клиента в списке из маршрут листа
                    try {
                        _tmp = jsonObjRecv.get("Saldo").toString();
                        _tmp = _tmp.replace(".0000", "");
                        Marshrut_records_accountsActivity._myLayout.ms_client_balance[Marshrut_records_accountsActivity._myLayout.arr_position] = _tmp;

                        //povtornyy zaezd, rangash zard
                        if(Sales_Activity._myLayout.repeat_order) {
                            Marshrut_records_accountsActivity._myLayout.ms_client_status[Marshrut_records_accountsActivity._myLayout.arr_position] = "2";
                            LoginActivity._myLayout.set_red_accounts(1, Marshrut_records_accountsActivity._myLayout.ms_client_account[Marshrut_records_accountsActivity._myLayout.arr_position]);
                        }else{
                            //baroi strokaya kabud nishon dodan
                            Marshrut_records_accountsActivity._myLayout.ms_client_status[Marshrut_records_accountsActivity._myLayout.arr_position] = "1";
                            LoginActivity._myLayout.set_red_accounts(0, Marshrut_records_accountsActivity._myLayout.ms_client_account[Marshrut_records_accountsActivity._myLayout.arr_position]);
                        }
                    } catch (Exception e) {}

                    if (!Sales_Activity._myLayout.sales_client_prixod.equalsIgnoreCase("0")) {
                        JSONObject jsonObjSend = new JSONObject();
                        try {
                            jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
                            jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
                            jsonObjSend.put("Client", Sales_Activity._myLayout.sales_client_account);
                            jsonObjSend.put("Sum", Sales_Activity._myLayout.sales_client_prixod);
                            jsonObjSend.put("Comment", "Отпуск товара. Приход от клиента.");

                            Log.d("json", jsonObjSend.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/prixodDeneg/", jsonObjSend);
                        http_send_kassoviy_order kassa_me = new http_send_kassoviy_order(Sales_Activity._myLayout);
                        kassa_me.execute(params);
                    } else {
                        Sales_Activity._myLayout.show_client_list();
                        ((Activity) ctx).finish();
                    }
                }else{
                    Settings_Activity._myLayout.clear_tables(Settings_Activity._myLayout._type_operation);
                    Settings_Activity._myLayout.send_operations();
                }
            } else if (result.equalsIgnoreCase("Proiz")) {
                if(!ctx.getClass().getSimpleName().equalsIgnoreCase("Settings_Activity")) {
                    //4istim tablitsi
                    //operaton_common_info
                    //operation_info
                    Proizvodtsvo_Activity._myLayout.clear_tables();
                    Proizvodtsvo_Activity._myLayout.fill_tovar_list();
                    Proizvodtsvo_Activity._myLayout.clear_fields();
                    Proizvodtsvo_Activity._myLayout.txt_tsex.setEnabled(true);
                    Proizvodtsvo_Activity._myLayout.txt_tsex.setText("");
                }else{
                    Settings_Activity._myLayout.clear_tables(Settings_Activity._myLayout._type_operation);
                    Settings_Activity._myLayout.send_operations();
                }
            } else if (result.equalsIgnoreCase("Kassoviy_order")) {
                if(!ctx.getClass().getSimpleName().equalsIgnoreCase("Settings_Activity")) {
                    if (Priem_sredstvActivity._myLayout != null) {
                        Priem_sredstvActivity._myLayout.clear_fields();
                        Priem_sredstvActivity._myLayout.txtchanged = true;
                    }
                    //меняем остаток клиента в списке из маршрут листа
                    try {
                        _tmp = jsonObjRecv.get("Saldo").toString();
                        _tmp = _tmp.replace(".0000", "");
                        Marshrut_records_accountsActivity._myLayout.ms_client_balance[Marshrut_records_accountsActivity._myLayout.arr_position] = _tmp;
                        //baroi strokaya kabud nishon dodan
                        Marshrut_records_accountsActivity._myLayout.ms_client_status[Marshrut_records_accountsActivity._myLayout.arr_position] = "1";
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e("error", e.getMessage().toString());
                    }
                    //show marshrut list, if class referrer is sale_activity
                    if(ctx.getClass().getSimpleName().equalsIgnoreCase("Sales_Activity")){
                        Sales_Activity._myLayout.show_client_list();
                        ((Activity) ctx).finish();
                    }
                }else{
                    Settings_Activity._myLayout.clear_tables(Settings_Activity._myLayout._type_operation);
                    Settings_Activity._myLayout.send_operations();
                }
            } else if (result.equalsIgnoreCase("Rasxodi")) {
                if(!ctx.getClass().getSimpleName().equalsIgnoreCase("Settings_Activity")) {
                    Rasxodi_Activity._myLayout.clear_fields();
                }else{
                    Settings_Activity._myLayout.clear_tables(Settings_Activity._myLayout._type_operation);
                    Settings_Activity._myLayout.send_operations();
                }
            } else if(result.equalsIgnoreCase("Cancel_sale")){
                if(!ctx.getClass().getSimpleName().equalsIgnoreCase("Settings_Activity")) {
                    Toast.makeText(ctx, ctx.getString(R.string.sales_tovar_send_to_server_ok), Toast.LENGTH_LONG).show();
                }else{
                    Settings_Activity._myLayout.clear_tables(Settings_Activity._myLayout._type_operation);
                    Settings_Activity._myLayout.send_operations();
                }
            }else if (result.equalsIgnoreCase("Jurnal_records")) {
                try {
                    _tmp = jsonObjRecv.get("Members").toString();
                    _json_arr = new JSONArray(_tmp);
                } catch (Exception e) {
                }

                //pereobyavlyaem peremennye
                Jurnal_recordsActivity._myLayout.jn_operation_name = new String[_json_arr.length()];
                Jurnal_recordsActivity._myLayout.jn_operation_id = new String[_json_arr.length()];
                Jurnal_recordsActivity._myLayout.jn_operation_type = new String[_json_arr.length()];
                Jurnal_recordsActivity._myLayout.jn_operation_sum = new String[_json_arr.length()];
                Jurnal_recordsActivity._myLayout.jn_operation_client = new String[_json_arr.length()];
                Jurnal_recordsActivity._myLayout.jn_operation_date = new String[_json_arr.length()];
                Jurnal_recordsActivity._myLayout.jn_operation_img = new Integer[_json_arr.length()];

                //v cikle 4itaem dannie kazhdoy zapisi
                for (int i = 0; i < _json_arr.length(); i++) {
                    try {
                        _json_obj = _json_arr.getJSONObject(i);
                        Jurnal_recordsActivity._myLayout.jn_operation_name[i] = _json_obj.get("Operation").toString();
                        Jurnal_recordsActivity._myLayout.jn_operation_id[i] = _json_obj.get("Operation_id").toString();
                        Jurnal_recordsActivity._myLayout.jn_operation_type[i] = _json_obj.get("Type").toString();
                        Jurnal_recordsActivity._myLayout.jn_operation_sum[i] = _json_obj.get("Sum").toString();
                        Jurnal_recordsActivity._myLayout.jn_operation_client[i] = _json_obj.get("Client").toString();
                        Jurnal_recordsActivity._myLayout.jn_operation_date[i] = _json_obj.get("Date").toString();

                        /*
                        switch(Integer.parseInt(Jurnal_recordsActivity._myLayout.jn_operation_type[i]))
                        {
                            case 1006:     //prodaja tovarov
                                Jurnal_recordsActivity._myLayout.img = ctx.getResources().getIdentifier("operations", "drawable", "com.mobilewarehouse.firdavs.mobilewarehouse");
                                break;
                            case 1007:     //prixodniy kassoviy order
                                Jurnal_recordsActivity._myLayout.img = ctx.getResources().getIdentifier("prixodniy_order", "drawable", "com.mobilewarehouse.firdavs.mobilewarehouse");
                                break;
                            case 1008:    //rasxodi
                                Jurnal_recordsActivity._myLayout.img = ctx.getResources().getIdentifier("rasxodi", "drawable", "com.mobilewarehouse.firdavs.mobilewarehouse");
                                break;
                            default:    //vozvrat
                                Jurnal_recordsActivity._myLayout.img = ctx.getResources().getIdentifier("vozvrat", "drawable", "com.mobilewarehouse.firdavs.mobilewarehouse");
                                break;
                        }
                        Jurnal_recordsActivity._myLayout.jn_operation_img[i] = Jurnal_recordsActivity._myLayout.img;
                        */
                    } catch (Exception e) {
                    }
                }

                Intent intent = new Intent(ctx.getApplicationContext(), Jurnal_recordsActivity.class);
                ctx.startActivity(intent);

            } else if (result.equalsIgnoreCase("Jurnal_record_info")) { //jurnal record info
                try {
                    _tmp = jsonObjRecv.get("Members").toString();
                    _json_arr = new JSONArray(_tmp);
                } catch (Exception e) {
                }

                //pereobyavlyaem peremennye
                Jurnal_record_infoActivity._myLayout.jni_tovar = new String[_json_arr.length()];
                Jurnal_record_infoActivity._myLayout.jni_kolvo = new String[_json_arr.length()];
                Jurnal_record_infoActivity._myLayout.jni_tsena = new String[_json_arr.length()];

                try {
                    Jurnal_record_infoActivity._myLayout.jni_client_name = jsonObjRecv.get("Client").toString();
                    Jurnal_record_infoActivity._myLayout.jni_date = jsonObjRecv.get("Date").toString();
                    Jurnal_record_infoActivity._myLayout.jni_opn_name = jsonObjRecv.get("Operation").toString();
                } catch (Exception e) {
                }

                //v cikle 4itaem dannie kazhdogo tovara
                for (int i = 0; i < _json_arr.length(); i++) {
                    try {
                        _json_obj = _json_arr.getJSONObject(i);
                        //_json_prices = new JSONArray(_json_obj.get("Members").toString());
                        //for (int j = 0; j < _json_prices.length(); j++) {
                        //    _json_obj = _json_prices.getJSONObject(i);
                        Jurnal_record_infoActivity._myLayout.jni_tovar[i] = _json_obj.get("Naimenovanie").toString();
                        Jurnal_record_infoActivity._myLayout.jni_kolvo[i] = _json_obj.get("Count").toString();
                        Jurnal_record_infoActivity._myLayout.jni_tsena[i] = _json_obj.get("Price").toString();
                        //}
                    } catch (Exception e) {
                    }
                }

                Intent intent = new Intent(ctx.getApplicationContext(), Jurnal_record_infoActivity.class);
                ctx.startActivity(intent);
            } else if (result.equalsIgnoreCase("Otchet_records")) {
                try {
                    _tmp = jsonObjRecv.get("Members").toString();
                    _json_arr = new JSONArray(_tmp);
                } catch (Exception e) {
                }

                //pereobyavlyaem peremennye
                Otchet_recordsActivity._myLayout.ot_name = new String[_json_arr.length()];
                Otchet_recordsActivity._myLayout.ot_id = new String[_json_arr.length()];
                Otchet_recordsActivity._myLayout.ot_p1_source = new String[_json_arr.length()];
                Otchet_recordsActivity._myLayout.ot_p1 = new Boolean[_json_arr.length()];
                Otchet_recordsActivity._myLayout.ot_p2 = new Boolean[_json_arr.length()];
                Otchet_recordsActivity._myLayout.ot_p3 = new Boolean[_json_arr.length()];
                Otchet_recordsActivity._myLayout.ot_img = new Integer[_json_arr.length()];

                //v cikle 4itaem dannie kazhdoy zapisi
                for (int i = 0; i < _json_arr.length(); i++) {
                    try {
                        _json_obj = _json_arr.getJSONObject(i);
                        Otchet_recordsActivity._myLayout.ot_name[i] = _json_obj.get("Naimenovanie").toString();
                        Otchet_recordsActivity._myLayout.ot_id[i] = _json_obj.get("id").toString();
                        Otchet_recordsActivity._myLayout.ot_p1_source[i] = _json_obj.get("Source").toString();
                        Otchet_recordsActivity._myLayout.ot_p1[i] = Boolean.valueOf(_json_obj.get("Param1").toString());
                        Otchet_recordsActivity._myLayout.ot_p2[i] = Boolean.valueOf(_json_obj.get("Param2").toString());
                        Otchet_recordsActivity._myLayout.ot_p3[i] = Boolean.valueOf(_json_obj.get("Param3").toString());
                    } catch (Exception e) {
                    }
                }

                Intent intent = new Intent(ctx.getApplicationContext(), Otchet_recordsActivity.class);
                ctx.startActivity(intent);
            } else if (result.equalsIgnoreCase("Otchet_data")) {
                try {
                    _tmp = jsonObjRecv.get("html").toString();
                } catch (Exception e) {
                }
                Otchet_record_poles._myLayout.ot_html = _tmp;

                Intent intent = new Intent(ctx.getApplicationContext(), View_otchetActivity.class);
                ctx.startActivity(intent);
            } else if (result.equalsIgnoreCase("Tovar_info")) {
                try {
                    _tmp = jsonObjRecv.get("html").toString();
                } catch (Exception e) {
                }
                _tmp = "<html><head><link rel=stylesheet href='css/bootstrap.min.css'><style>.table td{font-size: 10px;} .table,body {padding: 0px; margin: 0px;}</style></head><body>" + _tmp + "</body></html>";
                Sales_Activity._myLayout.web_tovar_info.loadDataWithBaseURL("file:///android_asset/", _tmp, "text/html", "UTF-8", "");
            } else if (result.equalsIgnoreCase("Katalog")) {
                try {
                    _tmp = jsonObjRecv.get("Members").toString();
                    _json_arr = new JSONArray(_tmp);
                } catch (Exception e) {
                }

                //pereobyavlyaem peremennye
                Katalog_sliderActivity._myLayout.tovar_name = new String[_json_arr.length()];
                Katalog_sliderActivity._myLayout.tovar_description = new String[_json_arr.length()];
                Katalog_sliderActivity._myLayout.tovar_image = new String[_json_arr.length()];

                //v cikle 4itaem dannie kazhdoy zapisi
                for (int i = 0; i < _json_arr.length(); i++) {
                    try {
                        _json_obj = _json_arr.getJSONObject(i);
                        Katalog_sliderActivity._myLayout.tovar_name[i] = _json_obj.get("Naimenovanie").toString();
                        Katalog_sliderActivity._myLayout.tovar_description[i] = _json_obj.get("Opisanie").toString();
                        Katalog_sliderActivity._myLayout.tovar_image[i] = _json_obj.get("Image").toString();
                    } catch (Exception e) {
                    }
                }

                Intent intent = new Intent(ctx.getApplicationContext(), Katalog_sliderActivity.class);
                ctx.startActivity(intent);

                /*
                AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                builder.setTitle("katalog");
                builder.setMessage(_tmp);
                builder.setNegativeButton("cancel", null);
                builder.setCancelable(false);
                builder.create();
                builder.show();
                */
            } else if (result.equalsIgnoreCase("sync_marshrut_list")) {
                LoginActivity._myLayout.keys_editor = LoginActivity._myLayout.keys.edit();
                LoginActivity._myLayout.keys_editor.putString("List_marshrut", jsonObjRecv.toString());
                try {
                    LoginActivity._myLayout.keys_editor.commit();
                } catch (Exception err) {
                    LoginActivity._myLayout.generate_message(ctx.getString(R.string.app_error2), err.getMessage().toString());
                }

                try {
                    _json_arr = new JSONArray(jsonObjRecv.get("Members").toString());
                } catch (Exception e) {}


                if (_json_arr.length() > 0) {
                    LoginActivity._myLayout.sync_marshrut_names = new String[_json_arr.length()];
                    for (int i = 0; i < _json_arr.length(); i++) {
                        try {
                            _json_obj = _json_arr.getJSONObject(i);
                            LoginActivity._myLayout.sync_marshrut_names[i] = _json_obj.get("Naimenovanie").toString();
                        }catch(Exception e){}
                    }

                    LoginActivity._myLayout.sync_marshrut_clients = 0;
                    JSONObject jsonObjSend = new JSONObject();
                    try {
                        jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
                        jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
                        jsonObjSend.put("Name", LoginActivity._myLayout.sync_marshrut_names[LoginActivity._myLayout.sync_marshrut_clients]);
                        jsonObjSend.put("Type", "5");
                    } catch (JSONException e) {}

                    Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/getList/", jsonObjSend);
                    http_get_marshrut_list marshrut_record_info_me = new http_get_marshrut_list(ctx);
                    marshrut_record_info_me.execute(params);
                }
            } else if (result.equalsIgnoreCase("sync_marshrut_list_clients")) {
                if(LoginActivity._myLayout.sync_marshrut_clients < LoginActivity._myLayout.sync_marshrut_names.length - 1){
                    LoginActivity._myLayout.keys_editor = LoginActivity._myLayout.keys.edit();
                    LoginActivity._myLayout.keys_editor.putString(LoginActivity._myLayout.sync_marshrut_names[LoginActivity._myLayout.sync_marshrut_clients], jsonObjRecv.toString());
                    try {
                        LoginActivity._myLayout.keys_editor.commit();
                    } catch (Exception err) {
                        LoginActivity._myLayout.generate_message(ctx.getString(R.string.app_error2), err.getMessage().toString());
                    }

                    LoginActivity._myLayout.sync_marshrut_clients++;
                    JSONObject jsonObjSend = new JSONObject();
                    try {
                        jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
                        jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
                        jsonObjSend.put("Name", LoginActivity._myLayout.sync_marshrut_names[LoginActivity._myLayout.sync_marshrut_clients]);
                        jsonObjSend.put("Type", "5");
                    } catch (JSONException e) {}

                    Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/getList/", jsonObjSend);
                    http_get_marshrut_list marshrut_record_info_me = new http_get_marshrut_list(ctx);
                    marshrut_record_info_me.execute(params);
                }else{
                    //poluchaem spisok tovarov
                    if(LoginActivity._myLayout.sync_marshrut_clients == LoginActivity._myLayout.sync_marshrut_names.length - 1) {
                        LoginActivity._myLayout.sync_marshrut_clients++;
                        JSONObject jsonObjSend = new JSONObject();
                        try {
                            jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
                            jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
                            jsonObjSend.put("Like", "");
                            jsonObjSend.put("Type", "2");
                            jsonObjSend.put("Role", LoginActivity._myLayout.login_type);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/getList/", jsonObjSend);
                        http_get_marshrut_list marshrut_record_info_me = new http_get_marshrut_list(ctx);
                        marshrut_record_info_me.execute(params);
                    }else {
                        LoginActivity._myLayout.keys_editor = LoginActivity._myLayout.keys.edit();
                        LoginActivity._myLayout.keys_editor.putString("List_tovari", jsonObjRecv.toString());
                        try {
                            LoginActivity._myLayout.keys_editor.commit();
                        } catch (Exception err) {
                            LoginActivity._myLayout.generate_message(ctx.getString(R.string.app_error2), err.getMessage().toString());
                        }

                        //dalshe poluchaem spisok statey rasxodov
                        JSONObject jsonObjSend = new JSONObject();
                        try {
                            jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
                            jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/getListOfStatyiRasxoda/", jsonObjSend);
                        http_fill_list fill_list = new http_fill_list(Settings_Activity._myLayout);
                        fill_list.execute(params);
//                        Settings_Activity._myLayout.chk_sync_clients.setChecked(false);
//                        Toast.makeText(ctx, ctx.getString(R.string.app_sync_marshrut_ok), Toast.LENGTH_LONG).show();
                    }
                }
            } else if (result.equalsIgnoreCase("List_marshrut")) {
//                String _name = "";
                try {
//                    _name = jsonObjRecv.get("Name").toString();
                    _tmp = jsonObjRecv.get("Members").toString();
                    _json_arr = new JSONArray(_tmp);
                } catch (Exception e) {}

                Intent intent = null;
                if (_json_arr.length() > 0) {
                    //pereobyavlyaem peremennye
                    Marshrut_recordsActivity._myLayout.client_count = _json_arr.length();
                    Marshrut_recordsActivity._myLayout.ms_marshrut_name = new String[_json_arr.length()];
                    Marshrut_recordsActivity._myLayout.ms_client_name = new String[_json_arr.length()];
                    Marshrut_recordsActivity._myLayout.ms_client_account = new String[_json_arr.length()];
                    Marshrut_recordsActivity._myLayout.ms_client_balance = new String[_json_arr.length()];
                    Marshrut_recordsActivity._myLayout.ms_operation_img = new Integer[_json_arr.length()];

                    //v cikle 4itaem dannie kazhdoy zapisi
                    for (int i = 0; i < _json_arr.length(); i++) {
                        try {
                            _json_obj = _json_arr.getJSONObject(i);
                            Marshrut_recordsActivity._myLayout.ms_marshrut_name[i] = _json_obj.get("Naimenovanie").toString();
                            Marshrut_recordsActivity._myLayout.ms_client_name[i] = " ";
                            Marshrut_recordsActivity._myLayout.ms_client_account[i] = " ";
                            Marshrut_recordsActivity._myLayout.ms_client_balance[i] = _json_obj.get("Ostatok").toString();
                            Marshrut_recordsActivity._myLayout.ms_operation_type = 1;
                        } catch (Exception e) {
                        }
                    }

                    intent = new Intent(ctx.getApplicationContext(), Marshrut_recordsActivity.class);
                }

                //xranim takzhe spisok prichin otmeny zayavki
                try {
                    _tmp = jsonObjRecv.get("CancelList").toString();
                    _json_arr = new JSONArray(_tmp);
                } catch (Exception e) {}
                if (_json_arr.length() > 0) {
                    //pereobyavlyaem peremennye
                    Marshrut_recordsActivity._myLayout.cancel_list_id = new String[_json_arr.length()];
                    Marshrut_recordsActivity._myLayout.cancel_list = new String[_json_arr.length()];
                    for (int i = 0; i < _json_arr.length(); i++) {
                        try {
                            _json_obj = _json_arr.getJSONObject(i);
                            Marshrut_recordsActivity._myLayout.cancel_list_id[i] = _json_obj.get("Id").toString();
                            Marshrut_recordsActivity._myLayout.cancel_list[i] = _json_obj.get("Naimenovanie").toString();
                        } catch (Exception e) {}
                    }
                }

                ctx.startActivity(intent);
                //((Activity) ctx).finish();
            } else if (result.equalsIgnoreCase("List_marshrut_clients")) {
                String _name = "";
                try {
                    _name = jsonObjRecv.get("Name").toString();
                    _tmp = jsonObjRecv.get("Members").toString();
                    _json_arr = new JSONArray(_tmp);
                } catch (Exception e) {
                }

                Intent intent = null;
                if (_json_arr.length() > 0) {
                    //pereobyavlyaem peremennye
                    Marshrut_records_accountsActivity._myLayout.client_count = _json_arr.length();
                    Marshrut_records_accountsActivity._myLayout.ms_marshrut_name = new String[_json_arr.length()];
                    Marshrut_records_accountsActivity._myLayout.ms_client_name = new String[_json_arr.length()];
                    Marshrut_records_accountsActivity._myLayout.ms_client_account = new String[_json_arr.length()];
                    Marshrut_records_accountsActivity._myLayout.ms_client_balance = new String[_json_arr.length()];
                    Marshrut_records_accountsActivity._myLayout.ms_operation_img = new Integer[_json_arr.length()];
                    Marshrut_records_accountsActivity._myLayout.ms_client_address = new String[_json_arr.length()];
                    Marshrut_records_accountsActivity._myLayout.ms_client_status = new String[_json_arr.length()];

                    //v cikle 4itaem dannie kazhdoy zapisi
                    for (int i = 0; i < _json_arr.length(); i++) {
                        try {
                            _json_obj = _json_arr.getJSONObject(i);
                            Marshrut_records_accountsActivity._myLayout.ms_marshrut_name[i] = Marshrut_recordsActivity._myLayout.ms_marshrut;
                            Marshrut_records_accountsActivity._myLayout.ms_client_name[i] = _json_obj.get("FIO").toString();
                            Marshrut_records_accountsActivity._myLayout.ms_client_account[i] = _json_obj.get("Account").toString();
                            Marshrut_records_accountsActivity._myLayout.ms_client_balance[i] = _json_obj.get("Balance").toString();
                            Marshrut_records_accountsActivity._myLayout.ms_client_address[i] = _json_obj.get("Address").toString();
                            if(LoginActivity._myLayout.red_accounts.indexOf(Marshrut_records_accountsActivity._myLayout.ms_client_account[i] + ",") < 0) {
                                Marshrut_records_accountsActivity._myLayout.ms_client_status[i] = _json_obj.get("Status").toString();
                            }else{
                                Marshrut_records_accountsActivity._myLayout.ms_client_status[i] = "2";
                            }
                            Marshrut_records_accountsActivity._myLayout.ms_operation_type = 2;
                        } catch (Exception e) {
                        }
                    }

                    intent = new Intent(ctx.getApplicationContext(), Marshrut_records_accountsActivity.class);
                }
                ctx.startActivity(intent);
            } else if (result.equalsIgnoreCase("Send_photo_file")) {
                //snachalo udalyaem file
                File file_to_be_deleted = new File(Photo_Activity._myLayout.global_file_name);
                Boolean deleted = file_to_be_deleted.delete();
                if (deleted == false)
                    Toast.makeText(ctx, ctx.getString(R.string.photo_picture_deleted_not), Toast.LENGTH_LONG).show();
                //aktiviruem cameru
                View vew = null;
                try {
                    Photo_Activity._myLayout.onPhotoCancel(vew);    //vydaet error esli photoactivity finished
                } catch (Exception err) {
                    Toast.makeText(ctx, err.getMessage().toString(), Toast.LENGTH_LONG).show();
                }
                Toast.makeText(ctx, ctx.getString(R.string.photo_send_ok), Toast.LENGTH_LONG).show();
                Photo_Activity._myLayout.btn_photo_exit.setEnabled(true);
            }
        }
    }
}
