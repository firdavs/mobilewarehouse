package com.mobilewarehouse.firdavs.mobilewarehouse;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Firdavs on 20.02.2015.
 */
public class Main_menuActivity extends Activity {
    public static Main_menuActivity _myLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.slide_to_right, R.anim.slide_to_left);
        setContentView(R.layout.activity_menu_main);
        Main_menuActivity._myLayout = this;
        this.setTitle(LoginActivity._myLayout.app_title);

        ImageButton mnu_operation = (ImageButton) findViewById(R.id.btn_operation);
        ImageButton mnu_jurnal = (ImageButton) findViewById(R.id.btn_jurnal);
        ImageButton mnu_reports = (ImageButton) findViewById(R.id.btn_reports);
        ImageButton mnu_settings = (ImageButton) findViewById(R.id.btn_settings);
        ImageButton mnu_katalog = (ImageButton) findViewById(R.id.btn_katalog);
        ImageButton mnu_about = (ImageButton) findViewById(R.id.btn_about);

        mnu_operation.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().setColorFilter(0x77000000, PorterDuff.Mode.SRC_ATOP);
                        v.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        //on button click
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        v.invalidate();

                        start_intent(1);
                        break;
                    }
                    case MotionEvent.ACTION_CANCEL: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }
                return true;
            }
        });
        mnu_jurnal.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().setColorFilter(0x77000000, PorterDuff.Mode.SRC_ATOP);
                        v.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        //on button click
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        v.invalidate();

                        date_range_selector();
                        break;
                    }
                    case MotionEvent.ACTION_CANCEL: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }
                return true;
            }
        });
        mnu_reports.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().setColorFilter(0x77000000, PorterDuff.Mode.SRC_ATOP);
                        v.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        //on button click
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        v.invalidate();

                        get_otchet_records();
                        break;
                    }
                    case MotionEvent.ACTION_CANCEL: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }
                return true;
            }
        });
        mnu_katalog.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().setColorFilter(0x77000000, PorterDuff.Mode.SRC_ATOP);
                        v.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        //on button click
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        v.invalidate();

                        //start_intent(4);
                        get_katalog();
                        break;
                    }
                    case MotionEvent.ACTION_CANCEL: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }
                return true;
            }
        });
        mnu_settings.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().setColorFilter(0x77000000, PorterDuff.Mode.SRC_ATOP);
                        v.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        //on button click
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        v.invalidate();

                        start_intent(4);
                        break;
                    }
                    case MotionEvent.ACTION_CANCEL: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }
                return true;
            }
        });
        mnu_about.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().setColorFilter(0x77000000, PorterDuff.Mode.SRC_ATOP);
                        v.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        //on button click
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        v.invalidate();

                        start_intent(5);
                        break;
                    }
                    case MotionEvent.ACTION_CANCEL: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }
                return true;
            }
        });
    }

    public void start_intent(int _page) {
        Intent intent = null;
        switch (_page) {
            case 1: //operations
                intent = new Intent(getApplicationContext(), Operations_menuActivity.class);
                break;
            case 4: //settings
                intent = new Intent(getApplicationContext(), Settings_Activity.class);
                break;
            case 5: //about
                intent = new Intent(getApplicationContext(), About_Activity.class);
                break;
            default:
                intent = new Intent(getApplicationContext(), Main_menuActivity.class);
                break;
        }
        startActivity(intent);
    }

    void date_range_selector() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Main_menuActivity.this);
        builder.setTitle(getString(R.string.app_select_period)); // viberite period, pjl

        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.date_range_form, null);
        builder.setView(view);

        DatePicker _dt_ot = (DatePicker) view.findViewById(R.id.jurnal_dt_ot);
        DatePicker _dt_po = (DatePicker) view.findViewById(R.id.jurnal_dt_po);

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // set current date into datepicker
        _dt_ot.init(year, month, day, null);
        _dt_po.init(year, month, day, null);

        builder.setNegativeButton(getString(R.string.app_cancel), new DialogInterface.OnClickListener() {
            //@Override
            public void onClick(DialogInterface dialog, int item) {
                dialog.cancel();
            }
        });
        builder.setPositiveButton(getString(R.string.app_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                Dialog f = (Dialog) dialog;
                String _jurnal_period_ot, _jurnal_period_po;

                DatePicker _dt_ot = (DatePicker) f.findViewById(R.id.jurnal_dt_ot);
                DatePicker _dt_po = (DatePicker) f.findViewById(R.id.jurnal_dt_po);

                int day = _dt_ot.getDayOfMonth();
                int month = _dt_ot.getMonth();
                int year = _dt_ot.getYear() - 1900;

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                _jurnal_period_ot = sdf.format(new Date(year, month, day));

                day = _dt_po.getDayOfMonth();
                month = _dt_po.getMonth();
                year = _dt_po.getYear() - 1900;

                _jurnal_period_po = sdf.format(new Date(year, month, day));
                //http
                get_jurnal_records(_jurnal_period_ot, _jurnal_period_po);
            }
        });

        builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    void get_jurnal_records(String _date1, String _date2) {
        JSONObject jsonObjSend = new JSONObject();
        try {
            jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
            jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
            jsonObjSend.put("Date1", _date1 + " 00:00:00");
            jsonObjSend.put("Date2", _date2 + " 23:59:59");
            jsonObjSend.put("Type", "1");

            Log.d("json", jsonObjSend.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/getOperations/", jsonObjSend);
        http_get_jurnal_records jurnal_records_me = new http_get_jurnal_records(Main_menuActivity.this);
        jurnal_records_me.execute(params);
    }

    void get_katalog() {
        JSONObject jsonObjSend = new JSONObject();
        try {
            jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
            jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/getListOfAllProducts/", jsonObjSend);
        http_get_katalog katalog_me = new http_get_katalog(Main_menuActivity.this);
        katalog_me.execute(params);
    }

    void get_otchet_records() {
        JSONObject jsonObjSend = new JSONObject();
        try {
            jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
            jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
            jsonObjSend.put("Type", "1");

            Log.d("json", jsonObjSend.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/getListOfReports/", jsonObjSend);
        http_get_otchet_records otchet_records_me = new http_get_otchet_records(Main_menuActivity.this);
        otchet_records_me.execute(params);
    }

    //testing gps
    void get_location() {
        LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean enabled = service
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        // check if enabled and if not send user to the GSP settings
        // Better solution would be to display a dialog and suggesting to
        // go to the settings
        Intent intent = null;
        if (!enabled) {
            intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        } else {
            //intent = new Intent(getApplicationContext(), GetLocation_Activity.class);
            //startService(intent);
            //startService(new Intent(this, GetLocation_Activity.class));
            GetLocation_Activity gps = new GetLocation_Activity(this);
            //double lat = gps.latitude;
            //double lon = gps.longitude;
        }
    }
    //test

    ///////// WORKING WITH BACK BUTTON //////////////////////////////////////
    @Override
    public void onBackPressed() {
        finish();
        //overridePendingTransition(R.anim.slide_to_right, R.anim.slide_to_left);
    }

    //создаем меню
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        menu.add(Menu.NONE, LoginActivity.IDM_EXIT, menu.NONE, getString(R.string.app_menu4))
                .setIcon(android.R.drawable.ic_menu_revert);
        return (super.onCreateOptionsMenu(menu));
    }

    ;

    //обрабатываем выбор пункта меню
    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case LoginActivity.IDM_EXIT:
                System.exit(0);
                break;
            default:
                return false;
        }
        return true;
    }

    ;

    public static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
