package com.mobilewarehouse.firdavs.mobilewarehouse;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Firdavs on 26.02.2015.
 */
public class http_fill_list extends AsyncTask<Url_Params, Void, Void> {
    StringBuilder builder = new StringBuilder();
    Boolean _result = false;
    ProgressDialog progressdlg;
    private Context context;
    String _list;
    JSONObject jsonObjRecv;

    public http_fill_list(Context cxt) {
        context = cxt;
        progressdlg = new ProgressDialog(context);
    }

    //@Override
    protected Void doInBackground(Url_Params... params) {
        String url = "http://" + params[0].url;
        JSONObject jsonObjSend = params[0].jsonObjSend;

        try {
            DefaultHttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPostRequest = new HttpPost(url);


            List<NameValuePair> data = new ArrayList<NameValuePair>();
            data.add(new BasicNameValuePair("data", jsonObjSend.toString()));
            httpPostRequest.setEntity(new UrlEncodedFormEntity(data, "UTF-8"));

            HttpResponse response = httpclient.execute(httpPostRequest);

            // Get hold of the response entity (-> the data):
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                // Read the content stream
                InputStream instream = entity.getContent();
                // convert content stream to a String
                String resultString = Main_menuActivity._myLayout.convertStreamToString(instream);
                instream.close();

                try {
                    jsonObjRecv = new JSONObject(resultString);
                    if (jsonObjRecv != null) {
                        try {
                            if (jsonObjRecv.get("Has_result").toString().equalsIgnoreCase("true")) {
                                _list = jsonObjRecv.get("Name").toString();
                                if (_list.equalsIgnoreCase("List_clients")
                                        || _list.equalsIgnoreCase("List_tovari")
                                        || _list.equalsIgnoreCase("List_statyi")) {
                                    _result = true;
                                } else {
                                    _result = false;
                                }
                            }
                        } catch (Exception e) {
                            _result = false;
                        }
                    }
                } catch (Exception e) {
                    _result = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        showDialog(context);
    }

    @Override
    protected void onPostExecute(Void result) {
        if (_result == true) {
            Parse_http_result _prs = new Parse_http_result();
            _prs.parse_result(context, _list, jsonObjRecv);
        } else {
            //Toast.makeText(this.context, context.getString(R.string.app_login_failes), Toast.LENGTH_LONG).show();
            String _error = "";
            try {
                _error = jsonObjRecv.get("message").toString();
            } catch (Exception e) {
            }
            Toast.makeText(this.context, _error, Toast.LENGTH_LONG).show();

            //
            Sales_Activity._myLayout.tovar_name = null;
            Sales_Activity._myLayout.tovar_id = null;
            Sales_Activity._myLayout.tovar_price = null;
            Sales_Activity._myLayout.tovar_ostatok = null;
            Sales_Activity._myLayout.default_tovar_price = null;
        }
        closeDialog();
    }

    private void closeDialog() {
        if (progressdlg != null) {
            progressdlg.dismiss();
        }
    }

    private void showDialog(Context context) {
        if (progressdlg != null) {
            try {
                progressdlg.dismiss();
                progressdlg.cancel();
                progressdlg = null;
            } catch (Exception e) {
            }
        }
        if (progressdlg == null) {
            progressdlg = new ProgressDialog(context);
            progressdlg.setMessage(context.getString(R.string.app_progress_fill_list));
            try {
                progressdlg.show();
            } catch (Exception e) {
            }
        }

    }
}
