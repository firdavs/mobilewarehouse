package com.mobilewarehouse.firdavs.mobilewarehouse;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by Firdavs on 10.03.2015.
 */
public class View_otchetActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_otchet);
        this.setTitle(LoginActivity._myLayout.app_title);

        WebView web_ot = (WebView) findViewById(R.id.web_otchet);
        web_ot.getSettings().setLoadWithOverviewMode(true);
        web_ot.getSettings().setUseWideViewPort(true);
        web_ot.getSettings().setBuiltInZoomControls(true);
        web_ot.getSettings().setDisplayZoomControls(false);

        final Activity activity = this;
        web_ot.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                activity.setProgress(progress * 100);
            }
        });

        //zamenyaem slova na nazvanie otcheta
        if(Otchet_recordsActivity._myLayout.ot_name != null) {
            Otchet_record_poles._myLayout.ot_html = Otchet_record_poles.ot_html.replace("Параметры отчета", "&nbsp;" + Otchet_recordsActivity._myLayout.ot_name[Otchet_recordsActivity._myLayout.sel_otchet]);
        }else{  //vyzov chz formu prodaji
            Otchet_record_poles._myLayout.ot_html = Otchet_record_poles.ot_html.replace("Параметры отчета", "&nbsp;Выписка клиента:&nbsp;" + Sales_Activity._myLayout.txt_client.getText().toString());
        }
        Otchet_record_poles._myLayout.ot_html = "<html><head><link rel=stylesheet href='css/bootstrap.min.css'></head><body>" + Otchet_record_poles._myLayout.ot_html + "</body></html>";
        web_ot.loadDataWithBaseURL("file:///android_asset/", Otchet_record_poles._myLayout.ot_html, "text/html", "UTF-8", "");
    }

    ///////// WORKING WITH BACK BUTTON //////////////////////////////////////
    @Override
    public void onBackPressed() {
        finish();
    }

    //создаем меню
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        menu.add(Menu.NONE, LoginActivity.IDM_MAIN, menu.NONE, getString(R.string.app_menu2))
                .setIcon(android.R.drawable.ic_menu_revert);
        return (super.onCreateOptionsMenu(menu));
    }

    ;

    //обрабатываем выбор пункта меню
    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case LoginActivity.IDM_MAIN:
                finish();
                break;
            default:
                return false;
        }
        return true;
    }

    ;
}
