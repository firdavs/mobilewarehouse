package com.mobilewarehouse.firdavs.mobilewarehouse;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Firdavs on 09.03.2015.
 */
public class Marshrut_records_accountsActivity extends ListActivity {
    public static Marshrut_records_accountsActivity _myLayout;
    myAdapter ms_records_list;

    public static String[] ms_marshrut_name, ms_client_name, ms_client_account, ms_client_balance, ms_client_address, ms_client_status;
    public static int ms_operation_type = 0;
    public static String ms_marshrut;
    public static Integer[] ms_operation_img;
    int img = 0, arr_position = 0;
    public static int client_count = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //MainFormActivity.myLayout.set_theme(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mrsh_accounts_records);
        Marshrut_records_accountsActivity._myLayout = this;
        this.setTitle(LoginActivity._myLayout.app_title);

        get_images();

        TextView lbl_mrsh_name = (TextView) findViewById(R.id.lbl_mrsh_name);
        lbl_mrsh_name.setText(Marshrut_recordsActivity._myLayout.ms_marshrut + Marshrut_recordsActivity._myLayout.ms_marshrut_balance);

        ms_records_list = new myAdapter(this);
        setListAdapter(ms_records_list);
    }

    void get_images() {
        for (int i = 0; i < ms_marshrut_name.length; i++) {
            try {
                switch (_myLayout.ms_operation_type) {
                    case 1:     //marshrut list
                        img = getResources().getIdentifier("sales_j", "drawable", "com.mobilewarehouse.firdavs.mobilewarehouse");
                        break;
                    default:    //vozvrat
                        img = getResources().getIdentifier("man2man_j", "drawable", "com.mobilewarehouse.firdavs.mobilewarehouse");
                        break;
                }
                Marshrut_records_accountsActivity._myLayout.ms_operation_img[i] = img;
            } catch (Exception e) {
            }
        }
    }

    public void onListItemClick(ListView parent, View v, int position, long id) {
        //list item click haminja
        arr_position = position;
        Intent intent = null;
        if (LoginActivity._myLayout.operation_page == 1) {           //sales
            intent = new Intent(getApplicationContext(), Sales_Activity.class);
            Sales_Activity._myLayout.sclient_name = ms_client_name[position];
            Sales_Activity._myLayout.sclient_account = ms_client_account[position];
            Sales_Activity._myLayout.sales_client_account = ms_client_account[position];
            Sales_Activity._myLayout.sclient_balance = ms_client_balance[position];
            Sales_Activity._myLayout.txtchanged = false;
            //zapominaem account
            LoginActivity._myLayout.set_tmp_accounts(LoginActivity._myLayout.tmp_accounts, Sales_Activity._myLayout.sclient_account);
        } else if (LoginActivity._myLayout.operation_page == 3) {           //rasxody
            Rasxodi_Activity._myLayout.statya_name = ms_client_name[position];
            Rasxodi_Activity._myLayout.statya_id = ms_client_account[position];
            Rasxodi_Activity._myLayout.txt_statya.setText(Rasxodi_Activity._myLayout.statya_name);

            //fill_statya_prices_to_arr(item);
            Rasxodi_Activity._myLayout.stsex_id = Proizvodtsvo_Activity._myLayout.tsex_id[0];
            Rasxodi_Activity._myLayout.txt_kolvo.requestFocus();
        } else {  //LoginActivity._myLayout.operation_page = 2        //prixod ot clientov
            intent = new Intent(getApplicationContext(), Priem_sredstvActivity.class);
            Priem_sredstvActivity._myLayout.sclient_name = ms_client_name[position];
            Priem_sredstvActivity._myLayout.sclient_account = ms_client_account[position];
            Priem_sredstvActivity._myLayout.sclient_balance = ms_client_balance[position];
            Priem_sredstvActivity._myLayout.txtchanged = false;
            //zapominaem account
            LoginActivity._myLayout.set_tmp_accounts(LoginActivity._myLayout.tmp_accounts, Priem_sredstvActivity._myLayout.sclient_account);
        }

        if (LoginActivity._myLayout.operation_page != 3)
            startActivity(intent);
        finish();
    }

    void get_marshrut_record_info(String _marshrut_name) {
        JSONObject jsonObjSend = new JSONObject();
        try {
            jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
            jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
            jsonObjSend.put("Name", _marshrut_name);
            jsonObjSend.put("Type", "5");

            Log.d("json", jsonObjSend.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/getList/", jsonObjSend);
        http_get_marshrut_list marshrut_record_info_me = new http_get_marshrut_list(Marshrut_records_accountsActivity.this);
        marshrut_record_info_me.execute(params);
        //finish();
    }

    public class myAdapter extends BaseAdapter {
        private LayoutInflater mLayoutInflater;

        public myAdapter(Context ctx) {
            mLayoutInflater = LayoutInflater.from(ctx);
        }

        public int getCount() {
            //int cnt = 0;
            return client_count;
            /*
            if(ms_operation_type == 1) {
                cnt = ms_marshrut_name.length;
            }else{
                cnt = ms_client_name.length;
            }

            return cnt;
            */
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }


        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null)
                convertView = mLayoutInflater.inflate(R.layout.item_marshrut_account_record, null);

            if (ms_client_status[position].equalsIgnoreCase("1")) {
                convertView.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_light));
            }else if (ms_client_status[position].equalsIgnoreCase("2")) {
                convertView.setBackgroundColor(getResources().getColor(android.R.color.holo_red_light));
            } else {
                convertView.setBackgroundColor(getResources().getColor(android.R.color.white));
            }

            ImageView image = (ImageView) convertView.findViewById(R.id.img_mrsh_account);
            image.setImageResource(ms_operation_img[position]);

            TextView itm_op_name = (TextView) convertView.findViewById(R.id.itm_mrsh_account_name);
            itm_op_name.setText(ms_client_name[position]);

            TextView itm_op_client = (TextView) convertView.findViewById(R.id.itm_mrsh_account_account);
            itm_op_client.setText(ms_client_address[position]);

            TextView itm_op_summa = (TextView) convertView.findViewById(R.id.itm_mrhs_account_ostatok);
            itm_op_summa.setText(ms_client_balance[position]);
            return convertView;
        }
    }

    ///////// WORKING WITH BACK BUTTON //////////////////////////////////////
    @Override
    public void onBackPressed() {
        if (ms_operation_type == 2) {
            ms_operation_type = 1;
        }
        finish();
    }

    //создаем меню
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(Menu.NONE, LoginActivity.IDM_MAIN, menu.NONE, getString(R.string.app_menu2))
                .setIcon(android.R.drawable.ic_menu_revert);
        return (super.onCreateOptionsMenu(menu));
    }

    ;

    //обрабатываем выбор пункта меню
    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case LoginActivity.IDM_MAIN:
                finish();
                break;
            default:
                return false;
        }
        return true;
    }

    ;
}
