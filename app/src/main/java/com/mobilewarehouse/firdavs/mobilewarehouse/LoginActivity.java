package com.mobilewarehouse.firdavs.mobilewarehouse;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import dbhelper.ExternalDbOpenHelper;


public class LoginActivity extends Activity {
    public static final String DB_NAME = "base";
    public static SQLiteDatabase database;
    public static final int IDM_MAIN = 1;
    public static final int IDM_CANCEL = 2;
    public static final int IDM_BACK = 3;
    public static final int IDM_EXIT = 4;

    SharedPreferences keys;
    SharedPreferences.Editor keys_editor;

    public static String app_version = "1.0.7";
    public static String sklad_id = "13";
    public static String server_ip, login, passw, passw1, login_fio, app_title, login_type, login_ostatok, login_account;
    public static String tmp_accounts;
    public static String red_accounts;
    public static EditText txt_login, txt_password;
    public static TextView txt_sever_ip, txt_version, txt_app_mode;
    public static Switch geo_location;
    public static Boolean do_geo = false;
    public static LoginActivity _myLayout;
    //public static String filename_jurnal = "sklad_jurnal";
    public static String user_login, user_password;
    public static CheckBox chk_remember = null;
    public static int camera_resolution;
    public static int operation_page;
    public static boolean app_mode, syncing;
    public static int sync_marshrut_clients;
    public static String[] sync_marshrut_names;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        LoginActivity._myLayout = this;

        //rabotaems bd
        ExternalDbOpenHelper dbOpenHelper = new ExternalDbOpenHelper(this, DB_NAME);
        database = dbOpenHelper.openDataBase();
        //

        Button btn_login = (Button) findViewById(R.id.btn_login);
        Button btn_ip = (Button) findViewById(R.id.btn_ip);
        chk_remember = (CheckBox) findViewById(R.id.chk_remember);
        txt_login = (EditText) findViewById(R.id.edt_login);
        txt_password = (EditText) findViewById(R.id.edt_password);
        txt_sever_ip = (TextView) findViewById(R.id.txt_server_ip);
        txt_version = (TextView) findViewById(R.id.txt_version);
        txt_app_mode = (TextView) findViewById(R.id.txt_app_mode);
        geo_location = (Switch) findViewById(R.id.switch_geo);

        read_preferences();

        txt_login.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (txt_login.getRight() - txt_login.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        txt_login.setText("");
                        return true;
                    }
                }
                return false;
            }
        });

        txt_password.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (txt_password.getRight() - txt_password.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        txt_password.setText("");
                        return true;
                    }
                }
                return false;
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (txt_login.getText().toString().trim().length() == 0
                        || txt_password.getText().toString().trim().length() == 0) {
                    generate_message(getString(R.string.app_warning), getString(R.string.app_fill_field));
                } else {
                    if(app_mode) {  //online
                        //proveryaem, vkl ili net geo_location
                        if (geo_location.isChecked()) {
                            do_geo = true;
                            if (get_location() == 1) return;
                        }

                        user_login = txt_login.getText().toString();
                        user_password = md5(txt_password.getText().toString());
                        passw1 = txt_password.getText().toString();

                        //запоминаем логин/пароль
                        keys_editor = keys.edit();
                        if (chk_remember.isChecked()) {
                            keys_editor.putString("login", user_login);
                            keys_editor.putString("passw", passw1);
                        } else {
                            keys_editor.putString("login", "");
                            keys_editor.putString("passw", "");
                        }
                        try {
                            keys_editor.commit();
                        } catch (Exception err) {
                            generate_message(getString(R.string.app_error2), err.getMessage().toString());
                        }

                        JSONObject jsonObjSend = new JSONObject();
                        try {
                            Boolean rememb = true;
                            jsonObjSend.put("UserName", user_login);
                            jsonObjSend.put("Password", user_password);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Url_Params params = new Url_Params(server_ip + "/android/authenticate/", jsonObjSend);
                        http_auth_me auth_me = new http_auth_me(LoginActivity.this);
                        auth_me.execute(params);
                    }else{  //offline
                        user_login = txt_login.getText().toString();
                        user_password = md5(txt_password.getText().toString());
                        try {
                            keys = getPreferences(Context.MODE_PRIVATE);
                            LoginActivity._myLayout.login_fio = keys.getString("login_fio", "NO_FIO");
                            LoginActivity._myLayout.login_type = keys.getString("login_type", "0");
                            LoginActivity._myLayout.login_ostatok = keys.getString("login_ostatok", "0");
                            LoginActivity._myLayout.login_account = keys.getString("login_account", "0");
                            LoginActivity._myLayout.app_title = getString(R.string.man2man_manager) + ": " + LoginActivity._myLayout.login_fio;
                        } catch (Exception e) {}

                        finish();  //finish Login_activity, in order to exit app from Main_menu.
                        Intent intent = new Intent(LoginActivity.this, Main_menuActivity.class);
                        startActivity(intent);
                        Toast.makeText(LoginActivity.this, getString(R.string.app_login_ok_offline), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        btn_ip.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setTitle(getString(R.string.app_change_ip)); // заголовок для диалога

                LayoutInflater inflater = LoginActivity.this.getLayoutInflater();
                View view = inflater.inflate(R.layout.dialog_for_ip, null);
                builder.setView(view);

                EditText edt_ip_address = (EditText) view.findViewById(R.id.edt_ip);
                edt_ip_address.setText(LoginActivity.server_ip);

                builder.setNegativeButton(getString(R.string.app_cancel), new DialogInterface.OnClickListener() {
                    //@Override
                    public void onClick(DialogInterface dialog, int item) {
                        dialog.cancel();
                    }
                });
                builder.setPositiveButton(getString(R.string.app_save), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        Dialog f = (Dialog) dialog;
                        EditText edt_ip_address = (EditText) f.findViewById(R.id.edt_ip);

                        if (edt_ip_address.getText().toString().trim().length() == 0) {
                            generate_message(getString(R.string.app_warning), getString(R.string.app_enter_ip));
                        } else {
                            keys_editor = keys.edit();
                            keys_editor.putString("server_ip", edt_ip_address.getText().toString().trim());
                            try {
                                keys_editor.commit();
                                server_ip = edt_ip_address.getText().toString().trim();
                                txt_sever_ip.setText(getString(R.string.app_server_host) + " " + server_ip);
                            } catch (Exception err) {
                                generate_message(getString(R.string.app_error1), err.getMessage().toString());
                            }
                        }
                    }
                });
                builder.setCancelable(false);
                builder.create();
                builder.show();
            }
        });
    }

    public Cursor query(String tablename, String[] polya, String uslovie, String sortby) {
        Cursor c = database.query( tablename, polya, uslovie, null, null, null, sortby);
        if (c != null) {
            c.moveToFirst();
        }
        return c;
    }

    void read_preferences() {
        txt_version.setText(getString(R.string.app_versiya) + " " + app_version);
        keys = this.getPreferences(Context.MODE_PRIVATE);
        server_ip = keys.getString("server_ip", "warehouse.at.my.server.tj");
        login = keys.getString("login", "");
        passw1 = keys.getString("passw", "");
        camera_resolution = keys.getInt("camera", 0);
        tmp_accounts = keys.getString("tmp_accounts", "");
        red_accounts = keys.getString("red_accounts", "");
        app_mode = keys.getBoolean("app_mode", true);
        syncing = false;
        sync_marshrut_clients = -1;

        if (tmp_accounts.length() == 0
                || tmp_accounts.indexOf(String.valueOf(android.text.format.DateFormat.format("yyMMdd", new java.util.Date()))) < 0) {
            tmp_accounts = String.valueOf(android.text.format.DateFormat.format("yyMMdd", new java.util.Date())) + ":";
        }

        if (red_accounts.length() == 0
                || red_accounts.indexOf(String.valueOf(android.text.format.DateFormat.format("yyMMdd", new java.util.Date()))) < 0) {
            red_accounts = String.valueOf(android.text.format.DateFormat.format("yyMMdd", new java.util.Date())) + ":";
        }

        if (login.trim().length() > 0 || passw1.trim().length() > 0) chk_remember.setChecked(true);
        txt_sever_ip.setText(getString(R.string.app_server_host) + " " + server_ip.toUpperCase());
        txt_app_mode.setText(getString(R.string.settings_menu_mode) + ": " + getString(R.string.settings_menu_online).toUpperCase());
        if(app_mode == false)
            txt_app_mode.setText(getString(R.string.settings_menu_mode) + ": " + getString(R.string.settings_menu_offline).toUpperCase());
        txt_login.setText(login);
        txt_password.setText(passw1);
    }

    void generate_message(String title, String message) {
        AlertDialog.Builder alert_message = new AlertDialog.Builder(this);
        alert_message.setTitle(title);
        alert_message.setMessage(message);
        alert_message.setCancelable(false);
        alert_message.setNegativeButton(getString(R.string.app_ok), null); //ok
        alert_message.show();
    }

    int get_location() {
        int _otvet = 0;
        LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean enabled = service
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        Intent intent = null;
        if (!enabled) {
            Toast.makeText(this, getString(R.string.login_geo_warn), Toast.LENGTH_LONG).show();

            intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
            _otvet = 1;
        }
        return _otvet;
    }

    void set_tmp_accounts(String tmp, String account) {
        if (tmp.indexOf(account) < 0) {
            tmp_accounts += account + ",";
            //запоминаем логин/пароль
            keys_editor = keys.edit();
            keys_editor.putString("tmp_accounts", tmp_accounts);

            try {
                keys_editor.commit();
            } catch (Exception err) {
                generate_message(getString(R.string.app_error2), err.getMessage().toString());
            }
        }
    }

    void set_red_accounts(int cmd, String account) {
        if (cmd == 1 && red_accounts.indexOf(account + ",") < 0) { //add
            red_accounts += account + ",";
        }else {         //remove
            red_accounts = red_accounts.replace(account + ",", "");
        }
        try {
            keys_editor = keys.edit();
            keys_editor.putString("red_accounts", red_accounts);
            keys_editor.commit();
        } catch (Exception err) {
            generate_message(getString(R.string.app_error2), err.getMessage().toString());
        }

    }

    void get_list_from_pref(int type, String list) {
        String arr = keys.getString(list, "");
        if(arr.length() > 0) {
            try {
                JSONObject jsonObjRecv = new JSONObject(arr);
                Parse_http_result _prs = new Parse_http_result();
                if(type == 1) {
                    _prs.parse_result(LoginActivity.this, "List_marshrut", jsonObjRecv);
                }else if (type == 2){
                    _prs.parse_result(LoginActivity.this, "List_marshrut_clients", jsonObjRecv);
                }else if (type == 3){
                    _prs.parse_result(LoginActivity.this, "List_tovari", jsonObjRecv);
                }else if (type == 4){
                    _prs.parse_result(LoginActivity.this, "List_statyi", jsonObjRecv);
                }else if (type == 5){
                    _prs.parse_result(LoginActivity.this, "List_fas", jsonObjRecv);
                }else if (type == 6){
                    _prs.parse_result(LoginActivity.this, "List_tovars", jsonObjRecv);
                }
            }catch(Exception e){}
        }else{
            Toast.makeText(LoginActivity.this, getString(R.string.app_sync_no_data), Toast.LENGTH_LONG).show();
        }
    }

    public static final String md5(final String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    //создаем меню
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        menu.add(Menu.NONE, LoginActivity.IDM_EXIT, menu.NONE, getString(R.string.app_menu4))
                .setIcon(android.R.drawable.ic_menu_revert);
        return (super.onCreateOptionsMenu(menu));
    }

    ;

    //обрабатываем выбор пункта меню
    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case LoginActivity.IDM_EXIT:
                System.exit(0);
                break;
            default:
                return false;
        }
        return true;
    }

    ;
}
