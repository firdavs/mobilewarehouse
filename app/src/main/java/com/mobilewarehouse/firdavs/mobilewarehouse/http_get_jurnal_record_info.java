package com.mobilewarehouse.firdavs.mobilewarehouse;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Firdavs on 26.02.2015.
 */
public class http_get_jurnal_record_info extends AsyncTask<Url_Params, Void, Void> {
    StringBuilder builder = new StringBuilder();
    Boolean _result = false;
    ProgressDialog progressdlg;
    private Context context;
    String _list;
    JSONObject jsonObjRecv;

    public http_get_jurnal_record_info(Context cxt) {
        context = cxt;
        progressdlg = new ProgressDialog(context);
    }

    //@Override
    protected Void doInBackground(Url_Params... params) {
        String url = "http://" + params[0].url;
        JSONObject jsonObjSend = params[0].jsonObjSend;

        try {
            DefaultHttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPostRequest = new HttpPost(url);

            List<NameValuePair> data = new ArrayList<NameValuePair>();
            data.add(new BasicNameValuePair("data", jsonObjSend.toString()));
            httpPostRequest.setEntity(new UrlEncodedFormEntity(data, "UTF-8"));

            HttpResponse response = httpclient.execute(httpPostRequest);

            // Get hold of the response entity (-> the data):
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                // Read the content stream
                InputStream instream = entity.getContent();
                // convert content stream to a String
                String resultString = Main_menuActivity._myLayout.convertStreamToString(instream);
                instream.close();

                try {
                    jsonObjRecv = new JSONObject(resultString);
                    if (jsonObjRecv != null) {
                        try {
                            if (jsonObjRecv.get("Has_result").toString().equalsIgnoreCase("true")) {
                                _result = true;
                            }
                        } catch (Exception e) {
                            _result = false;
                        }
                    }
                } catch (Exception e) {
                    _result = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        showDialog(context);
    }

    @Override
    protected void onPostExecute(Void result) {
        if (_result == true) {
            //Toast.makeText(this.context, context.getString(R.string.sales_tovar_send_to_server_ok), Toast.LENGTH_LONG).show();
            Parse_http_result _prs = new Parse_http_result();
            _prs.parse_result(context, "Jurnal_record_info", jsonObjRecv);
        } else {
            //Toast.makeText(this.context, context.getString(R.string.app_login_failes), Toast.LENGTH_LONG).show();
            String _error = "";
            try {
                _error = jsonObjRecv.get("message").toString();
            } catch (Exception e) {
            }
            Toast.makeText(this.context, _error, Toast.LENGTH_LONG).show();
        }
        closeDialog();
    }

    private void closeDialog() {
        if (progressdlg != null) {
            progressdlg.dismiss();
        }
    }

    private void showDialog(Context context) {
        if (progressdlg != null) {
            try {
                progressdlg.dismiss();
                progressdlg.cancel();
                progressdlg = null;
            } catch (Exception e) {
            }
        }
        if (progressdlg == null) {
            progressdlg = new ProgressDialog(context);
            progressdlg.setMessage(context.getString(R.string.app_progress_send_sale));
            try {
                progressdlg.show();
            } catch (Exception e) {
            }
        }
    }
}
