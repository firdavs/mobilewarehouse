package com.mobilewarehouse.firdavs.mobilewarehouse;

/**
 * Created by Firdavs on 12.03.2015.
 */

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class PageFragment extends Fragment {

    static final String ARGUMENT_PAGE_NUMBER = "arg_page_number";

    int pageNumber;
    int backColor;

    static PageFragment newInstance(int page) {
        PageFragment pageFragment = new PageFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(ARGUMENT_PAGE_NUMBER, page);
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageNumber = getArguments().getInt(ARGUMENT_PAGE_NUMBER);

        //Random rnd = new Random();
        //backColor = Color.argb(40, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_screen_slide_page, null);

        TextView tvNaimenovanie = (TextView) view.findViewById(R.id.tvNaimenovanie);
        tvNaimenovanie.setText(getString(R.string.katalog) + "\n" + Katalog_sliderActivity._myLayout.tovar_name[pageNumber]);

        TextView tvDescription = (TextView) view.findViewById(R.id.tvDescription);
        tvDescription.setText(Katalog_sliderActivity._myLayout.tovar_description[pageNumber]);
        tvDescription.setMovementMethod(new ScrollingMovementMethod());

        ImageView tvImg = (ImageView) view.findViewById(R.id.tvImage);

        http_get_image down_img = new http_get_image((ImageView) view.findViewById(R.id.tvImage));
        down_img.execute("http://" + LoginActivity._myLayout.server_ip + "/assets/uploads/products/" + Katalog_sliderActivity._myLayout.tovar_image[pageNumber]);

        tvImg.setClickable(true);
        tvImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show_image_big(Katalog_sliderActivity._myLayout.tovar_name[pageNumber], Katalog_sliderActivity._myLayout.tovar_image[pageNumber]);
            }
        });

        return view;
    }

    void show_image_big(String img_title, String img_filename) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Katalog_sliderActivity._myLayout);
        builder.setTitle(img_title); // заголовок для диалога

        LayoutInflater inflater = Katalog_sliderActivity._myLayout.getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_big_image, null);
        builder.setView(view);

        http_get_image down_img = new http_get_image((ImageView) view.findViewById(R.id.fragment_image_big));
        down_img.execute("http://" + LoginActivity._myLayout.server_ip + "/assets/uploads/products/big/" + img_filename);

        builder.setNegativeButton(getString(R.string.app_ok), new DialogInterface.OnClickListener() {
            //@Override
            public void onClick(DialogInterface dialog, int item) {
                dialog.cancel();
            }
        });
        builder.setCancelable(true);
        builder.create();
        builder.show();
    }
}

