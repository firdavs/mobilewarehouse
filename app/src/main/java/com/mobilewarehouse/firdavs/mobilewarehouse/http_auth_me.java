package com.mobilewarehouse.firdavs.mobilewarehouse;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Firdavs on 26.02.2015.
 */
public class http_auth_me extends AsyncTask<Url_Params, Void, Void> {
    StringBuilder builder = new StringBuilder();
    Boolean _result = false;
    ProgressDialog progressdlg;
    private Context context;
    JSONObject jsonObjRecv = null;

    /*
    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
    */

    public http_auth_me(Context cxt) {
        context = cxt;
        progressdlg = new ProgressDialog(context);
    }

    //@Override
    protected Void doInBackground(Url_Params... params) {
        String url = "http://" + params[0].url;
        JSONObject jsonObjSend = params[0].jsonObjSend;
        /*
        try {
            url = url + "data=" + URLEncoder.encode(jsonObjSend.toString(), "UTF-8");
        }catch (Exception e){}
        */
        //url = url.replace("\"","\'");
        //Log.d("url", url);
        try {
            DefaultHttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPostRequest = new HttpPost(url);


            List<NameValuePair> data = new ArrayList<NameValuePair>();
            data.add(new BasicNameValuePair("data", jsonObjSend.toString()));
            httpPostRequest.setEntity(new UrlEncodedFormEntity(data, "UTF-8"));

            HttpResponse response = httpclient.execute(httpPostRequest);

            // Get hold of the response entity (-> the data):
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                // Read the content stream
                InputStream instream = entity.getContent();
                // convert content stream to a String
                String resultString = Main_menuActivity._myLayout.convertStreamToString(instream);
                instream.close();

                jsonObjRecv = new JSONObject(resultString);

                String tmp = null;
                if (jsonObjRecv != null) {
                    try {
                        if (jsonObjRecv.get("Has_result").toString().equalsIgnoreCase("true")) {
                            tmp = jsonObjRecv.get("IsAuthenticated").toString();
                            if (tmp.equalsIgnoreCase("true")) {
                                //TasksActivity._fullname=jsonObjRecv.get("FullName").toString();
                                _result = true;
                            }
                        } else {
                            _result = false;
                        }
                    } catch (Exception e) {
                        _result = false;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        showDialog(context);
    }

    @Override
    protected void onPostExecute(Void result) {
        if (_result == true) {
            try {
                LoginActivity._myLayout.login_fio = jsonObjRecv.get("FullName").toString();
                LoginActivity._myLayout.login_type = jsonObjRecv.get("Role").toString();
                LoginActivity._myLayout.login_ostatok = jsonObjRecv.get("Saldo").toString();
                LoginActivity._myLayout.login_account = jsonObjRecv.get("Schet").toString();
                //LoginActivity._myLayout.login_type = "1";
                LoginActivity._myLayout.app_title = context.getString(R.string.man2man_manager) + ": " + LoginActivity._myLayout.login_fio;

                //add to prefs fio, type, ostatok
                LoginActivity._myLayout.keys_editor = LoginActivity._myLayout.keys.edit();
                LoginActivity._myLayout.keys_editor.putString("login_fio", LoginActivity._myLayout.login_fio);
                LoginActivity._myLayout.keys_editor.putString("login_type", LoginActivity._myLayout.login_type);
                LoginActivity._myLayout.keys_editor.putString("login_ostatok", LoginActivity._myLayout.login_ostatok);
                LoginActivity._myLayout.keys_editor.putString("login_account", LoginActivity._myLayout.login_account);
                try {
                    LoginActivity._myLayout.keys_editor.commit();
                } catch (Exception err) {
                    LoginActivity._myLayout.generate_message(context.getString(R.string.app_error2), err.getMessage().toString());
                }
            } catch (Exception e) {
            }

            ((Activity) context).finish();  //finish Login_activity, in order to exit app from Main_menu.
            Intent intent = new Intent(this.context, Main_menuActivity.class);
            this.context.startActivity(intent);
            Toast.makeText(this.context, context.getString(R.string.app_login_ok), Toast.LENGTH_LONG).show();

            if (LoginActivity._myLayout.do_geo == true) {
                GetLocation_Activity gps = new GetLocation_Activity(this.context);
            }
        } else {
            String _error = "";
            try {
                _error = jsonObjRecv.get("message").toString();
            } catch (Exception e) {
            }
            Toast.makeText(this.context, _error, Toast.LENGTH_LONG).show();
        }
        closeDialog();
    }

    private void closeDialog() {
        if (progressdlg != null) {
            progressdlg.dismiss();
        }
    }

    private void showDialog(Context context) {
        if (progressdlg != null) {
            try {
                progressdlg.dismiss();
                progressdlg.cancel();
                progressdlg = null;
            } catch (Exception e) {
            }
        }
        if (progressdlg == null) {
            progressdlg = new ProgressDialog(context);
            progressdlg.setMessage(context.getString(R.string.app_progress_auth));
            try {
                progressdlg.show();
            } catch (Exception e) {
            }
        }

    }
}
