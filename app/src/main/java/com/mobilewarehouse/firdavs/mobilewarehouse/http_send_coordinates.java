package com.mobilewarehouse.firdavs.mobilewarehouse;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Firdavs on 30.04.2015.
 */
public class http_send_coordinates extends AsyncTask<Url_Params, Void, Void> {
    StringBuilder builder = new StringBuilder();
    Boolean _result = false;
    private Context context;
    String _list;
    JSONObject jsonObjRecv;

    public http_send_coordinates(Context cxt) {
        context = cxt;
    }

    //@Override
    protected Void doInBackground(Url_Params... params) {
        String url = "http://" + params[0].url;
        JSONObject jsonObjSend = params[0].jsonObjSend;

        try {
            DefaultHttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPostRequest = new HttpPost(url);


            List<NameValuePair> data = new ArrayList<NameValuePair>();
            data.add(new BasicNameValuePair("data", jsonObjSend.toString()));
            httpPostRequest.setEntity(new UrlEncodedFormEntity(data, "UTF-8"));

            HttpResponse response = httpclient.execute(httpPostRequest);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream instream = entity.getContent();
                String resultString = Main_menuActivity._myLayout.convertStreamToString(instream);
                instream.close();

                try {
                    jsonObjRecv = new JSONObject(resultString);
                    if (jsonObjRecv != null) {
                        try {
                            if (jsonObjRecv.get("Has_result").toString().equalsIgnoreCase("true")) {
                                _result = true;
                            }
                        } catch (Exception e) {
                            _result = false;
                        }
                    }
                } catch (Exception e) {
                    _result = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected void onPostExecute(Void result) {
        if (_result == false) {
            String _error = this.context.getString(R.string.app_error4);
            try {
                _error = jsonObjRecv.get("message").toString();
            } catch (Exception e) {
            }
            Toast.makeText(this.context, _error, Toast.LENGTH_LONG).show();
        }
    }
}
