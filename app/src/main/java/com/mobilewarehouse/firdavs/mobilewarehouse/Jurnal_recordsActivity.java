package com.mobilewarehouse.firdavs.mobilewarehouse;

import android.app.ListActivity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Firdavs on 09.03.2015.
 */
public class Jurnal_recordsActivity extends ListActivity {
    public static Jurnal_recordsActivity _myLayout;
    myAdapter jn_records_list;

    public static String[] jn_operation_name, jn_operation_id, jn_operation_type, jn_operation_sum, jn_operation_client, jn_operation_date;
    public static Integer[] jn_operation_img;
    int img = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //MainFormActivity.myLayout.set_theme(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jurnal_records);
        Jurnal_recordsActivity._myLayout = this;
        this.setTitle(LoginActivity._myLayout.app_title);

        get_images();

        jn_records_list = new myAdapter(this);
        setListAdapter(jn_records_list);
    }

    void get_images() {
        for (int i = 0; i < jn_operation_name.length; i++) {
            try {
                switch (Integer.parseInt(_myLayout.jn_operation_type[i])) {
                    case 1006:     //sales
                        img = getResources().getIdentifier("sales_j", "drawable", "com.mobilewarehouse.firdavs.mobilewarehouse");
                        break;
                    case 1007:     //prixodniy_order
                        img = getResources().getIdentifier("prixodniy_order_j", "drawable", "com.mobilewarehouse.firdavs.mobilewarehouse");
                        break;
                    case 1008:    //rasxodi
                        img = getResources().getIdentifier("rasxodi_j", "drawable", "com.mobilewarehouse.firdavs.mobilewarehouse");
                        break;
                    case 1014:    //man2man
                        img = getResources().getIdentifier("man2man_j", "drawable", "com.mobilewarehouse.firdavs.mobilewarehouse");
                        break;
                    default:    //vozvrat
                        img = getResources().getIdentifier("vozvrat_j", "drawable", "com.mobilewarehouse.firdavs.mobilewarehouse");
                        break;
                }
                Jurnal_recordsActivity._myLayout.jn_operation_img[i] = img;
            } catch (Exception e) {
            }
        }
    }

    public void onListItemClick(ListView parent, View v, int position, long id) {
        //list item click haminja
        get_jurnal_record_info(jn_operation_type[position], jn_operation_id[position]);
    }

    void get_jurnal_record_info(String _opn_type, String _opn_id) {
        JSONObject jsonObjSend = new JSONObject();
        try {
            jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
            jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
            jsonObjSend.put("Operation_Type", _opn_type);
            jsonObjSend.put("Id", _opn_id);
            jsonObjSend.put("Type", "2");

            Log.d("json", jsonObjSend.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/getOperationDetails/", jsonObjSend);
        http_get_jurnal_record_info jurnal_record_info_me = new http_get_jurnal_record_info(Jurnal_recordsActivity.this);
        jurnal_record_info_me.execute(params);
    }

    public class myAdapter extends BaseAdapter {
        private LayoutInflater mLayoutInflater;

        public myAdapter(Context ctx) {
            mLayoutInflater = LayoutInflater.from(ctx);
        }

        public int getCount() {
            return jn_operation_name.length;
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }


        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null)
                convertView = mLayoutInflater.inflate(R.layout.item_jurnal_record, null);

            ImageView image = (ImageView) convertView.findViewById(R.id.img_operation);
            image.setImageResource(jn_operation_img[position]);

            TextView itm_op_name = (TextView) convertView.findViewById(R.id.itm_op_name);
            itm_op_name.setText(jn_operation_name[position]);

            TextView itm_op_client = (TextView) convertView.findViewById(R.id.itm_op_client);
            itm_op_client.setText(jn_operation_client[position]);

            TextView itm_op_date = (TextView) convertView.findViewById(R.id.itm_op_date);
            itm_op_date.setText(jn_operation_date[position]);

            TextView itm_op_summa = (TextView) convertView.findViewById(R.id.itm_op_summa);
            itm_op_summa.setText(jn_operation_sum[position]);
            return convertView;
        }
    }

    ///////// WORKING WITH BACK BUTTON //////////////////////////////////////
    @Override
    public void onBackPressed() {
        finish();
    }

    //создаем меню
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        menu.add(Menu.NONE, LoginActivity.IDM_MAIN, menu.NONE, getString(R.string.app_menu2))
                .setIcon(android.R.drawable.ic_menu_revert);
        return (super.onCreateOptionsMenu(menu));
    }

    ;

    //обрабатываем выбор пункта меню
    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case LoginActivity.IDM_MAIN:
                finish();
                break;
            default:
                return false;
        }
        return true;
    }

    ;
}
