package com.mobilewarehouse.firdavs.mobilewarehouse;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Firdavs on 25.02.2015.
 */
public class Rasxodi_Activity extends Activity {
    public AutoCompleteTextView txt_statya;
    public AutoCompleteTextView txt_tsex;
    public ImageButton btn_statya;
    public ImageButton btn_tsex_select;
    public EditText txt_summa;
    public EditText txt_primechanie;
    public EditText txt_price;
    public EditText txt_kolvo;
    public Button btn_add;
    public static Rasxodi_Activity _myLayout;
    public RadioButton opt_statya, opt_korobka;

    public String[] list_statya, list_statya_id;
    public String statya_name, statya_id, sstatya_price;
    public static String[][] statya_price;
    public static String[] sstatya_prices;
    public static String stsex_name, stsex_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.slide_to_right, R.anim.slide_to_left);
        setContentView(R.layout.activity_menu_rasxodi);
        Rasxodi_Activity._myLayout = this;
        this.setTitle(LoginActivity._myLayout.app_title);

        fill_list_statya();
        //iz servera poluchaem spisok tsexov
//        fill_list("1", "");

        txt_tsex = (AutoCompleteTextView) findViewById(R.id.edt_rasxodi_proiz_tsex);
        txt_statya = (AutoCompleteTextView) findViewById(R.id.edt_rasxodi_statya);
        txt_summa = (EditText) findViewById(R.id.edt_rasxodi_summa);
        txt_primechanie = (EditText) findViewById(R.id.edt_rasxodi_primechanie);
        txt_price = (EditText) findViewById(R.id.edt_rasxodi_tsena);
        txt_kolvo = (EditText) findViewById(R.id.edt_rasxodi_kolvo);
        btn_add = (Button) findViewById(R.id.btn_rasxodi_rasxod);
        btn_statya = (ImageButton) findViewById(R.id.btn_rasxodi_statya);
        btn_tsex_select = (ImageButton) findViewById(R.id.btn_rasxodi_proiz_tsex);
        opt_statya = (RadioButton) findViewById(R.id.opt_statya);
        opt_korobka = (RadioButton) findViewById(R.id.opt_korobka);
        TextView txt_tsex_caption = (TextView) findViewById(R.id.txt_tsex_caption);
        txt_tsex_caption.setVisibility(View.GONE);
        btn_tsex_select.setVisibility(View.GONE);
        txt_tsex.setVisibility(View.GONE);

        //при загрузке, статично задаем значения переменных
//        statya_id = "3";    //id korobka
//        sstatya_price = "1";
        stsex_id = LoginActivity._myLayout.sklad_id;
        txt_kolvo.setFocusable(true);

        //сами задаем айди цеха, т.к., цех один
        //и прячем поле цех
        //stsex_id=Proizvodtsvo_Activity._myLayout.tsex_id[0];

        //vybor tovara iz spiska
        btn_statya.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (opt_statya.isChecked()) {
                    show_statya_list();
                } else {
                    Operations_menuActivity._myLayout._operation = "_sales";
                    LoginActivity._myLayout.operation_page = 3; //rasxody
                    Operations_menuActivity._myLayout.get_marshrut_list();
                }
            }
        });

        opt_statya.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                opt_korobka.setChecked(false);
                clear_fields();
            }
        });
        opt_korobka.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                opt_statya.setChecked(false);
                clear_fields();
            }
        });

        //vybor tsexa iz spiska
        btn_tsex_select.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                show_tsex_list();
            }
        });

        txt_statya.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (txt_statya.getRight() - txt_statya.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        //txt_statya.setText("");
                        clear_fields();
                        return true;
                    }
                }
                return false;
            }
        });

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alrtauth = new AlertDialog.Builder(Rasxodi_Activity.this);
                alrtauth.setTitle(getString(R.string.app_warning));
                if (check_fields() == 0) {   //все поля заполнены
                    //delaem proc. otpravki dannyx na server
                    send_rasxodi_to_server_dlg();
                    return;
                } else {
                    //заполните поля
                    alrtauth.setMessage(getString(R.string.app_fill_field));
                }
                alrtauth.setCancelable(false);
                alrtauth.setNegativeButton(getString(R.string.app_ok), null);
                alrtauth.show();
            }
        });

        /*
        txt_summa.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (txt_summa.getRight() - txt_summa.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        txt_summa.setText("");
                        return true;
                    }
                }
                return false;
            }
        });
        */

        txt_primechanie.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (txt_primechanie.getRight() - txt_primechanie.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        txt_primechanie.setText("");
                        return true;
                    }
                }
                return false;
            }
        });

        txt_tsex.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (txt_tsex.getRight() - txt_tsex.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        txt_tsex.setText("");
                        //txtchanged = true;
                        return true;
                    }
                }
                return false;
            }
        });

        txt_price.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    ////show_statya_price_list();
                }
            }
        });

        txt_kolvo.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    txt_summa.setText(String.valueOf(new DecimalFormat("##.##").format(Integer.parseInt(txt_kolvo.getText().toString()) * Double.parseDouble(txt_price.getText().toString()))).replace(",", "."));
                } catch (Exception e) {
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        txt_price.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    txt_summa.setText(String.valueOf(new DecimalFormat("##.##").format(Integer.parseInt(txt_kolvo.getText().toString()) * Double.parseDouble(txt_price.getText().toString()))).replace(",", "."));
                } catch (Exception e) {
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    //pokazyvaem spisok tsexov
    void show_tsex_list() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.sales_tovar_select)); // заголовок для диалога
        builder.setItems(Proizvodtsvo_Activity._myLayout.tsex_name, new DialogInterface.OnClickListener() {
            //@Override
            public void onClick(DialogInterface dialog, int item) {
                stsex_id = Proizvodtsvo_Activity._myLayout.tsex_id[item];
                stsex_name = Proizvodtsvo_Activity._myLayout.tsex_name[item];

                txt_tsex.setText(stsex_name);
            }
        });
        //builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    //pokazyvaem spisok цен statyi
    void show_statya_price_list() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.sales_tovar_price_select)); // заголовок для диалога
        builder.setItems(sstatya_prices, new DialogInterface.OnClickListener() {
            //@Override
            public void onClick(DialogInterface dialog, int item) {
                sstatya_price = sstatya_prices[item];
                txt_price.setText(sstatya_price);
                txt_kolvo.requestFocus();
            }
        });
        builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    void fill_statya_prices_to_arr(int position) {
        String _prc;
        sstatya_prices = new String[statya_price[position].length];
        for (int i = 0; i < statya_price[position].length; i++) {
            _prc = statya_price[position][i];
            sstatya_prices[i] = _prc;
        }
    }

    void fill_list_statya() {
        if(LoginActivity._myLayout.app_mode == true) {   //online
            JSONObject jsonObjSend = new JSONObject();
            try {
                // Add key/value pairs
                Boolean rememb = true;
                jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
                jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
                // Output the JSON object we're sending to Logcat:
                Log.d("json", jsonObjSend.toString(2));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/getListOfStatyiRasxoda/", jsonObjSend);
            http_fill_list fill_list = new http_fill_list(Rasxodi_Activity.this);
            fill_list.execute(params);
        }else{
            LoginActivity._myLayout.get_list_from_pref(4, "List_statyi");
        }
    }

    //pokazyvaem spisok tovarov
    void show_statya_list() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.rasxodi_statya_select)); // заголовок для диалога
        builder.setItems(list_statya, new DialogInterface.OnClickListener() {
            //@Override
            public void onClick(DialogInterface dialog, int item) {
                //item v massive
                statya_name = list_statya[item];
                statya_id = list_statya_id[item];
                txt_statya.setText(statya_name);

                fill_statya_prices_to_arr(item);
                //sstatya_price = sstatya_prices[0]; //po umolch
                //txt_price.setText(sstatya_price);

//                stsex_id = Proizvodtsvo_Activity._myLayout.tsex_id[0];

                txt_kolvo.requestFocus();
            }
        });
        //builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    void send_rasxodi_to_server_dlg() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_warning)); // заголовок для диалога
        builder.setMessage(getString(R.string.sales_tovar_send_to_server));
        builder.setNegativeButton(getString(R.string.app_no), new DialogInterface.OnClickListener() {
            //@Override
            public void onClick(DialogInterface dialog, int item) {
                dialog.cancel();
            }
        });
        builder.setPositiveButton(getString(R.string.app_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                send_rasxodi_to_server();
            }
        });

        builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    void send_rasxodi_to_server() {
        if(LoginActivity._myLayout.app_mode) {
            JSONObject jsonObjSend = new JSONObject();
            try {
                sstatya_price = txt_price.getText().toString();

                jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
                jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
                jsonObjSend.put("Sum", txt_summa.getText().toString().replace(",", "."));
                jsonObjSend.put("Comment", txt_primechanie.getText().toString());
                jsonObjSend.put("Statya", statya_id);
                jsonObjSend.put("Kolvo", txt_kolvo.getText().toString());
                jsonObjSend.put("Price", sstatya_price);
                jsonObjSend.put("Sklad_Id", stsex_id);

                Log.d("json", jsonObjSend.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/rasxodDeneg/", jsonObjSend);
            http_send_rasxodi rasxodi_me = new http_send_rasxodi(Rasxodi_Activity.this);
            rasxodi_me.execute(params);
        }else{
            if(save_rasxod(txt_summa.getText().toString().replace(",", "."), txt_primechanie.getText().toString(), statya_id, txt_kolvo.getText().toString(), txt_price.getText().toString(), stsex_id) != 0)
                Toast.makeText(Rasxodi_Activity.this, getString(R.string.sales_prixod_save_err), Toast.LENGTH_LONG).show();
        }
    }

    int save_rasxod(String sum, String comment, String statya_id, String kolvo, String price, String sklad_id) {
        int otvet = 0;
        SimpleDateFormat dformat = new SimpleDateFormat("yyMMddHHmmss");
        Date date = new Date();
        String _tranzaction = dformat.format(date);

        ContentValues content = new ContentValues();
        //записываем в табл. prixody
        content.put("_tranzcode", _tranzaction);
        content.put("_sum", sum);
        content.put("_comment", comment);
        content.put("_statya", statya_id);
        content.put("_kolvo", kolvo);
        content.put("_price", price);
        content.put("_sklad_id", sklad_id);
        try {
            LoginActivity.database.insert("_rasxody", null, content);
            clear_fields();
        } catch (Exception e) {
            otvet = 1;
        }
        content.clear();
        return otvet;
    }

    void fill_list(String _type, String _sklad_id) {
        JSONObject jsonObjSend = new JSONObject();
        try {
            // Add key/value pairs
            Boolean rememb = true;
            jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
            jsonObjSend.put("Password", LoginActivity._myLayout.user_password);

            // Output the JSON object we're sending to Logcat:
            Log.d("json", jsonObjSend.toString(2));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Url_Params params = null;
        params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/getListOfSklads/", jsonObjSend);
        http_fill_list_tsex fill_list_tsex = new http_fill_list_tsex(Rasxodi_Activity.this);
        fill_list_tsex.execute(params);
    }

    //chistim polya
    void clear_fields() {
        txt_statya.setText("");
        txt_summa.setText("");
        txt_primechanie.setText("");
        txt_kolvo.setText("");
        txt_price.setText("");
        txt_summa.requestFocus();
        statya_id = "";
        sstatya_price = "";
        txt_tsex.setText("");
    }

    int check_fields() {
        int otvet = 0;
        if (
                txt_summa.length() == 0
                        //|| txt_primechanie.length()==0
                        || txt_statya.length() == 0
                        || txt_price.length() == 0
                        || txt_kolvo.length() == 0
            //|| txt_tsex.length()==0
                ) {
            otvet = 1;
        }
        return otvet;
    }

    ///////// WORKING WITH BACK BUTTON //////////////////////////////////////
    @Override
    public void onBackPressed() {
        finish();
    }

    //создаем меню
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        menu.add(Menu.NONE, LoginActivity.IDM_MAIN, menu.NONE, getString(R.string.app_menu2))
                .setIcon(android.R.drawable.ic_menu_revert);
        return (super.onCreateOptionsMenu(menu));
    }

    ;

    //обрабатываем выбор пункта меню
    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case LoginActivity.IDM_MAIN:
                finish();
                break;
            default:
                return false;
        }
        return true;
    }

    ;

}
