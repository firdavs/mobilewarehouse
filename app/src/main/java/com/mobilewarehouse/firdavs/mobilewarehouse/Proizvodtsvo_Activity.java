package com.mobilewarehouse.firdavs.mobilewarehouse;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Firdavs on 23.02.2015.
 */
public class Proizvodtsvo_Activity extends Activity {
    public static Proizvodtsvo_Activity _myLayout;
    public static String[] client_name;
    public static String[] client_account;
    public static String[] client_balance;
    public static String stsex_name, stsex_account, sclient_balance, stsex_id;

    public static String[] tovar_name, tovar_id;
    public static String[] tsex_name, tsex_id;
    //public static String[][] tovar_price, list_tovar_price;
    //public static String[] list_tovar_price;
    //public static String[] tovar_ostatok, list_tovar_ostatok;
    public static String stovar_name, stovar_id, stovar_price, stovar_ostatok, stovar_kolvo;
    //public static String[] stovar_prices;
    public static String _tranzaction;
    //public static Boolean txtchanged=true;
    //public static Boolean dont_show_list=true;

    int[] _tovar_kolvo_to_change;
    String[] _tovar_idrref_to_del = null;   //в этот массив запоминаем айди товаров, и при используем при удалении
    String[] _tovar_description_to_del = null;   //в этот массив запоминаем названия товаров, и при используем при удалении

    public AutoCompleteTextView txt_tsex;
    public AutoCompleteTextView txt_tovar;
    public EditText txt_kolvo;
    public EditText txt_price;
    public EditText txt_summa;
    public TextView txt_proiz_itogo;
    public ImageButton btn_add;
    public ImageButton btn_plus;
    public ImageButton btn_minus;
    public ImageButton btn_delete;
    public ImageButton btn_save;
    public ImageButton btn_tovar_select;
    public ImageButton btn_tsex_select;
    ListView list_tovari;

    public View selprevrow;    //выбранный айтем в списке товаров
    public static int listtouchposition = -1;    //тоже, выбранный айтем в списке товаров

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proizvodstvo);
        Proizvodtsvo_Activity._myLayout = this;
        this.setTitle(LoginActivity._myLayout.app_title);


        txt_tsex = (AutoCompleteTextView) findViewById(R.id.edt_proiz_tsex);
        txt_tovar = (AutoCompleteTextView) findViewById(R.id.edt_proiz_tovar);
        txt_kolvo = (EditText) findViewById(R.id.edt_proiz_kolvo);
        btn_add = (ImageButton) findViewById(R.id.btn_proiz_add);
        btn_plus = (ImageButton) findViewById(R.id.btn_proiz_plus);
        btn_minus = (ImageButton) findViewById(R.id.btn_proiz_minus);
        btn_delete = (ImageButton) findViewById(R.id.btn_proiz_delete);
        btn_tovar_select = (ImageButton) findViewById(R.id.btn_proiz_tovar);
        btn_tsex_select = (ImageButton) findViewById(R.id.btn_proiz_tsex);
        btn_save = (ImageButton) findViewById(R.id.btn_proiz_save);
        list_tovari = (ListView) findViewById(R.id.list_tovari_proiz);

        txt_proiz_itogo = (TextView) findViewById(R.id.txt_proiz_itogo);
        TextView txt_sales_caption = (TextView) findViewById(R.id.txt_sales_caption);

        gen_tranzcode();

        //iz servera poluchaem spisok tsexov
        fill_list("1", "");

        //чистим таблицы
        clear_tables();

        //generiruem zagolovki spiska
        set_tovari_list_headers();

        //читаем список из табл.
        //fill_tovar_list();

        //vybor tovara iz spiska
        btn_tovar_select.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                show_tovar_list();
            }
        });

        //vybor tovara iz spiska
        btn_tsex_select.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                show_tsex_list();
            }
        });

        //удаление товара
        btn_delete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (listtouchposition != -1) {   //по умолч. -1, поэтому, если не -1, то товар уже выбран
                    delete_tovar_from_list(_tovar_idrref_to_del[listtouchposition], _tovar_description_to_del[listtouchposition]);
                } else {
                    //выберите товар из списка, пожалуйста!
                    gen_message(Proizvodtsvo_Activity._myLayout, getString(R.string.app_warning), getString(R.string.sales_tovar_select));
                }
            }
        });

        //увеличиваем колво товара
        btn_plus.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (listtouchposition != -1) {   //по умолч. -1, поэтому, если не -1, то товар уже выбран
                    change_tovar_kol(1, _tovar_idrref_to_del[listtouchposition]);
                } else {
                    //выберите товар из списка, пожалуйста!
                    gen_message(Proizvodtsvo_Activity._myLayout, getString(R.string.app_warning), getString(R.string.sales_tovar_select));
                }
            }
        });

        //уменьшааем колво товара
        btn_minus.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (listtouchposition != -1) {   //по умолч. -1, поэтому, если не -1, то товар уже выбран
                    change_tovar_kol(-1, _tovar_idrref_to_del[listtouchposition]);
                } else {
                    //выберите товар из списка, пожалуйста!
                    gen_message(Proizvodtsvo_Activity._myLayout, getString(R.string.app_warning), getString(R.string.sales_tovar_select));
                }
            }
        });

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alrtauth = new AlertDialog.Builder(Proizvodtsvo_Activity.this);
                alrtauth.setTitle(getString(R.string.app_warning));
                stovar_kolvo = txt_kolvo.getText().toString();
                //stovar_price=txt_price.getText().toString();
                if (check_fields() == 0)   //все поля заполнены
                {
                    if (save_tovar() == 0) {
                        //set tsex field disabled
                        txt_tsex.setEnabled(false);
                        //чистим поля
                        clear_fields();
                        //регенерируем заново список товаров
                        fill_tovar_list();
                        //dat vozmozhnost polzovatsya http_fill_list
                        //txtchanged = true;
                        txt_tovar.requestFocus();
                        //товар успешно сохранен
                        //alrtauth.setMessage(getString(R.string.sales_tovar_added));
                        Toast.makeText(Proizvodtsvo_Activity.this, getString(R.string.sales_tovar_added), Toast.LENGTH_LONG).show();
                        return;
                    } else {
                        //чистим таблицы
                        clear_tables();
                        //произошла ошибка во время записи
                        alrtauth.setMessage(getString(R.string.sales_tovar_add_err));
                    }
                } else {
                    //заполните поля
                    alrtauth.setMessage(getString(R.string.app_fill_field));
                }
                alrtauth.setCancelable(false);
                alrtauth.setNegativeButton(getString(R.string.app_ok), null); //ok
                alrtauth.show();

            }
        });

        txt_tovar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (txt_tovar.getRight() - txt_tovar.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        txt_tovar.setText("");
                        //txtchanged = true;
                        return true;
                    }
                }
                return false;
            }
        });

        txt_tsex.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (txt_tsex.getRight() - txt_tsex.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        txt_tsex.setText("");
                        //txtchanged = true;
                        return true;
                    }
                }
                return false;
            }
        });

        list_tovari.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if (selprevrow != null) {
                    selprevrow.setBackgroundResource(android.R.color.transparent);
                }
                selprevrow = view;
                //view.requestFocusFromTouch();
                //view.setBackgroundResource(R.drawable.abc_list_pressed_holo_dark);

                listtouchposition = position;
                fill_tovar_list();
            }
        });

        //send2server
        btn_save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if(LoginActivity._myLayout.app_mode) {
                    mnu_send_zakaz();
                }else {
                    clear_fields();
                    gen_tranzcode();
                    fill_tovar_list();
                }
            }
        });
    }

    //pokazyvaem spisok tovarov
    void show_tovar_list() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.sales_tovar_select)); // заголовок для диалога
        builder.setItems(tovar_name, new DialogInterface.OnClickListener() {
            //@Override
            public void onClick(DialogInterface dialog, int item) {
                //txtchanged = false;
                //item v massive
                stovar_name = tovar_name[item];
                stovar_id = tovar_id[item];
                //stovar_price = list_tovar_price[item][0];

                //fill_tovar_prices_to_arr(item);

                //stovar_ostatok = list_tovar_ostatok[item];
                txt_tovar.setText(stovar_name);
                //txt_price.setText(stovar_price);
                //txt_kolvo.setHint(stovar_ostatok);
                txt_kolvo.requestFocus();
            }
        });
        //builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    //pokazyvaem spisok tovarov
    void show_tsex_list() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.sales_tovar_select)); // заголовок для диалога
        builder.setItems(tsex_name, new DialogInterface.OnClickListener() {
            //@Override
            public void onClick(DialogInterface dialog, int item) {
                stsex_account = tsex_id[item];
                stsex_name = tsex_name[item];

                txt_tsex.setText(tsex_name[item]);
                txt_tovar.setText("");

                stsex_id = tsex_id[item];
//                fill_list("2", stsex_id);
                fill_list("2", LoginActivity._myLayout.sklad_id);
            }
        });
        //builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    //otmena zayavki, 4istim tabl i polya
    void cancel_sale() {
        clear_fields();
        clear_tables();
        fill_tovar_list();
        txt_tsex.setEnabled(true);
        txt_tsex.setText("");
        //txt_kolvo.setHint("");
        txt_tsex.requestFocus();
        //txtchanged = true;
    }

    void mnu_send_zakaz() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_warning)); // заголовок для диалога
        builder.setMessage(getString(R.string.sales_tovar_send_to_server));
        builder.setNegativeButton(getString(R.string.app_no), new DialogInterface.OnClickListener() {
            //@Override
            public void onClick(DialogInterface dialog, int item) {
                dialog.cancel();
            }
        });
        builder.setPositiveButton(getString(R.string.app_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                send_zakaz();
            }
        });

        builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    public void send_zakaz() {
        //отправляем заказ на сервер
        String _tmp = "";
        String[] _send_data = new String[1];
        String[] s = {"*"};
        Cursor cur = null;
        JSONObject jsonObjSend = new JSONObject();
        JSONObject jsonObjSend2 = null;
        JSONArray jsonArr = null;
        int kol = 0;

        cur = LoginActivity._myLayout.query("_operations_common_info_proiz", s, "", "");
        if (cur.getCount() > 0) {
            try {
                jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
                jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
                jsonObjSend.put("Sklad_id", LoginActivity._myLayout.sklad_id);
                jsonObjSend.put("Fas", cur.getString(3));
                //jsonObjSend.put("Operation", "1");
                //jsonObjSend.put("Role", LoginActivity._myLayout.login_type);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            cur.close();

            //получаем данные о товарах
            cur = LoginActivity._myLayout.query("_operations_info_proiz", s, "_tranzcode = '" + _tranzaction + "'", "_tovar_name");

            if (cur.getCount() > 0) {
                jsonArr = new JSONArray();
                do {
                    try {
                        jsonObjSend2 = new JSONObject();
                        //jsonObjSend2.put("Naimenovanie", cur.getString(2));
                        jsonObjSend2.put("Id", cur.getString(1));
                        jsonObjSend2.put("Count", cur.getString(3));
                        //jsonObjSend2.put("Price", cur.getString(5));

                        try {
                            jsonArr.put(kol, jsonObjSend2);
                        } catch (Exception e) {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    kol++;
                } while (cur.moveToNext());
            }
            cur.close();

            try {
                jsonObjSend.put("Members", jsonArr);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Url_Params params = null;
            params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/prixodNaSklad/", jsonObjSend);
            http_send_proiz send_proiz = new http_send_proiz(Proizvodtsvo_Activity.this);
            send_proiz.execute(params);
        }
    }

    public void change_tovar_kol(int _kolvo, String _tovar_idrref_to_change) {
        boolean do_change = true;
        ContentValues content = new ContentValues();

        if (_kolvo == -1) {    //minusovat
            if (_tovar_kolvo_to_change[listtouchposition] <= 1)
                do_change = false;
        }

        if (do_change) {
            content.put("_kolvo", String.valueOf(_tovar_kolvo_to_change[listtouchposition] + _kolvo));
            try {
                LoginActivity._myLayout.database.update("_operations_info_proiz", content, "_tovar_id='" + _tovar_idrref_to_change + "'", null);
            } catch (Exception e) {
                gen_message(Proizvodtsvo_Activity._myLayout, getString(R.string.app_warning), e.getMessage().toString());
                return;
            }
            //заново формируем список
            fill_tovar_list();
        }
        list_tovari.requestFocusFromTouch();
        list_tovari.setSelection(listtouchposition);
    }

    public void delete_tovar_from_list(final String _tovar_to_del, String _tovar_name_to_del) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_warning)); // vnimanie
        builder.setMessage(getString(R.string.sales_tovar_delete).replaceAll("@", _tovar_name_to_del));

        builder.setNegativeButton(getString(R.string.app_no), new DialogInterface.OnClickListener() {
            //@Override
            public void onClick(DialogInterface dialog, int item) {
                dialog.cancel();
            }
        });
        builder.setPositiveButton(getString(R.string.app_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                try {
                    LoginActivity.database.delete("_operations_info_proiz", "_tovar_id='" + _tovar_to_del + "' and _tranzcode='" + _tranzaction + "'", null);
                } catch (Exception e) {
                    return;
                }

                listtouchposition = -1;
                //заново формируем список
                fill_tovar_list();

                list_tovari.requestFocusFromTouch();
                list_tovari.setSelection(0);
            }
        });
        builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    int check_fields() {
        int otvet = 0;
        if (
                txt_tsex.length() == 0
                        || txt_tovar.length() == 0
                        || txt_kolvo.length() == 0
                ) {
            otvet = 1;
        }
        return otvet;
    }

    int save_tovar() {
        int otvet = 0;

//        gen_tranzcode();

        ContentValues content = new ContentValues();
        //прежде чем добавить запись, мы должны проверить, если такой товар есть в списке, то мы просто должны увеличить кол-во
        String[] s = new String[]{"_kolvo"};
        Cursor cur = LoginActivity._myLayout.query("_operations_info_proiz", s, "_tovar_id='" + stovar_id + "' and _tranzcode='" + _tranzaction + "'", "");
        if (cur.getCount() > 0) {
            content.put("_kolvo", String.valueOf(Integer.parseInt(cur.getString(0)) + Integer.parseInt(stovar_kolvo)));
            try {
                LoginActivity.database.update("_operations_info_proiz", content, "_tovar_id='" + stovar_id + "' and _tranzcode='" + _tranzaction + "'", null);
            } catch (Exception e) {
                gen_message(Proizvodtsvo_Activity._myLayout, getString(R.string.app_warning), getString(R.string.app_error3));
                otvet = 1;
            }
            return otvet;
        }

        //записываем в табл. operations_common_info
        content.put("_tranzcode", _tranzaction);
        content.put("_login", LoginActivity._myLayout.user_login);
        content.put("_data_ot", _tranzaction);
        content.put("_client_account", stsex_account);
        content.put("_client_name", stsex_name);
        content.put("_status", "5");
        try {
            long i = LoginActivity.database.insert("_operations_common_info_proiz", null, content);
            if (i == -1) {
                otvet = 1;
            } else {
                content.clear();
                //записываем в табл. operations_info
                content.put("_tranzcode", _tranzaction);
                content.put("_tovar_id", stovar_id);
                content.put("_tovar_name", stovar_name);
                content.put("_kolvo", stovar_kolvo);
                content.put("_ed_izm", "");
                content.put("_tsena", "0");
                content.put("_summa", "0");

                try {
                    i = LoginActivity.database.insert("_operations_info_proiz", null, content);
                    if (i == -1) {
                        otvet = 1;
                    }
                } catch (Exception e) {
                    otvet = 1;
                }
            }
        } catch (Exception e) {
            otvet = 1;
        }
        content.clear();
        return otvet;
    }


    public void fill_tovar_list() {
        //list_tovari = (ListView) findViewById(R.id.list_tovari);
        ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();

        String[] s = {"*"};
        int kol = 0;
        double obshs = 0;
        String _tranz_for_first_time = null;
        Cursor cur = LoginActivity._myLayout.query("_operations_info_proiz", s, "_tranzcode='" + _tranzaction + "'", "_tovar_name");

        if (cur.getCount() > 0) {
            _tovar_idrref_to_del = new String[cur.getCount()];
            _tovar_description_to_del = new String[cur.getCount()];
            _tovar_kolvo_to_change = new int[cur.getCount()];
            //_tovar_kolvo_v_nalichii_to_change = new int[cur.getCount()];
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("tovar", cur.getString(2));
                map.put("kolvo", cur.getString(3));
                mylist.add(map);

                _tovar_idrref_to_del[kol] = cur.getString(1);
                _tovar_description_to_del[kol] = cur.getString(2);
                _tovar_kolvo_to_change[kol] = Integer.parseInt(cur.getString(3));
                //_tovar_kolvo_v_nalichii_to_change[kol] = Integer.parseInt(cur.getString(14));
                obshs += (Double.parseDouble(cur.getString(3).replace(',', '.')) * 1);
                kol++;
            } while (cur.moveToNext());
            cur.close();
        }
        txt_proiz_itogo.setText(getString(R.string.sales_tovar_summa_itogo) + " " + String.valueOf(new DecimalFormat("##.##").format(obshs)));

        SimpleAdapter tovarlist = new SimpleAdapter(this, mylist, R.layout.list_tovari_rows_proiz,
                new String[]{"tovar", "kolvo"}, new int[]{R.id.tovari_name_proiz, R.id.tovari_kolvo_proiz});
        list_tovari.setAdapter(tovarlist);
        list_tovari.setClickable(true);

        list_tovari.requestFocusFromTouch();
        list_tovari.setSelection(listtouchposition);
        cur = null;
    }

    void gen_tranzcode() {
        SimpleDateFormat dformat = new SimpleDateFormat("yyMMddHHmmss");
        Date date = new Date();
        _tranzaction = dformat.format(date);
    }

    public void gen_message(Context context, String errtitle, String errtext) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(errtitle);
        builder.setMessage(errtext);
        builder.setNegativeButton(getString(R.string.app_cancel), null);
        builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    public void set_tovari_list_headers() {
        ListView list_tovari_heads = (ListView) findViewById(R.id.list_tovari_heads_proiz);
        ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();
        list_tovari_heads.addHeaderView(getLayoutInflater().inflate(R.layout.list_tovari_headers_proiz, null, false));

        SimpleAdapter tovarlist = new SimpleAdapter(this, mylist, R.layout.list_tovari_rows_proiz,
                new String[]{"tovar", "kolvo"}, new int[]{R.id.tovari_name_proiz, R.id.tovari_kolvo_proiz});
        list_tovari_heads.setAdapter(tovarlist);
        list_tovari_heads.setClickable(false);

        //header titles
        TextView txt = (TextView) findViewById(R.id.htovar_name_proiz);
        txt.setText(R.string.sales_tovar_naimenovanie);
        //kolvo
        txt = (TextView) findViewById(R.id.htovari_kolvo_proiz);
        txt.setText(R.string.sales_tovar_kolvo);
    }

    void clear_tables() {
        try {
            LoginActivity.database.delete("_operations_common_info_proiz", "_tranzcode='" + _tranzaction + "'", null);
        } catch (Exception e) {
            return;
        }

        try {
            LoginActivity.database.delete("_operations_info_proiz", "_tranzcode='" + _tranzaction + "'", null);
        } catch (Exception e) {
            return;
        }
        _tovar_idrref_to_del = null;    //обнуляем, иначе будет твердить, что операция не закрыта
        gen_tranzcode();
    }

    void clear_fields() {
        stsex_name = "";
        stsex_account = "";
        sclient_balance = "";
        stovar_name = "";
        stovar_id = "";
        stovar_price = "";
        stovar_ostatok = "";
        //txt_client.setText("");
        txt_tovar.setText("");
        txt_kolvo.setText("");
        //txt_price.setText("");
        //txt_summa.setText("");
        txt_proiz_itogo.setText(getString(R.string.sales_tovar_summa_itogo));
    }

    void fill_list(String _type, String _sklad_id) {
        if(LoginActivity._myLayout.app_mode == true) {   //online
            JSONObject jsonObjSend = new JSONObject();
            try {
                // Add key/value pairs
                Boolean rememb = true;
                jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
                jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
                if (_type.equalsIgnoreCase("2"))
                    jsonObjSend.put("Sklad_id", _sklad_id);

                // Output the JSON object we're sending to Logcat:
                Log.d("json", jsonObjSend.toString(2));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Url_Params params = null;
            if (_type.equalsIgnoreCase("1")) {
//            params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/getListOfSklads/", jsonObjSend);
                params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/getListOfFas/", jsonObjSend);
            } else {
                params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/getListOfTovarsOfSklad/", jsonObjSend);
            }
            http_fill_list_tsex fill_list_tsex = new http_fill_list_tsex(Proizvodtsvo_Activity.this);
            fill_list_tsex.execute(params);
        }else{
            if (_type.equalsIgnoreCase("1")) {
                LoginActivity._myLayout.get_list_from_pref(5, "List_fas");
            }else{
                LoginActivity._myLayout.get_list_from_pref(6, "List_tovars_of_sklad");
            }
        }
    }

    ///////// WORKING WITH BACK BUTTON //////////////////////////////////////
    @Override
    public void onBackPressed() {
        if(LoginActivity._myLayout.app_mode)
            clear_tables();
        finish();
    }

    //создаем меню
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        menu.add(Menu.NONE, LoginActivity.IDM_MAIN, menu.NONE, getString(R.string.app_menu2))
                .setIcon(android.R.drawable.ic_menu_revert);
        menu.add(Menu.NONE, LoginActivity.IDM_CANCEL, menu.NONE, getString(R.string.app_menu3))
                .setIcon(android.R.drawable.ic_menu_revert);
        return (super.onCreateOptionsMenu(menu));
    }

    ;

    //обрабатываем выбор пункта меню
    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case LoginActivity.IDM_MAIN:
                if(LoginActivity._myLayout.app_mode)
                    clear_tables();
                finish();
                break;
            case LoginActivity.IDM_CANCEL:
                cancel_sale();
                break;
            default:
                return false;
        }
        return true;
    }

    ;
}
