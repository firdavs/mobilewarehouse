package com.mobilewarehouse.firdavs.mobilewarehouse;

import org.json.JSONObject;

/**
 * Created by Firdavs on 26.02.2015.
 */
public class Photo_Url_Params {
    String url;
    String file_name;
    JSONObject jsonObjSend;

    //UrlParams(String url, JSONObject jsonObjSend) {
    Photo_Url_Params(String url, String file_name, JSONObject jsonObjSend) {
        this.url = url;
        this.file_name = file_name;
        this.jsonObjSend = jsonObjSend;
    }
}
