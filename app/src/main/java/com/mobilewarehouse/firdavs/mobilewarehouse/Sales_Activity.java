package com.mobilewarehouse.firdavs.mobilewarehouse;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Firdavs on 23.02.2015.
 */
public class Sales_Activity extends Activity {
    public static Sales_Activity _myLayout;
    public static String[] client_name;
    public static String[] client_account;
    public static String[] client_balance;
    public static String sclient_name, sclient_account, sclient_balance, sclient_balance2;

    public static String[] tovar_name, list_tovar_name;
    public static String[] tovar_id, list_tovar_id;
    public static String[][] tovar_price, list_tovar_price;
    //public static String[] list_tovar_price;
    public static String[] tovar_ostatok, list_tovar_ostatok, default_tovar_price;
    public static String stovar_name, stovar_id, stovar_price, stovar_ostatok, stovar_kolvo;
    public static String[] stovar_prices;
    public static String _tranzaction;
    public static Boolean txtchanged = true;
    public static Boolean dont_show_list = true;
    public static String sales_client_prixod;
    public static String sales_client_account;
    public static double summa_pokupki;
    public static boolean repeat_order = false;

    int[] _tovar_kolvo_to_change;
    String[] _tovar_idrref_to_del = null;   //в этот массив запоминаем айди товаров, и при используем при удалении
    String[] _tovar_description_to_del = null;   //в этот массив запоминаем названия товаров, и при используем при удалении

    public AutoCompleteTextView txt_client;
    public AutoCompleteTextView txt_tovar;
    public EditText txt_kolvo;
    public EditText txt_price;
    public EditText txt_summa;
    public TextView txt_sales_summa;
    public ImageButton btn_add;
    public ImageButton btn_plus;
    public ImageButton btn_minus;
    public ImageButton btn_delete;
    public ImageButton btn_save;
    public ImageButton btn_tovar_select;
    public ImageButton btn_client_select;
    public ImageButton btn_take_photo;
    public ImageButton btn_show_report;
    public TextView lbl_klient, lbl_client_ostatok;
    public WebView web_tovar_info;
    ListView list_tovari;

    public View selprevrow;    //выбранный айтем в списке товаров
    public static int listtouchposition = -1;    //тоже, выбранный айтем в списке товаров

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.slide_to_right, R.anim.slide_to_left);
        setContentView(R.layout.activity_menu_sales);
        Sales_Activity._myLayout = this;
        this.setTitle(LoginActivity._myLayout.app_title);

        web_tovar_info = (WebView) findViewById(R.id.web_tovar_info);
        txt_client = (AutoCompleteTextView) findViewById(R.id.edt_sales_klient);
        txt_tovar = (AutoCompleteTextView) findViewById(R.id.edt_sales_tovar);
        txt_kolvo = (EditText) findViewById(R.id.edt_sales_kolvo);
        txt_price = (EditText) findViewById(R.id.edt_sales_tsena);
        txt_summa = (EditText) findViewById(R.id.edt_sales_summa);
        btn_add = (ImageButton) findViewById(R.id.btn_sales_add);
        btn_plus = (ImageButton) findViewById(R.id.btn_sales_plus);
        btn_minus = (ImageButton) findViewById(R.id.btn_sales_minus);
        btn_delete = (ImageButton) findViewById(R.id.btn_sales_delete);
        btn_tovar_select = (ImageButton) findViewById(R.id.btn_tovar_select);
        btn_client_select = (ImageButton) findViewById(R.id.btn_client_select);
        btn_take_photo = (ImageButton) findViewById(R.id.btn_take_photo);
        btn_show_report = (ImageButton) findViewById(R.id.btn_sales_orders);
        btn_save = (ImageButton) findViewById(R.id.btn_sales_save);
        list_tovari = (ListView) findViewById(R.id.list_tovari);
        txt_sales_summa = (TextView) findViewById(R.id.txt_sales_summa);
        lbl_klient = (TextView) findViewById(R.id.lbl_sales_klient);
        lbl_client_ostatok = (TextView) findViewById(R.id.lbl_client_ostatok);
        TextView txt_sales_caption = (TextView) findViewById(R.id.txt_sales_caption);

        btn_save.setEnabled(false);

        //esli idet peredacha tovaru ot man2man
        //to pryachem pole txt_price
        if (Operations_menuActivity._myLayout._operation.equalsIgnoreCase("_man2man")) {
            txt_price.setEnabled(false);
            txt_sales_caption.setText(getString(R.string.operation_menu_man2man));

            TextView txt_sales_tsena = (TextView) findViewById(R.id.txt_sales_tsena);
            TextView txt_sales_itogo = (TextView) findViewById(R.id.txt_sales_itogo);

            txt_sales_tsena.setVisibility(View.GONE);
            txt_price.setVisibility(View.GONE);
            txt_sales_itogo.setVisibility(View.GONE);
            txt_summa.setVisibility(View.GONE);
        } else if (Operations_menuActivity._myLayout._operation.equalsIgnoreCase("_vozvrat")) {
            txt_sales_caption.setText(getString(R.string.operation_menu_vozvrat));
        }

        //zapolnyaem polya
        if (sclient_balance != null) {
            sclient_balance2 = sclient_balance;
            lbl_klient.setText(getString(R.string.sales_client));// + "[" + sclient_balance + "]");
            lbl_client_ostatok.setText(getString(R.string.sales_client_ostatok2) + sclient_balance);
        } else {
            btn_client_select.setEnabled(false);
        }
        txt_client.setText(sclient_name);

        //iz servera poluchaem spisok tovarov
        dont_show_list = true;
        txtchanged = true;
        if(LoginActivity._myLayout.app_mode == true) {
            fill_list("", "2");
        }else{
            LoginActivity._myLayout.get_list_from_pref(3, "List_tovari");
        }

        //generiruem tranzcode
        gen_tranzcode();

        //чистим таблицы
        clear_tables();

        //generiruem zagolovki spiska
        set_tovari_list_headers();

        //читаем список из табл.
        fill_tovar_list();

        //show report
        btn_show_report.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (sclient_account != null) {
                    get_otchet_data();
                }
            }
        });

        //take photo
        btn_take_photo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (sclient_account != null) {
                    Intent intent = new Intent(getApplicationContext(), Photo_Activity.class);
                    startActivity(intent);
                }
            }
        });

        //vybor tovara iz spiska
        btn_tovar_select.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                show_tovar_list();
            }
        });

        //vybor clienta iz spiska
        btn_client_select.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                show_client_list();
            }
        });

        //удаление товара
        btn_delete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (listtouchposition != -1) {   //по умолч. -1, поэтому, если не -1, то товар уже выбран
                    delete_tovar_from_list(_tovar_idrref_to_del[listtouchposition], _tovar_description_to_del[listtouchposition]);
                } else {
                    //выберите товар из списка, пожалуйста!
                    gen_message(Sales_Activity._myLayout, getString(R.string.app_warning), getString(R.string.sales_tovar_select));
                }
            }
        });

        //увеличиваем колво товара
        btn_plus.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (listtouchposition != -1) {   //по умолч. -1, поэтому, если не -1, то товар уже выбран
                    change_tovar_kol(1, _tovar_idrref_to_del[listtouchposition]);
                } else {
                    //выберите товар из списка, пожалуйста!
                    gen_message(Sales_Activity._myLayout, getString(R.string.app_warning), getString(R.string.sales_tovar_select));
                }
            }
        });

        //уменьшааем колво товара
        btn_minus.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (listtouchposition != -1) {   //по умолч. -1, поэтому, если не -1, то товар уже выбран
                    change_tovar_kol(-1, _tovar_idrref_to_del[listtouchposition]);
                } else {
                    //выберите товар из списка, пожалуйста!
                    gen_message(Sales_Activity._myLayout, getString(R.string.app_warning), getString(R.string.sales_tovar_select));
                }
            }
        });

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alrtauth = new AlertDialog.Builder(Sales_Activity.this);
                alrtauth.setTitle(getString(R.string.app_warning));
                stovar_kolvo = txt_kolvo.getText().toString();
                stovar_price = txt_price.getText().toString();
                if (check_fields() == 0)   //все поля заполнены
                {
                    if (save_tovar() == 0) {
                        //set client field disabled
                        txt_client.setEnabled(false);
                        //чистим поля
                        clear_fields();
                        //регенерируем заново список товаров
                        fill_tovar_list();
                        //dat vozmozhnost polzovatsya http_fill_list
                        txtchanged = true;
                        txt_tovar.requestFocus();
                        btn_save.setEnabled(true);
                        //товар успешно сохранен
                        //alrtauth.setMessage(getString(R.string.sales_tovar_added));
                        Toast.makeText(Sales_Activity.this, getString(R.string.sales_tovar_added), Toast.LENGTH_LONG).show();
                        return;
                    } else {
                        //чистим таблицы
                        clear_tables();

                        //произошла ошибка во время записи
                        alrtauth.setMessage(getString(R.string.sales_tovar_add_err));
                    }
                } else {
                    //заполните поля
                    alrtauth.setMessage(getString(R.string.app_fill_field));
                }
                alrtauth.setCancelable(false);
                alrtauth.setNegativeButton(getString(R.string.app_ok), null); //ok
                alrtauth.show();

            }
        });

        txt_client.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (txt_client.getText().length() > 2
                        && txt_client.getText().length() < 11
                        && txtchanged == true) {
                    fill_list(txt_client.getText().toString(), "1");
                }
            }
        });

        txt_client.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                //position v massive
                sclient_name = client_name[position];
                sclient_account = client_account[position];
                sclient_balance = client_balance[position];
                txtchanged = false;

                //srazu vyzyvaem spisok tovarov
                //fill_list("","2");
                //pokazyvaem ostatok klienta
                lbl_klient.setText(getString(R.string.sales_client) + "[" + sclient_balance + "]");
            }
        });

        txt_tovar.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (txt_tovar.getText().length() > 2
                        && txt_tovar.getText().length() < 11
                        && txtchanged == true) {

                    //vremenno uburaem
                    fill_list(txt_tovar.getText().toString(), "2");
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        txt_tovar.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                txtchanged = false;
                //position v massive
                stovar_name = tovar_name[position];
                stovar_id = tovar_id[position];
                stovar_price = tovar_price[position][0]; //vremenno

                fill_tovar_prices_to_arr(position);

                stovar_ostatok = tovar_ostatok[position];
                txt_price.setText(stovar_price);
                txt_kolvo.setHint(stovar_ostatok);
                txt_kolvo.requestFocus();

            }
        });

        txt_tovar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (txt_tovar.getRight() - txt_tovar.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        txt_tovar.setText("");
                        txt_kolvo.setText("");
                        txt_kolvo.setHint("");
                        txt_price.setText("");
                        txt_summa.setText("");

                        txtchanged = true;
                        return true;
                    }
                }
                return false;
            }
        });

        txt_client.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (txt_client.getRight() - txt_client.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        txt_client.setText("");
                        lbl_klient.setText(getString(R.string.sales_client));
                        txtchanged = true;
                        return true;
                    }
                }
                return false;
            }
        });

        txt_kolvo.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    txt_summa.setText(String.valueOf(new DecimalFormat("##.##").format(Integer.parseInt(txt_kolvo.getText().toString()) * Double.parseDouble(txt_price.getText().toString()))));
                } catch (Exception e) {
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        txt_price.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    txt_summa.setText(String.valueOf(new DecimalFormat("##.##").format(Integer.parseInt(txt_kolvo.getText().toString()) * Double.parseDouble(txt_price.getText().toString()))));
                } catch (Exception e) {
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        list_tovari.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if (selprevrow != null) {
                    selprevrow.setBackgroundResource(android.R.color.transparent);
                }
                selprevrow = view;
                //view.requestFocusFromTouch();
                //view.setBackgroundResource(R.drawable.abc_list_pressed_holo_dark);

                listtouchposition = position;
                fill_tovar_list();
            }
        });

        txt_price.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    show_tovar_price_list();
                }
            }
        });

        //send2server
        btn_save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                mnu_send_zakaz();
            }
        });
    }

    //pokazyvaem spisok цен tovarа
    void show_tovar_price_list() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.sales_tovar_price_select)); // заголовок для диалога
        builder.setItems(stovar_prices, new DialogInterface.OnClickListener() {
            //@Override
            public void onClick(DialogInterface dialog, int item) {
                stovar_price = stovar_prices[item];
                txt_price.setText(stovar_price);
                txt_kolvo.requestFocus();
            }
        });
        builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    //pokazyvaem spisok цен tovarа
    void show_cancel_list() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.sales_tovar_cancel_prichina)); // заголовок для диалога
        builder.setItems(Marshrut_recordsActivity._myLayout.cancel_list, new DialogInterface.OnClickListener() {
            //@Override
            public void onClick(DialogInterface dialog, int item) {
                send_cancel(sclient_account, Marshrut_recordsActivity._myLayout.cancel_list_id[item]);
            }
        });
        builder.setCancelable(true);
        builder.create();
        builder.show();
    }

    void send_cancel(String client_account, String cancel_id)
    {
        if(LoginActivity._myLayout.app_mode) {
            JSONObject jsonObjSend = new JSONObject();
            try {
                jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
                jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
                jsonObjSend.put("Client", client_account);
                jsonObjSend.put("CancelID", cancel_id);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/sendCancel/", jsonObjSend);
            http_send_cancel_reason cancel_me = new http_send_cancel_reason(Sales_Activity.this);
            cancel_me.execute(params);
        }else{
            if (save_cancel(cancel_id) != 0)
                Toast.makeText(Sales_Activity.this, getString(R.string.sales_prixod_save_err), Toast.LENGTH_LONG).show();
        }

        clear_fields();
        fill_tovar_list();
        txt_client.setEnabled(true);
        txt_client.setText("");
        txt_kolvo.setHint("");
        txt_client.requestFocus();
        txtchanged = true;
        lbl_klient.setText(getString(R.string.sales_client));

        show_client_list();
    }

    //pokazyvaem spisok klientov
    void show_client_list() {
        if(LoginActivity._myLayout.app_mode) {
            clear_tables();
        }
        Intent intent = new Intent(getApplicationContext(), Marshrut_records_accountsActivity.class);
        startActivity(intent);
        finish();
    }

    //pokazyvaem spisok tovarov
    void show_tovar_list() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.sales_tovar_select)); // заголовок для диалога
        builder.setItems(list_tovar_name, new DialogInterface.OnClickListener() {
            //@Override
            public void onClick(DialogInterface dialog, int item) {
                btn_save.setEnabled(false);
                txtchanged = false;
                //item v massive
                stovar_name = list_tovar_name[item];
                stovar_id = list_tovar_id[item];
//                stovar_price = list_tovar_price[item][0];

                fill_tovar_prices_to_arr(item);

                stovar_ostatok = list_tovar_ostatok[item];
                txt_tovar.setText(stovar_name);
//                txt_price.setText(stovar_price);
                txt_kolvo.setHint(stovar_ostatok);
//                txt_kolvo.requestFocus();

                // цена по умолчанию, сразу заполняем поле цена
                stovar_price = default_tovar_price[item];
                txt_price.setText(stovar_price);
                txt_kolvo.requestFocus();
                //
                get_tovar_info();
            }
        });
        //builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    void fill_tovar_prices_to_arr(int position) {
        String _prc;
        stovar_prices = new String[tovar_price[position].length];
        for (int i = 0; i < tovar_price[position].length; i++) {
            _prc = tovar_price[position][i];
            stovar_prices[i] = _prc;
        }
    }

    //otmena zayavki, 4istim tabl i polya
    void cancel_sale() {
        clear_tables();
        //prosim ukazat prichinu otmeny
        show_cancel_list();
    }

    void mnu_send_zakaz() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.sales_tovar_send_to_server)); // заголовок для диалога
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_send_zakaz_to_server, null);

        double ostatok = Float.parseFloat(sclient_balance2.replace(',', '.'));
        TextView txt_ostatok = (TextView) view.findViewById(R.id.txt_sales_account_ostatok);
        TextView txt_purchase_sum = (TextView) view.findViewById(R.id.txt_sales_purchase_sum);
        TextView txt_itogo = (TextView) view.findViewById(R.id.txt_sales_itogo);

        txt_purchase_sum.setText(getString(R.string.sales_client_purchase) + " " + String.valueOf(new DecimalFormat("##.##").format(summa_pokupki)));
        txt_ostatok.setText(getString(R.string.sales_client_ostatok2) + " " + sclient_balance2);
        txt_itogo.setText(getString(R.string.sales_tovar_summa_itogo) + " " + String.valueOf(new DecimalFormat("##.##").format(Math.abs(ostatok - summa_pokupki))));
        builder.setView(view);
        //builder.setMessage(getString(R.string.sales_tovar_send_to_server));
        builder.setNegativeButton(getString(R.string.app_no), new DialogInterface.OnClickListener() {
            //@Override
            public void onClick(DialogInterface dialog, int item) {
                dialog.cancel();
            }
        });
        builder.setPositiveButton(getString(R.string.app_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                Dialog f = (Dialog) dialog;
                EditText edt_sales_prixod = (EditText) f.findViewById(R.id.edt_sales_client_prixod);
                CheckBox chk_repeat = (CheckBox) f.findViewById(R.id.chk_repeat);

                if (edt_sales_prixod.getText().toString().trim().length() == 0) {
                    //generate_message(getString(R.string.app_warning), getString(R.string.app_fill_field));
                    edt_sales_prixod.setText("0");
                }
                sales_client_prixod = edt_sales_prixod.getText().toString();
                repeat_order = chk_repeat.isChecked();

                if(LoginActivity._myLayout.app_mode) {
                    send_zakaz();
                }else{
                    if(!sales_client_prixod.equalsIgnoreCase("0")) {
                        if (save_prixod(sales_client_prixod, getString(R.string.sales_client_otpusk_tovara_comment)) != 0)
                            Toast.makeText(Sales_Activity.this, getString(R.string.sales_prixod_save_err), Toast.LENGTH_LONG).show();
                    }
                    show_client_list();
                    finish();
                }
            }
        });

        builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    void generate_message(String title, String message) {
        AlertDialog.Builder alert_message = new AlertDialog.Builder(this);
        alert_message.setTitle(title);
        alert_message.setMessage(message);
        alert_message.setCancelable(false);
        alert_message.setNegativeButton(getString(R.string.app_ok), null); //ok
        alert_message.show();
    }

    void get_otchet_data() {
        JSONObject jsonObjSend = new JSONObject();
        try {
            jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
            jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
            jsonObjSend.put("Id", "1011");  //1011 - vypiska klienta

            jsonObjSend.put("Param1", sclient_account);
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -14);
            Date dateBefore14Days = cal.getTime();

            jsonObjSend.put("Param2", android.text.format.DateFormat.format("yyyy-MM-dd", dateBefore14Days));
            jsonObjSend.put("Param3", android.text.format.DateFormat.format("yyyy-MM-dd", new java.util.Date()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/getReport/", jsonObjSend);
        http_get_otchet_data otchet_data_me = new http_get_otchet_data(Sales_Activity.this);
        otchet_data_me.execute(params);
    }

    void get_tovar_info() {
        JSONObject jsonObjSend = new JSONObject();
        try {
            jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
            jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
            jsonObjSend.put("Id", "1044");  //1044 - tovar info

            jsonObjSend.put("Param1", sclient_account);
            jsonObjSend.put("Param2", stovar_id);
            jsonObjSend.put("Param3", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/getReport/", jsonObjSend);
        http_get_tovar_info tovar_info_me = new http_get_tovar_info(Sales_Activity.this);
        tovar_info_me.execute(params);
    }

    public void send_zakaz() {
        //отправляем заказ на сервер
        String _tmp = "";
        String[] _send_data = new String[1];
        String[] s = {"*"};
        Cursor cur = null;
        JSONObject jsonObjSend = new JSONObject();
        JSONObject jsonObjSend2 = null;
        JSONArray jsonArr = null;
        int kol = 0;

        cur = LoginActivity._myLayout.query("_operations_common_info", s, "_tranzcode = '" + _tranzaction + "'", "");
        if (cur.getCount() > 0) {
            try {
                jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
                jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
                jsonObjSend.put("Client", cur.getString(3));
                jsonObjSend.put("Operation", "1");
                jsonObjSend.put("Role", LoginActivity._myLayout.login_type);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //
            sales_client_account = cur.getString(3);
            cur.close();

            //получаем данные о товарах
            cur = LoginActivity._myLayout.query("_operations_info", s, "_tranzcode = '" + _tranzaction + "'", "_tovar_name");

            if (cur.getCount() > 0) {
                jsonArr = new JSONArray();
                do {
                    try {
                        jsonObjSend2 = new JSONObject();
                        jsonObjSend2.put("Naimenovanie", cur.getString(2));
                        jsonObjSend2.put("Id", cur.getString(1));
                        jsonObjSend2.put("Count", cur.getString(3));
                        jsonObjSend2.put("Price", cur.getString(5));

                        try {
                            jsonArr.put(kol, jsonObjSend2);
                        } catch (Exception e) {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    kol++;
                } while (cur.moveToNext());
            }
            cur.close();

            try {
                jsonObjSend.put("Members", jsonArr);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Url_Params params = null;
            if (Operations_menuActivity._myLayout._operation.equalsIgnoreCase("_man2man")) {
                params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/perenosTovara/", jsonObjSend);
            } else if (Operations_menuActivity._myLayout._operation.equalsIgnoreCase("_vozvrat")) {
                params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/vozvratTovara/", jsonObjSend);
            } else {
                params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/otpuskTovara/", jsonObjSend);
            }
            http_send_sale send_sale = new http_send_sale(Sales_Activity.this);
            send_sale.execute(params);
        }
    }

    public void change_tovar_kol(int _kolvo, String _tovar_idrref_to_change) {
        boolean do_change = true;
        ContentValues content = new ContentValues();

        if (_kolvo == -1)    //minusovat
        {
            if (_tovar_kolvo_to_change[listtouchposition] <= 1)
                do_change = false;

            //list_tovari.requestFocusFromTouch();
            //list_tovari.setSelection(listtouchposition);
        }/*else if(_kolvo == 1){
            if(_tovar_kolvo_to_change[listtouchposition] == _tovar_kolvo_v_nalichii_to_change[listtouchposition])
                do_change = false;

            list_tovari.requestFocusFromTouch();
            list_tovari.setSelection(listtouchposition);
        }*/

        if (do_change) {
            content.put("_kolvo", String.valueOf(_tovar_kolvo_to_change[listtouchposition] + _kolvo));
            try {
                LoginActivity._myLayout.database.update("_operations_info", content, "_tovar_id='" + _tovar_idrref_to_change + "' and _tranzcode = '" + _tranzaction + "'", null);
            } catch (Exception e) {
                gen_message(Sales_Activity._myLayout, getString(R.string.app_warning), e.getMessage().toString());
                return;
            }
            //заново формируем список
            fill_tovar_list();

            //list_tovari.requestFocusFromTouch();
            //list_tovari.setSelection(listtouchposition);
        }
        list_tovari.requestFocusFromTouch();
        list_tovari.setSelection(listtouchposition);
        content = null;
    }

    public void delete_tovar_from_list(final String _tovar_to_del, String _tovar_name_to_del) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_warning)); // vnimanie
        builder.setMessage(getString(R.string.sales_tovar_delete).replaceAll("@", _tovar_name_to_del));

        builder.setNegativeButton(getString(R.string.app_no), new DialogInterface.OnClickListener() {
            //@Override
            public void onClick(DialogInterface dialog, int item) {
                dialog.cancel();
            }
        });
        builder.setPositiveButton(getString(R.string.app_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                try {
                    LoginActivity.database.delete("_operations_info", "_tovar_id='" + _tovar_to_del + "' and _tranzcode = '" + _tranzaction + "'", null);
                } catch (Exception e) {
                    return;
                }

                listtouchposition = -1;
                //заново формируем список
                fill_tovar_list();

                list_tovari.requestFocusFromTouch();
                list_tovari.setSelection(0);
            }
        });
        builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    int check_fields() {
        int otvet = 0;
        if (
                txt_client.length() == 0
                        || txt_tovar.length() == 0
                        || txt_kolvo.length() == 0
                        || txt_price.length() == 0
                        || txt_summa.length() == 0
                ) {
            otvet = 1;
        }
        return otvet;
    }

    int save_tovar() {
        int otvet = 0;

//        gen_tranzcode();

        ContentValues content = new ContentValues();
        //прежде чем добавить запись, мы должны проверить, если такой товар есть в списке, то мы просто должны увеличить кол-во
        String[] s = new String[]{"_kolvo"};
        Cursor cur = LoginActivity._myLayout.query("_operations_info", s, "_tovar_id='" + stovar_id + "' and _tranzcode = '" + _tranzaction + "'", "");
        if (cur.getCount() > 0) {
            content.put("_kolvo", String.valueOf(Integer.parseInt(cur.getString(0)) + Integer.parseInt(stovar_kolvo)));
            try {
                LoginActivity.database.update("_operations_info", content, "_tovar_id='" + stovar_id + "' and _tranzcode = '" + _tranzaction + "'", null);
            } catch (Exception e) {
                gen_message(Sales_Activity._myLayout, getString(R.string.app_warning), getString(R.string.app_error3));
                otvet = 1;
            }
            return otvet;
        }

        //записываем в табл. operations_common_info
        content.put("_tranzcode", _tranzaction);
        content.put("_login", LoginActivity._myLayout.user_login);
        content.put("_data_ot", _tranzaction);
        content.put("_client_account", sclient_account);
        content.put("_client_name", sclient_name);
        content.put("_status", "5");
        try {
            long i = LoginActivity.database.insert("_operations_common_info", null, content);
            if (i == -1) {
                otvet = 1;
            } else {
                content.clear();
                //записываем в табл. operations_info
                content.put("_tranzcode", _tranzaction);
                content.put("_tovar_id", stovar_id);
                content.put("_tovar_name", stovar_name);
                content.put("_kolvo", stovar_kolvo);
                content.put("_ed_izm", "");
                content.put("_tsena", stovar_price);
                content.put("_summa", "0");

                try {
                    i = LoginActivity.database.insert("_operations_info", null, content);
                    if (i == -1) {
                        otvet = 1;
                    }
                } catch (Exception e) {
                    otvet = 1;
                }
            }
        } catch (Exception e) {
            otvet = 1;
        }
        content.clear();
        return otvet;
    }

    int save_prixod(String sum, String comment) {
        int otvet = 0;
        ContentValues content = new ContentValues();
        //записываем в табл. prixody
        content.put("_tranzcode", _tranzaction);
        content.put("_client_account", sales_client_account);
        content.put("_sum", sum);
        content.put("_comment", comment);
        try {
            LoginActivity.database.insert("_prixody", null, content);
        } catch (Exception e) {
            otvet = 1;
        }
        content.clear();
        return otvet;
    }

    int save_cancel(String cancel_id) {
        int otvet = 0;
        ContentValues content = new ContentValues();
        //записываем в табл. _cancels
        content.put("_tranzcode", _tranzaction);
        content.put("_client_account", sales_client_account);
        content.put("_cancel_id", cancel_id);
        try {
            LoginActivity.database.insert("_cancels", null, content);
        } catch (Exception e) {
            otvet = 1;
        }
        content.clear();
        return otvet;
    }

    public void fill_tovar_list() {
        //list_tovari = (ListView) findViewById(R.id.list_tovari);
        ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();

        String[] s = {"*"};
        int kol = 0;
        double obshs = 0;
        String _tranz_for_first_time = null;
        Cursor cur = LoginActivity._myLayout.query("_operations_info", s, "_tranzcode = '" + _tranzaction + "'", "_tovar_name");

        if (cur.getCount() > 0) {
            _tovar_idrref_to_del = new String[cur.getCount()];
            _tovar_description_to_del = new String[cur.getCount()];
            _tovar_kolvo_to_change = new int[cur.getCount()];
            //_tovar_kolvo_v_nalichii_to_change = new int[cur.getCount()];
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("tovar", cur.getString(2));
                map.put("kolvo", cur.getString(3));
                //map.put("ed_izm", cur.getString(11));
                map.put("tsena", cur.getString(5));
                map.put("summa", String.valueOf(new DecimalFormat("##.##").format(Double.parseDouble(cur.getString(3).replace(',', '.')) * Double.parseDouble(cur.getString(5)))));
                mylist.add(map);

                _tovar_idrref_to_del[kol] = cur.getString(1);
                _tovar_description_to_del[kol] = cur.getString(2);
                _tovar_kolvo_to_change[kol] = Integer.parseInt(cur.getString(3));
                //_tovar_kolvo_v_nalichii_to_change[kol] = Integer.parseInt(cur.getString(14));
                obshs += (Double.parseDouble(cur.getString(3).replace(',', '.')) * Double.parseDouble(cur.getString(5)));
                kol++;
            } while (cur.moveToNext());
            cur.close();
        }
        txt_sales_summa.setText(getString(R.string.sales_tovar_summa_itogo) + " " + String.valueOf(new DecimalFormat("##.##").format(obshs)));
        summa_pokupki = obshs;

        SimpleAdapter tovarlist = new SimpleAdapter(this, mylist, R.layout.list_tovari_rows,
                new String[]{"tovar", "kolvo", "tsena", "summa"}, new int[]{R.id.tovari_name, R.id.tovari_kolvo, R.id.tovari_tsena, R.id.tovari_summa});
        list_tovari.setAdapter(tovarlist);
        list_tovari.setClickable(true);

        list_tovari.requestFocusFromTouch();
        list_tovari.setSelection(listtouchposition);
        cur = null;
    }

    void gen_tranzcode() {
        SimpleDateFormat dformat = new SimpleDateFormat("yyMMddHHmmss");
        Date date = new Date();
        _tranzaction = dformat.format(date);
    }

    public void gen_message(Context context, String errtitle, String errtext) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(errtitle);
        builder.setMessage(errtext);
        builder.setNegativeButton(getString(R.string.app_cancel), null);
        builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    public void set_tovari_list_headers() {
        ListView list_tovari_heads = (ListView) findViewById(R.id.list_tovari_heads);
        ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();
        list_tovari_heads.addHeaderView(getLayoutInflater().inflate(R.layout.list_tovari_headers, null, false));

        SimpleAdapter tovarlist = new SimpleAdapter(this, mylist, R.layout.list_tovari_rows,
                new String[]{"tovar", "kolvo", "tsena", "summa"}, new int[]{R.id.tovari_name, R.id.tovari_kolvo, R.id.tovari_tsena, R.id.tovari_summa});
        list_tovari_heads.setAdapter(tovarlist);
        list_tovari_heads.setClickable(false);

        //header titles
        TextView txt = (TextView) findViewById(R.id.htovar_name);
        txt.setText(R.string.sales_tovar_naimenovanie);
        //kolvo
        txt = (TextView) findViewById(R.id.htovari_kolvo);
        txt.setText(R.string.sales_tovar_kolvo);
        //tsena
        txt = (TextView) findViewById(R.id.htovari_tsena);
        txt.setText(R.string.sales_tovar_tsena);
        //summa
        txt = (TextView) findViewById(R.id.htovari_summa);
        txt.setText(R.string.sales_tovar_summa);
    }

    void clear_tables() {
        try {
            LoginActivity.database.delete("_operations_common_info", "_tranzcode = '" + _tranzaction + "'", null);
        } catch (Exception e) {
            return;
        }

        try {
            LoginActivity.database.delete("_operations_info", "_tranzcode = '" + _tranzaction + "'", null);
        } catch (Exception e) {
            return;
        }
        _tovar_idrref_to_del = null;    //обнуляем, иначе будет твердить, что операция не закрыта
        gen_tranzcode();                //zanovo generiruem tranzcode
    }

    void clear_fields() {
        sclient_name = "";
        sclient_account = "";
        sclient_balance = "";
        stovar_name = "";
        stovar_id = "";
        stovar_price = "";
        stovar_ostatok = "";
        //txt_client.setText("");
        txt_tovar.setText("");
        txt_kolvo.setText("");
        txt_kolvo.setHint("");
        txt_price.setText("");
        txt_summa.setText("");
        txt_sales_summa.setText(getString(R.string.sales_tovar_summa_itogo));
        //lbl_klient.setText(getString(R.string.sales_client));
    }

    void fill_list(String _txt, String _type) {
        //esli operation==_man2man to menyaem _type na 3
        //chtobi poluchit spisok managerov
        if (Operations_menuActivity._myLayout._operation.equalsIgnoreCase("_man2man") && _type.equalsIgnoreCase("1")) {
            _type = "3";
        }

        JSONObject jsonObjSend = new JSONObject();
        try {
            // Add key/value pairs
            Boolean rememb = true;
            jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
            jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
            jsonObjSend.put("Like", _txt);
            jsonObjSend.put("Type", _type);
            jsonObjSend.put("Role", LoginActivity._myLayout.login_type);

            if (Operations_menuActivity._myLayout._operation.equalsIgnoreCase("_vozvrat") && _type.equalsIgnoreCase("2")) {
                jsonObjSend.put("Vozvrat", "1");
            }

            // Output the JSON object we're sending to Logcat:
            Log.d("json", jsonObjSend.toString(2));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/getlist/", jsonObjSend);
        http_fill_list fill_list = new http_fill_list(Sales_Activity.this);
        fill_list.execute(params);
    }

    //timer dlya pokaza spiska autocomplete
    public void show_drop_down_list(final int _list) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (_list == 1) {
                    txt_client.showDropDown();
                } else {
                    txt_tovar.showDropDown();
                }
            }
        }, 300);
    }

    ///////// WORKING WITH BACK BUTTON //////////////////////////////////////
    @Override
    public void onBackPressed() {
        show_client_list();
        //finish();
        //overridePendingTransition(R.anim.slide_to_left, R.anim.slide_to_right);
    }

    //создаем меню
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        menu.add(Menu.NONE, LoginActivity.IDM_MAIN, menu.NONE, getString(R.string.app_menu2))
                .setIcon(android.R.drawable.ic_menu_revert);
        menu.add(Menu.NONE, LoginActivity.IDM_CANCEL, menu.NONE, getString(R.string.app_menu3))
                .setIcon(android.R.drawable.ic_menu_revert);
        return (super.onCreateOptionsMenu(menu));
    }

    ;

    //обрабатываем выбор пункта меню
    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case LoginActivity.IDM_MAIN:
                show_client_list();
                //finish();
                break;
            case LoginActivity.IDM_CANCEL:
                cancel_sale();
                break;
            default:
                return false;
        }
        return true;
    }

    ;
}
