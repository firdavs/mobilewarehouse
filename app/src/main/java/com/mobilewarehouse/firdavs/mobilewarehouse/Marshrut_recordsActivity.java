package com.mobilewarehouse.firdavs.mobilewarehouse;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Firdavs on 09.03.2015.
 */
public class Marshrut_recordsActivity extends ListActivity {
    public static Marshrut_recordsActivity _myLayout;
    myAdapter ms_records_list;

    public static String[] ms_marshrut_name, ms_client_name, ms_client_account, ms_client_balance, cancel_list, cancel_list_id;
    public static int ms_operation_type = 0;
    public static String ms_marshrut, ms_marshrut_balance;
    public static Integer[] ms_operation_img;
    int img = 0;
    public static int client_count = 0;
    public static TextView txt_itogo;
    public static double itogo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //MainFormActivity.myLayout.set_theme(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marshrut_lists);
        Marshrut_recordsActivity._myLayout = this;
        this.setTitle(LoginActivity._myLayout.app_title);

        itogo = 0;
        txt_itogo =(TextView) findViewById(R.id.txt_marshrut_itogo);
        get_images();

        ms_records_list = new myAdapter(this);
        setListAdapter(ms_records_list);
    }

    void get_images() {
        for (int i = 0; i < ms_marshrut_name.length; i++) {
            try {
                switch (_myLayout.ms_operation_type) {
                    case 1:     //marshrut list
                        img = getResources().getIdentifier("sales_j", "drawable", "com.mobilewarehouse.firdavs.mobilewarehouse");
                        break;
                    default:    //vozvrat
                        img = getResources().getIdentifier("man2man_j", "drawable", "com.mobilewarehouse.firdavs.mobilewarehouse");
                        break;
                }
                Marshrut_recordsActivity._myLayout.ms_operation_img[i] = img;
            } catch (Exception e) {
            }
        }
    }

    public void onListItemClick(ListView parent, View v, int position, long id) {
        //list item click haminja
        //if(ms_operation_type == 1) { //query marshrut account
        ms_marshrut = ms_marshrut_name[position];
        ms_marshrut_balance = " [" + ms_client_balance[position] + "]";
        if(LoginActivity._myLayout.app_mode == true) {   //online
            get_marshrut_record_info(ms_marshrut);
        }else{
            LoginActivity._myLayout.get_list_from_pref(2, ms_marshrut);
        }
        /*else{
            //ms_operation_type = 1;
            Intent intent = null;
            if(LoginActivity._myLayout.operation_page == 1) {           //sales
                intent = new Intent(getApplicationContext(), Sales_Activity.class);
                Sales_Activity._myLayout.sclient_name = ms_client_name[position];
                Sales_Activity._myLayout.sclient_account = ms_client_account[position];
                Sales_Activity._myLayout.sclient_balance = ms_client_balance[position];
                Sales_Activity._myLayout.txtchanged = false;
            }else{  //LoginActivity._myLayout.operation_page = 2        //prixod ot clientov
                intent = new Intent(getApplicationContext(), Priem_sredstvActivity.class);
                Priem_sredstvActivity._myLayout.sclient_name = ms_client_name[position];
                Priem_sredstvActivity._myLayout.sclient_account = ms_client_account[position];
                Priem_sredstvActivity._myLayout.sclient_balance = ms_client_balance[position];
                Priem_sredstvActivity._myLayout.txtchanged = false;
            }
            startActivity(intent);
            finish();
        }*/

    }

    void get_marshrut_record_info(String _marshrut_name) {
        JSONObject jsonObjSend = new JSONObject();
        try {
            jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
            jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
            jsonObjSend.put("Name", _marshrut_name);
            jsonObjSend.put("Type", "5");

            Log.d("json", jsonObjSend.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/getList/", jsonObjSend);
        http_get_marshrut_list marshrut_record_info_me = null;
        if (LoginActivity._myLayout.operation_page == 3) {
            marshrut_record_info_me = new http_get_marshrut_list(Rasxodi_Activity._myLayout);
        } else {
            marshrut_record_info_me = new http_get_marshrut_list(Marshrut_recordsActivity.this);
        }
        marshrut_record_info_me.execute(params);

        if (LoginActivity._myLayout.operation_page == 3)
            finish();
    }

    public class myAdapter extends BaseAdapter {
        private LayoutInflater mLayoutInflater;

        public myAdapter(Context ctx) {
            mLayoutInflater = LayoutInflater.from(ctx);
        }

        public int getCount() {
            //int cnt = 0;
            return client_count;
            /*
            if(ms_operation_type == 1) {
                cnt = ms_marshrut_name.length;
            }else{
                cnt = ms_client_name.length;
            }

            return cnt;
            */
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }


        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null)
                convertView = mLayoutInflater.inflate(R.layout.item_marshrut_record, null);

            ImageView image = (ImageView) convertView.findViewById(R.id.img_mrsh_list);
            image.setImageResource(ms_operation_img[position]);

            TextView itm_op_name = (TextView) convertView.findViewById(R.id.itm_mrsh_list_name);
            itm_op_name.setText(ms_marshrut_name[position]);

            TextView itm_op_name_ostatok = (TextView) convertView.findViewById(R.id.itm_mrsh_list_name_ostatok);
            itm_op_name_ostatok.setText(ms_client_balance[position]);
            itogo += Float.valueOf(ms_client_balance[position]);
            txt_itogo.setText(String.valueOf(itogo));

            return convertView;
        }
    }

    ///////// WORKING WITH BACK BUTTON //////////////////////////////////////
    @Override
    public void onBackPressed() {
        if (ms_operation_type == 2) {
            ms_operation_type = 1;
        }
        finish();
    }

    //создаем меню
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        menu.add(Menu.NONE, LoginActivity.IDM_MAIN, menu.NONE, getString(R.string.app_menu2))
                .setIcon(android.R.drawable.ic_menu_revert);
        return (super.onCreateOptionsMenu(menu));
    }

    ;

    //обрабатываем выбор пункта меню
    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case LoginActivity.IDM_MAIN:
                finish();
                break;
            default:
                return false;
        }
        return true;
    }

    ;
}
