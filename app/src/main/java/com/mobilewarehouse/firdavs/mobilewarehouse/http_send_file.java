package com.mobilewarehouse.firdavs.mobilewarehouse;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Firdavs on 26.02.2015.
 */
public class http_send_file extends AsyncTask<Photo_Url_Params, Void, Void> {
    StringBuilder builder = new StringBuilder();
    Boolean _result = false;
    //ProgressDialog progressdlg;
    private Context context;
    String _list;
    JSONObject jsonObjRecv;

    public http_send_file(Context cxt) {
        context = cxt;
        //progressdlg = new ProgressDialog(context);
    }

    //@Override
    protected Void doInBackground(Photo_Url_Params... params) {
        String url = "http://" + params[0].url;
        String file_name = params[0].file_name;
        JSONObject jsonObjSend = params[0].jsonObjSend;

        try {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);

            MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
            entityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

            entityBuilder.addTextBody("filename", file_name);

            File f = new File(file_name);
            //InputStream is=new FileInputStream(file_name);
            entityBuilder.addPart("file", new FileBody(f));

            post.setEntity(entityBuilder.build());
            HttpResponse response = client.execute(post);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                InputStream instream = entity.getContent();
                String resultString = Main_menuActivity._myLayout.convertStreamToString(instream);
                instream.close();

                try {
                    jsonObjRecv = new JSONObject(resultString);
                    if (jsonObjRecv != null) {
                        try {
                            if (jsonObjRecv.get("Has_result").toString().equalsIgnoreCase("true")) {
                                _result = true;
                            }
                        } catch (Exception e) {
                            _result = false;
                        }
                    }
                } catch (Exception e) {
                    _result = false;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        //showDialog(context);
    }

    @Override
    protected void onPostExecute(Void result) {
        if (_result == true) {
            Parse_http_result _prs = new Parse_http_result();
            _prs.parse_result(context, "Send_photo_file", jsonObjRecv);
        } else {
            //Toast.makeText(this.context, context.getString(R.string.app_login_failes), Toast.LENGTH_LONG).show();
            String _error = "";
            try {
                _error = jsonObjRecv.get("message").toString();
            } catch (Exception e) {
            }
            Toast.makeText(this.context, _error, Toast.LENGTH_LONG).show();
        }
        //closeDialog();
    }
    /*
    private void closeDialog()
    {
        if (progressdlg != null){
            progressdlg.dismiss();
        }
    }

    private void showDialog(Context context)
    {
        if (progressdlg !=null){
            try{
                progressdlg.dismiss();
                progressdlg.cancel();
                progressdlg=null;
            }
            catch(Exception e)
            {}
        }
        if (progressdlg==null){
            progressdlg = new ProgressDialog(context);
            progressdlg.setMessage(context.getString(R.string.app_progress_fill_list));
            try{
                progressdlg.show();
            }
            catch(Exception e)
            {}
        }

    }
    */
}
