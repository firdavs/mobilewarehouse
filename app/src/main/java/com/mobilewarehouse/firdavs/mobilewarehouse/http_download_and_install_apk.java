package com.mobilewarehouse.firdavs.mobilewarehouse;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Firdavs on 01.04.2015.
 */
public class http_download_and_install_apk extends AsyncTask<String, String, Void> {

    ProgressDialog progressDialog;
    int status = 0;
    int lenghtOfFile = 0;
    ProgressDialog progressdlg;

    private Context context;
    /*
    public void setContext(Context context, ProgressDialog progress){
        this.context = context;
        this.progressDialog = progress;
    }
    */

    public http_download_and_install_apk(Context cxt) {
        context = cxt;
        progressdlg = new ProgressDialog(context);
        progressdlg.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressdlg.setMax(100);
        progressdlg.setProgress(100);
    }

    public void onPreExecute() {
        //progressDialog.show();
        showDialog(context);
    }

    @Override
    protected Void doInBackground(String... arg0) {
        try {
            URL url = new URL(arg0[0]);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setRequestProperty("Accept-Encoding", "identity");
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();

            lenghtOfFile = c.getContentLength();

            File sdcard = Environment.getExternalStorageDirectory();
            File myDir = new File(sdcard, "Android/data/com.mobilewarehouse.firdavs.mobilewarehouse/temp");
            myDir.mkdirs();
            File outputFile = new File(myDir, "temp.apk");
            if (outputFile.exists()) {
                outputFile.delete();
            }
            FileOutputStream fos = new FileOutputStream(outputFile);

            InputStream is = c.getInputStream();

            byte[] buffer = new byte[1024];
            int len1 = 0;
            long total = 0;
            while ((len1 = is.read(buffer)) != -1) {
                fos.write(buffer, 0, len1);

                total += len1;
                // publishing the progress....
                // After this onProgressUpdate will be called
                publishProgress("" + (int) ((total * 100) / lenghtOfFile));
            }
            fos.flush();
            fos.close();
            is.close();

            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(new File(sdcard, "Android/data/com.mobilewarehouse.firdavs.mobilewarehouse/temp/temp.apk")), "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // without this flag android returned a intent error!
            context.startActivity(intent);
        } catch (FileNotFoundException fnfe) {
            status = 1;
            Log.e("File", "FileNotFoundException! " + fnfe);
        } catch (Exception e) {
            Log.e("UpdateAPP", "Exception " + e);
        }
        return null;
    }

    protected void onProgressUpdate(String... progress) {
        // setting progress percentage
        progressdlg.setProgress(Integer.parseInt(progress[0]));
        //progressdlg.setMessage(context.getString(R.string.about_progress_dai) + ", " + progress[0] + "%");
    }

    public void onPostExecute(Void unused) {
        //progressDialog.dismiss();
        closeDialog();
        if (status == 1)
            Toast.makeText(context, context.getString(R.string.about_app_dai_error), Toast.LENGTH_LONG).show();
    }

    private void closeDialog() {
        if (progressdlg != null) {
            progressdlg.dismiss();
        }
    }

    private void showDialog(Context context) {
        if (progressdlg != null) {
            try {
                progressdlg.dismiss();
                progressdlg.cancel();
                progressdlg = null;
            } catch (Exception e) {
            }
        }
        if (progressdlg == null) {
            progressdlg = new ProgressDialog(context);
            progressdlg.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressdlg.setMax(100);
            progressdlg.setProgress(100);
            progressdlg.setMessage(context.getString(R.string.about_progress_dai));
            try {
                progressdlg.show();
            } catch (Exception e) {
            }
        }

    }
}
