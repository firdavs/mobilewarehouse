package com.mobilewarehouse.firdavs.mobilewarehouse;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import javax.crypto.spec.OAEPParameterSpec;

/**
 * Created by Firdavs on 23.02.2015.
 */
public class Operations_menuActivity extends Activity {
    public static String _operation;
    public static Operations_menuActivity _myLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //overridePendingTransition(R.anim.slide_to_right, R.anim.slide_to_left);
        setContentView(R.layout.activity_menu_operations);
        Operations_menuActivity._myLayout = this;
        this.setTitle(LoginActivity._myLayout.app_title);

        ImageButton mnu_sales = (ImageButton) findViewById(R.id.btn_sales);
        ImageButton mnu_priem = (ImageButton) findViewById(R.id.btn_kassa);
        ImageButton mnu_rasxodi = (ImageButton) findViewById(R.id.btn_rasxodi);
        ImageButton mnu_vozvrat = (ImageButton) findViewById(R.id.btn_vozvrat);
        ImageButton mnu_proiz = (ImageButton) findViewById(R.id.btn_proiz);
        ImageButton mnu_man2man = (ImageButton) findViewById(R.id.btn_man2man);
        TextView mnu_vozvrat_txt = (TextView) findViewById(R.id.txt_vozvrat);
        TextView mnu_man2man_txt = (TextView) findViewById(R.id.txt_man2man);
        TextView mnu_proiz_txt = (TextView) findViewById(R.id.txt_proiz);
        if(!LoginActivity._myLayout.login_type.equalsIgnoreCase("2")) {
            mnu_proiz.setVisibility(View.GONE);
            mnu_proiz_txt.setVisibility(View.GONE);
        }
        mnu_man2man.setVisibility(View.GONE);
        mnu_man2man_txt.setVisibility(View.GONE);

        mnu_vozvrat.setVisibility(View.GONE);
        mnu_vozvrat_txt.setVisibility(View.GONE);
        //}

        mnu_sales.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().setColorFilter(0x77000000, PorterDuff.Mode.SRC_ATOP);
                        v.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        //on button click
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        v.invalidate();

                        _operation = "_sales";
                        ////start_intent(1); //sales
                        LoginActivity._myLayout.operation_page = 1; //sales
                        if(LoginActivity._myLayout.app_mode == true) {   //online
                            get_marshrut_list();
                        }else{
                            LoginActivity._myLayout.get_list_from_pref(1, "List_marshrut");
                        }
                        break;
                    }
                    case MotionEvent.ACTION_CANCEL: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }
                return true;
            }
        });

        mnu_priem.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().setColorFilter(0x77000000, PorterDuff.Mode.SRC_ATOP);
                        v.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        //on button click
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        v.invalidate();

                        _operation = "_kassa";
                        //start_intent(2); //priem_sredtsv
                        LoginActivity._myLayout.operation_page = 2; //sales
//                        get_marshrut_list();
                        if(LoginActivity._myLayout.app_mode == true) {   //online
                            get_marshrut_list();
                        }else{
                            LoginActivity._myLayout.get_list_from_pref(1, "List_marshrut");
                        }
                        break;
                    }
                    case MotionEvent.ACTION_CANCEL: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }
                return true;
            }
        });

        mnu_rasxodi.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().setColorFilter(0x77000000, PorterDuff.Mode.SRC_ATOP);
                        v.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        //on button click
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        v.invalidate();

                        start_intent(3); //rasxodi
                        break;
                    }
                    case MotionEvent.ACTION_CANCEL: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }
                return true;
            }
        });

        mnu_proiz.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().setColorFilter(0x77000000, PorterDuff.Mode.SRC_ATOP);
                        v.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        //on button click
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        v.invalidate();

                        _operation = "_proiz";
                        start_intent(4); //proiz
                        break;
                    }
                    case MotionEvent.ACTION_CANCEL: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }
                return true;
            }
        });

        mnu_man2man.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().setColorFilter(0x77000000, PorterDuff.Mode.SRC_ATOP);
                        v.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        //on button click
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        v.invalidate();

                        _operation = "_man2man";
                        start_intent(1); //sales
                        break;
                    }
                    case MotionEvent.ACTION_CANCEL: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }
                return true;
            }
        });

        mnu_vozvrat.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().setColorFilter(0x77000000, PorterDuff.Mode.SRC_ATOP);
                        v.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        //on button click
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        v.invalidate();

                        _operation = "_vozvrat";
                        start_intent(1); //we use sales to make vozvrat
                        break;
                    }
                    case MotionEvent.ACTION_CANCEL: {
                        ImageButton view = (ImageButton) v;
                        view.getBackground().clearColorFilter();
                        view.invalidate();
                        break;
                    }
                }
                return true;
            }
        });
    }

    public void start_intent(int _page) {
        Intent intent = null;
        switch (_page) {
            case 1: //operations
                intent = new Intent(getApplicationContext(), Sales_Activity.class);
                break;
            case 2: //priem sredtsv
                intent = new Intent(getApplicationContext(), Priem_sredstvActivity.class);
                break;
            case 3: //rasxodi
                intent = new Intent(getApplicationContext(), Rasxodi_Activity.class);
                break;
            case 4: //proiz
                intent = new Intent(getApplicationContext(), Proizvodtsvo_Activity.class);
                break;
            default:
                intent = new Intent(getApplicationContext(), Operations_menuActivity.class);
                break;
        }
        startActivity(intent);
    }

    void get_marshrut_list() {
        JSONObject jsonObjSend = new JSONObject();
        try {
            jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
            jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
            jsonObjSend.put("Type", "4");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Url_Params params = new Url_Params(LoginActivity._myLayout.server_ip + "/android/getList/", jsonObjSend);
        http_get_marshrut_list marshrut_me = null;
        if (LoginActivity._myLayout.operation_page == 3) {
            marshrut_me = new http_get_marshrut_list(Rasxodi_Activity._myLayout);
        } else {
            marshrut_me = new http_get_marshrut_list(Operations_menuActivity.this);
        }
        marshrut_me.execute(params);
    }

    ///////// WORKING WITH BACK BUTTON //////////////////////////////////////
    @Override
    public void onBackPressed() {
        finish();
        //overridePendingTransition(R.anim.slide_to_left, R.anim.slide_to_right);
    }

    //создаем меню
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(Menu.NONE, LoginActivity.IDM_MAIN, menu.NONE, getString(R.string.app_menu1))
                .setIcon(android.R.drawable.ic_menu_sort_by_size);
        return (super.onCreateOptionsMenu(menu));
    }

    ;

    //обрабатываем выбор пункта меню
    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case LoginActivity.IDM_MAIN:
                finish();
                break;
            default:
                return false;
        }
        return true;
    }

    ;
}
