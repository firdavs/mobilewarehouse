package com.mobilewarehouse.firdavs.mobilewarehouse;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Firdavs on 09.03.2015.
 */
public class Otchet_recordsActivity extends ListActivity {
    public static Otchet_recordsActivity _myLayout;
    myAdapter ot_records_list;

    public static String[] ot_name, ot_id, ot_p1_source;
    public static Integer[] ot_img;
    public static Boolean[] ot_p1, ot_p2, ot_p3;
    int img = 0, sel_otchet = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //MainFormActivity.myLayout.set_theme(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otchet_records);
        Otchet_recordsActivity._myLayout = this;
        this.setTitle(LoginActivity._myLayout.app_title);

        get_images();

        ot_records_list = new myAdapter(this);
        setListAdapter(ot_records_list);
    }

    void get_images() {
        for (int i = 0; i < ot_name.length; i++) {
            img = getResources().getIdentifier("reports_j", "drawable", "com.mobilewarehouse.firdavs.mobilewarehouse");
            ot_img[i] = img;
        }
    }

    public void onListItemClick(ListView parent, View v, int position, long id) {
        sel_otchet = position;
        Intent intent = new Intent(getApplicationContext(), Otchet_record_poles.class);
        startActivity(intent);
    }

    public class myAdapter extends BaseAdapter {
        private LayoutInflater mLayoutInflater;

        public myAdapter(Context ctx) {
            mLayoutInflater = LayoutInflater.from(ctx);
        }

        public int getCount() {
            return ot_name.length;
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }


        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null)
                convertView = mLayoutInflater.inflate(R.layout.item_otchet_record, null);

            ImageView image = (ImageView) convertView.findViewById(R.id.img_otchet);
            image.setImageResource(ot_img[position]);

            TextView itm_ot_name = (TextView) convertView.findViewById(R.id.itm_ot_name);
            itm_ot_name.setText(ot_name[position]);
            return convertView;
        }
    }

    ///////// WORKING WITH BACK BUTTON //////////////////////////////////////
    @Override
    public void onBackPressed() {
        finish();
    }

    //создаем меню
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        menu.add(Menu.NONE, LoginActivity.IDM_MAIN, menu.NONE, getString(R.string.app_menu2))
                .setIcon(android.R.drawable.ic_menu_revert);
        return (super.onCreateOptionsMenu(menu));
    }

    ;

    //обрабатываем выбор пункта меню
    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case LoginActivity.IDM_MAIN:
                finish();
                break;
            default:
                return false;
        }
        return true;
    }

    ;
}
