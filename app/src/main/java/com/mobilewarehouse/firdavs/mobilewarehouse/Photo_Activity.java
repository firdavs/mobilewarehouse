package com.mobilewarehouse.firdavs.mobilewarehouse;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Firdavs on 07.12.2015.
 */
public class Photo_Activity extends Activity {
    SurfaceView surfaceView;
    Camera camera;
    String global_file_name;
    Button btn_photo_send;
    Button btn_photo_take;
    Button btn_photo_exit;
    List<Camera.Size> sizes;
    public static Photo_Activity _myLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_photo);
        Photo_Activity._myLayout = this;

        //pictures = Environment
        //        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
        btn_photo_send = (Button) findViewById(R.id.btn_photo_send);
        btn_photo_take = (Button) findViewById(R.id.btn_photo_take);
        btn_photo_exit = (Button) findViewById(R.id.btn_photo_exit);
        btn_photo_send.setEnabled(false);

        SurfaceHolder holder = surfaceView.getHolder();
        holder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    setCameraParams(LoginActivity._myLayout.camera_resolution);
                    camera.setPreviewDisplay(holder);
                    camera.startPreview();
                } catch (Exception exception) {
                    camera.release();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format,
                                       int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        camera = Camera.open();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (camera != null)
            camera.release();
        camera = null;
    }

    public void setCameraParams(int position) {
        Camera.Parameters parameters = camera.getParameters();
        sizes = parameters.getSupportedPictureSizes();
        Camera.Size mSize = sizes.get(position);
        parameters.set("orientation", "portrait");
        parameters.setPictureSize(mSize.width, mSize.height);
        camera.setDisplayOrientation(90);
        parameters.setRotation(90);
        camera.setParameters(parameters);
        this.setTitle(String.valueOf(mSize.width) + "x" + String.valueOf(mSize.height));
    }

    public void onSurfaceClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.photo_picture_size)); // заголовок для диалога

        // convert supported sizes into a width x height string array
        CharSequence[] supportedSizes = new String[sizes.size()];
        for (int i = 0; i < sizes.size(); i++) {
            supportedSizes[i] = String.format("%dx%d",
                    sizes.get(i).width,
                    sizes.get(i).height);
        }
        builder.setSingleChoiceItems(supportedSizes, LoginActivity._myLayout.camera_resolution, new DialogInterface.OnClickListener() {
            //@Override
            public void onClick(DialogInterface dialog, int item) {
                LoginActivity._myLayout.keys_editor = LoginActivity._myLayout.keys.edit();
                LoginActivity._myLayout.keys_editor.putInt("camera", item);
                try {
                    LoginActivity._myLayout.keys_editor.commit();
                } catch (Exception err) {
                }
                LoginActivity._myLayout.camera_resolution = item;
                setCameraParams(LoginActivity._myLayout.camera_resolution);
                dialog.cancel();
            }
        });
        builder.setCancelable(false);
        builder.create();
        builder.show();
    }

    public void onPhotoCancel(View view) {
        camera.startPreview();
        btn_photo_take.setEnabled(true);
    }

    public void onPhotoExit(View view) {
        camera.release();
        camera = null;
        finish();
    }

    public void onPhotoSend(View view) {
        JSONObject jsonObjSend = new JSONObject();
        try {
            jsonObjSend.put("UserName", LoginActivity._myLayout.user_login);
            jsonObjSend.put("Password", LoginActivity._myLayout.user_password);
            jsonObjSend.put("Operation_Type", "_photo");
            jsonObjSend.put("Id", "_id");
            jsonObjSend.put("Type", "2");

            Log.d("json", jsonObjSend.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Photo_Url_Params params = new Photo_Url_Params(LoginActivity._myLayout.server_ip + "/android/savePhotos/", global_file_name, jsonObjSend);
        http_send_file send_file_me = new http_send_file(Sales_Activity._myLayout);
        send_file_me.execute(params);
        btn_photo_send.setEnabled(false);
        btn_photo_exit.setEnabled(false);
        Toast.makeText(Photo_Activity.this, this.getString(R.string.photo_send_toast), Toast.LENGTH_LONG).show();
        //finish();
    }

    public void onPhotoTake(View view) {
        camera.takePicture(null, null, new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                try {
                    SimpleDateFormat dformat = new SimpleDateFormat("yyMMddHHmmss");
                    Date date = new Date();
                    //photoFile = new File(pictures, dformat.format(date)+".jpg");
                    File sdcard = Environment.getExternalStorageDirectory();
                    File myDir = new File(sdcard, "Android/data/com.mobilewarehouse.firdavs.mobilewarehouse/tochki");
                    myDir.mkdirs();
                    global_file_name = myDir + File.separator + LoginActivity._myLayout.login + "_" + Sales_Activity._myLayout.sclient_account + "_" + dformat.format(date) + ".jpg";
                    File photoFile = new File(myDir + File.separator + LoginActivity._myLayout.login + "_" + Sales_Activity._myLayout.sclient_account + "_" + dformat.format(date) + ".jpg");

                    FileOutputStream fos = new FileOutputStream(photoFile);
                    fos.write(data);
                    fos.flush();
                    fos.close();
                    //Toast.makeText(Photo_Activity.this, "Фото " + global_file_name + ", data=" + data.length, Toast.LENGTH_LONG).show();
                    //camera.startPreview();
                    btn_photo_take.setEnabled(false);
                    btn_photo_send.setEnabled(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

}
